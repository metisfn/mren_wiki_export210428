﻿using Metis.Core.DataObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public class Webhook : DataObjectBase, IConcerning
    {
        public const string WEBHOOK_TYPE_EMAIL = "Email";
        public const string WEBHOOK_TYPE_URL = "Url";

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WebhookId { get; set; }
        [NotMapped]
        public override int PrimaryKey {
            get { return WebhookId; }
            set { WebhookId = value; }
        }

        public string Token { get; set; }
        public string WebhookType { get; set; }
        public string WebhookFormat { get; set; }
        public int CreatedByMetisUserId { get; set; }

        //public int? ActingUserId { get; set; }
        //[ForeignKey(nameof(ActingUserId))]
        //public virtual MetisUser ActingUser { get; set; }

        public int ConcerningPrimaryKeyId { get; set; }
        public string ConcerningTargetType { get; set; }
        public Guid ConcerningObjectId { get; set; }
        public Guid? ConcerningRevisionId { get; set; }
        public DateTimeOffset? DateExpired { get; set; }
    }
}
