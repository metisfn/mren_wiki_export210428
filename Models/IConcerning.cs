﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public interface IConcerning
    {
        int ConcerningPrimaryKeyId { get; set; }
        [MaxLength(150)]
        string ConcerningTargetType { get; set; }
        Guid ConcerningObjectId { get; set; }
        Guid? ConcerningRevisionId { get; set; }
    }

    public static class IConcerning_Extenstions
    {
        public static TObject SetConcerning<TObject>(this TObject toSet, IMetisDataObject toSetTo)
            where TObject : IConcerning
        {
            var concerningPt = toSetTo as IPermissionTarget;

            toSet.ConcerningObjectId = concerningPt != null ? concerningPt.TargetObjectId : toSetTo.ObjectId;
            toSet.ConcerningPrimaryKeyId = concerningPt != null ? concerningPt.TargetPrimaryKeyId : toSetTo.PrimaryKey;
            toSet.ConcerningTargetType = concerningPt != null ? concerningPt.TargetType : toSetTo.DataObjectTargetType;

            var asHistory = toSetTo as IMetisDataObjectWithHistory;
            if (asHistory != null)
            {
                toSet.ConcerningRevisionId = asHistory.RevisionId;
            }
            return toSet;
        }
    }
}
