﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    public interface IUserInvite : IMetisDataObjectWithHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int UserInviteId { get; set; }

        string Email { get; set; }
        int UserId { get; set; }

        [ForeignKey("UserId")]
        MetisUser User { get; set; }

        string Step { get; set; }
        string Token { get; set; }
    }

    [ODataPath("UserInvites")]
    public partial class UserInvite : DataObjectBaseWithHistory<UserInvite>, IUserInvite
    {
        [MaxLength(200)]
        public string FirstName { get; set; }

        [MaxLength(200)]
        public string LastName { get; set; }

        [MaxLength(200)]
        public string Company { get; set; }

        [MaxLength(200)]
        public string ProfileToApply { get; set; }

        public DateTimeOffset Expiration { get; set; }
        public int InvitingUserId { get; set; }
        public string BaseUrl { get; set; }
        public string InviteType { get; set; }
        public int? InvitePrimaryKeyId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserInviteId { get; set; }

        public string Email { get; set; }

        public override int PrimaryKey
        {
            get { return UserInviteId; }
            set { UserInviteId = value; }
        }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual MetisUser User { get; set; }

        public string Step { get; set; }
        public string Token { get; set; }
    }
}