﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("UserFavorites")]
    public partial class UserFavorite : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserFavoriteId { get; set; }

        public int MetisUserId { get; set; }
        public string ConcerningTargetType { get; set; }
        public int TargetPrimaryKeyId { get; set; }

        public override int PrimaryKey
        {
            get { return UserFavoriteId; }
            set { UserFavoriteId = value; }
        }
    }
}

