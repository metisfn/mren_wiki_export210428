/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Process.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class ProcessInProgress : DataObjectBaseWithHistory<ProcessInProgress>, IProcessInProgress, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 ProcessInProgressId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return ProcessInProgressId; } set { ProcessInProgressId = value; } }
		public Int32? CurrentlyActingUserId { get; set; }
		public virtual MetisUser CurrentlyActingUser { get; set; }
		IMetisUser IProcessInProgress.CurrentlyActingUser { get { return this.CurrentlyActingUser; } set { this.CurrentlyActingUser = (MetisUser)value; } }
		public Int32 ProcessStepId { get; set; }
		public virtual ProcessStep CurrentStep { get; set; }
		IProcessStep IProcessInProgress.CurrentStep { get { return this.CurrentStep; } set { this.CurrentStep = (ProcessStep)value; } }
		public Int32 ProcessFlowId { get; set; }
		public virtual ProcessFlow ProcessFlow { get; set; }
		IProcessFlow IProcessInProgress.ProcessFlow { get { return this.ProcessFlow; } set { this.ProcessFlow = (ProcessFlow)value; } }
		public Int32 OriginatingUserId { get; set; }
		public virtual MetisUser OriginatingUser { get; set; }
		IMetisUser IProcessInProgress.OriginatingUser { get { return this.OriginatingUser; } set { this.OriginatingUser = (MetisUser)value; } }
		public Guid? ProcessTargetObjectId { get; set; }
		public Int32? ProcessTargetPermissionTargetId { get; set; }
		public Int32? ProcessTargetPrimaryKeyId { get; set; }
		public String ProcessTargetType { get; set; }
		public String State { get; set; }
        public void SetState(object data)
		{
			var type = data.GetType();
			while(type != typeof(object) && type.Assembly.IsDynamic == true) type = type.BaseType;
			State = type.FullName + "#" + JsonConvert.SerializeObject(data, new JsonSerializerSettings { 
                                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore 
                            });
		}
        public object GetState()
		{
			var data = State;
			var typeFullname = data.Substring(0, data.IndexOf("#"));
			var serializedData = data.Substring(data.IndexOf("#") + 1);
			var type = Type.GetType(typeFullname);
			var ret = JsonConvert.DeserializeObject(serializedData, type);
			return ret;
		}
	}
}
		