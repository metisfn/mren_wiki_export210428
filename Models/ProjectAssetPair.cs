﻿using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Valuation.Logic.Models
{
    [ODataPath("ProjectAssetPairs")]
    public class ProjectAssetPair : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectAssetPairId { get; set; }

        public int ProjectId { get; set; }
        public int AssetId { get; set; }
        public bool IsPublic { get; set; }

        [MetisMetadataDataType("MetisUserId")]
        public string GuestMetisUserIds { get; set; }
        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> GuestMetisUserIdsList => this.GuestMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.GuestMetisUserIds);

        [MetisMetadataDataType("MetisUserId")]
        public string AssignedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> AssignedMetisUserIdsList => this.AssignedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.AssignedMetisUserIds);

        [NotMapped, MetaIgnore]
        public int OrganizationId { get; set; }

        [NotMapped, MetaIgnore]
        public virtual Project Project { get; set; }

        public override int PrimaryKey
        {
            get { return ProjectAssetPairId; }
            set { ProjectAssetPairId = value; }
        }
    }
}
