﻿using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects.Attributes;
using System;

namespace Valuation.Logic.Models
{
    [ODataPath("ConversationItemReadStatuses")]
    public partial class ConversationItemReadStatus
    {
        public int? MetisUserId { get; set; }

        [ForeignKey("MetisUserId")]
        public virtual MetisUser MetisUser { get; set; }

        public const string STATUS_UNREAD = "UNREAD";
    }
}