﻿namespace Valuation.Logic.Models
{
    public interface IGalleryOwner
    {
        Gallery Gallery { get; set; }
        int? GalleryId { get; set; }
    }
}