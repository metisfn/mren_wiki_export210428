﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using Metis.Core.DataObjects.Attributes;
using Microsoft.Azure;
using Newtonsoft.Json;

namespace Valuation.Logic.Models
{
    [ODataPath("FilesystemNodes")]
    public partial class FilesystemNode : ISubscribable, ITagListOwner
    {
        private static readonly Regex _nameValidator = new Regex(",|;", RegexOptions.Compiled);

        [JsonIgnore]
        public virtual ICollection<FilesystemNode> Children { get; set; }

        [ForeignKey("ParentNodeId")]
        public virtual FilesystemNode Parent { get; set; }

        [JsonIgnore]
        public IQueryable<FilesystemNode> Tree { get; set; }

        public int SortOrder { get; set; }

        [MaxLength(1000)]
        public string DocumentHash { get; set; }

        //public DateTimeOffset? LoadedDate { get; set; }

        [MaxLength(150)]
        public string ConcerningTargetType { get; set; }

        public int? ConcerningPrimaryKeyId { get; set; }
        public Guid? ConcerningObjectId { get; set; }
        public bool IsNdaFile { get; set; }

        public int? UploadedByWorkRoomId { get; set; }

        [ForeignKey("UploadedByWorkRoomId")]
        public virtual WorkRoom UploadedByWorkRoom { get; set; }

        public Guid? UploadRequestId { get; set; }


        public string Type { get; set; }
        public string Category { get; set; }
        public string Notes { get; set; }

        public bool IsPublic { get; set; }
        
        // Full text search anyone??
        public string Content { get; set; }

        [MetisMetadataDataType("MetisUserId")]
        public string SubscribedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> SubscribedMetisUserIdsList => this.SubscribedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.SubscribedMetisUserIds);

        public string TagList { get; set; }

        [JsonIgnore]
        public string PublicUrl
        {
            get
            {
                var downloadHost = CloudConfigurationManager.GetSetting("StorageDownloadUri.Host") ??
                                   ConfigurationManager.AppSettings["StorageDownloadUri.Host"];
                var downloadUri = new UriBuilder();
                downloadUri.Host = downloadHost;
                downloadUri.Scheme = CloudConfigurationManager.GetSetting("StorageDownloadUri.Scheme") ??
                                     ConfigurationManager.AppSettings["StorageDownloadUri.Scheme"] ?? downloadUri.Scheme;
                downloadUri.Port = downloadUri.Scheme == "https" ? 443 : 80;
                downloadUri.Path = "ncd-dev/" + ObjectId.ToString("D");

                return downloadUri.ToString();
            }
        }

        /// <summary>
        ///     Replace ',' and ';' with '.' so name wouldn't break content-disposition header value and cause chaos
        /// </summary>
        /// <param name="rawName"></param>
        /// <returns></returns>
        public static string GetValidNodeName(string rawName)
        {
            return string.IsNullOrEmpty(rawName) ? null : _nameValidator.Replace(rawName, ".");
        }
    }
}