﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("WorkRoomInvites")]
    public partial class WorkRoomInvite : DataObjectBaseWithHistory<WorkRoomInvite>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRoomInviteId { get; set; }

        public int UserId { get; set; }

        public int ParentWorkRoomId { get; set; }
        public int? CreatedWorkRoomId { get; set; }
        public string Name { get; set; }
        public int SourceOrganizationId { get; set; }
        public int? TargetOrganizationId { get; set; }
        public string Status { get; set; }
        public bool NewUser { get; set; }
        public string BaseUrl { get; set; }
        public DateTimeOffset? LastInviteSent { get; set; }

        public override int PrimaryKey
        {
            get { return WorkRoomInviteId; }
            set { WorkRoomInviteId = value; }
        }

        // denormalized: capture email at the time of entry so we can display without over-exposing user's true/normalized contact info
        public string Email { get; set; }


        public bool UseMetisUsersNameInEmail { get; set; }
        public int FromUserId { get; set; }
        public int? RemovingMetisUserId { get; set; }
        public string CustomMessage { get; set; }
        public string CustomSubject { get; set; }
        public string SendAsEmail { get; set; }
        public string SendAsName { get; set; }
        public DateTimeOffset? LastTimeViewedFromEmail { get; set; }
        public bool SelfEntered { get; set; }

        public DateTimeOffset? DateInviteCompleted { get; set; }
        public int? InviteCompletedByMetisUserId { get; set; }

        [NotMapped] // This is just for querying.
        public int GrayLabelId { get; set; }
    }
}