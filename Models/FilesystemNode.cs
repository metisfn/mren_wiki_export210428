/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.FileRepository.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class FilesystemNode : DataObjectBaseWithHistory<FilesystemNode>, IFilesystemNode, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 FilesystemNodeId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return FilesystemNodeId; } set { FilesystemNodeId = value; } }
		public String Name { get; set; }
		public String NodeType { get; set; }
		public Int32? ParentNodeId { get; set; }
		public Int32? Size { get; set; }
        public Int32 RootFilesystemNodeId { get; set; }
	}
}
		