﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class FilesystemNodeRead : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FilesystemNodeReadId { get; set; }

        public int MetisUserId { get; set; }
        public int FilesystemNodeId  { get; set; }

        public override int PrimaryKey
        {
            get { return FilesystemNodeReadId; }
            set { FilesystemNodeReadId = value; }
        }
    }
}

