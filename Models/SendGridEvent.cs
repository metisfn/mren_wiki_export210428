﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class SendGridEvent : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SendGridEventId { get; set; }

        public string SendGridMessageId { get; set; }
        public string Email { get; set; }
        public string Event { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string Url { get; set; }
        public int? GroupId { get; set; }
        public int? MetisEmailLogId { get; set; }

        public override int PrimaryKey
        {
            get { return SendGridEventId; }
            set { SendGridEventId = value; }
        }
    }
}