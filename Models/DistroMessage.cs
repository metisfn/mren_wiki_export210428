﻿using Metis.Core.DataObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public class DistroMessage : DataObjectBase
    {
        [Key]
        public int DistroMessageId { get; set; }
        [JsonIgnore]
        public override int PrimaryKey {
            get { return DistroMessageId; }
            set { DistroMessageId = value; }
        }
        public string Message { get; set; }
    }

    public class DistroMessageMetisUser
    {
        [Key, Column(Order = 0)]
        public int DistroMessageId { get; set; }
        [Key, Column(Order = 1)]
        public int MetisUserId { get; set; }

        [ForeignKey(nameof(DistroMessageId))]
        public virtual DistroMessage DistroMessage { get; set; }
        [ForeignKey(nameof(MetisUserId))]
        public virtual MetisUser MetisUser { get; set; }

    }

    public class DistroMessageLog : DataObjectBase
    {
        [Key]
        public int DistroMessageLogId { get; set; }
        [JsonIgnore]
        public override int PrimaryKey
        {
            get { return DistroMessageLogId; }
            set { DistroMessageLogId = value; }
        }

        public int MetisUserId { get; set; }
        [ForeignKey(nameof(MetisUserId))]
        public virtual MetisUser MetisUser { get; set; }

        public string TabId { get; set; }
        public string IDbVersion { get; set; }
        public int ClientLatestSyncId { get; set; }
        public string ConfirmedDistroMessageIds { get; set; }
        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> ConfirmedDistroMessageIdsList => this.ConfirmedDistroMessageIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.ConfirmedDistroMessageIds);

        public int ServerLatestSyncId { get; set; }
        public string MissingDistroMessageIds { get; set; }
        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> MissingDistroMessageIdsList => this.MissingDistroMessageIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.MissingDistroMessageIds);

        public void SetConfirmedDistroMessageIds(IEnumerable<int> ids)
        {
            if (ids == null || !ids.Any()) return;
            ConfirmedDistroMessageIds = string.Join(",", ids);
        }

        public void SetMissingDistroMessageIds(IEnumerable<int> ids)
        {
            if (ids == null || !ids.Any()) return;
            MissingDistroMessageIds = string.Join(",", ids);
        }
    }
}
