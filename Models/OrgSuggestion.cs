﻿using Metis.Core.DataObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("OrgSuggestions")]
    public class OrgSuggestion : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrgSuggestionId { get; set; }

        public override int PrimaryKey
        {
            get { return OrgSuggestionId; }
            set { OrgSuggestionId = value; }
        }

        public int FromMetisUserId { get; set; }
        [ForeignKey("FromMetisUserId")]
        public virtual MetisUser FromMetisUser { get; set; }

        public string ConcerningTargetType { get; set; }
        public int ConcerningTargetKeyId { get; set; }

        public int SuggestedOrgId { get; set; }
        [ForeignKey("SuggestedOrgId")]
        public virtual Organization SuggestedOrg { get; set; }

        public int? SuggestedMetisUserId { get; set; }
        [ForeignKey("SuggestedMetisUserId")]
        public virtual MetisUser SuggestedMetisUser { get; set; }

        public string CustomMessage { get; set; }

        public DateTimeOffset? DateAccepted { get; set; }
        
        public bool Read { get; set; }

        public bool IsTransferRequest { get; set; }

        public bool ShowNoticeOfTransfer { get; set; }
    }
}
