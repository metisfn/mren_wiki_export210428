﻿using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("Organizations")]
    public partial class Organization
    {
        public const string PERMISSION_PERSONAL_ORGANIZATION = "PersonalOrganization";
        public const string PERMISSION_ORGANIZATION_MEMBER = "OrganizationMember";
        public const string PERMISSION_INVITE = "Invite";
        public const string PERMISSION_ADD_DEAL = "AddDeal";
        public const string PERMISSION_ADD_ASSET = "AddAsset";
        public const string PERMISSION_ORGANIZATION_ADMINISTRATOR = "OrganizationAdministrator";
        public const string PERMISSION_PEER_VISIBILITY = "PeerVisibility";



        public const string PERMISSION_GROUPING_DEALS = "Deals";
        public const string PERMISSION_GROUPING_ACTIVE_OBJECTS = "ActiveObjects";
        public const string PERMISSION_GROUPING_ARCHIVED = "ArchivedObjects";
        public const string PERMISSION_GROUPING_USERS = "Users";
        public const string PERMISSION_GROUPING_INVITED_PROJECTS = "InvitedProjects";
        public const string PERMISSION_GROUPING_SOURCE_WORKROOMS = "MyWorkRooms";
        public const string PERMISSION_GROUPING_TARGET_WORKROOMS = "TargetWorkRooms";
        public const string PERMISSION_GROUPING_OWNED_CONFERENCE_ROOMS = "OwnedConferenceRooms";
        public const string PERMISSION_GROUPING_CREATED_BREAKOUT_ROOMS = "CreatedBreakoutRooms";
        public const string PERMISSION_GROUPING_PARTICIPATING_CONFERENCE_ROOMS = "ParticipatingConferenceRooms";
        public const string PERMISSION_GROUPING_PARTICIPATING_BREAKOUT_ROOMS = "ParticipatingBreakoutRooms";

        public const string PERMISSION_GROUPING_PROJECTS_WITH_CONTRIBUTE = "ProjectsWithContribute";
        public const string PERMISSION_GROUPING_PROJECTS_WITH_SHARE = "ProjectsWithShare";


        public const string URL_PRIVACY_PUBLIC = "Public";
        public const string URL_PRIVACY_GL = "GrayLabel";
        public const string URL_PRIVACY_DISABLED = "Disabled";

    }
}