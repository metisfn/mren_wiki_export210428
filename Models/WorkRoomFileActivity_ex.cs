﻿using System.Linq;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class WorkRoomFileActivity
    {
        public const string ACTION_UPLOAD = "Upload";
        public const string ACTION_DELETE = "Delete";
        public const string ACTION_MOVE = "Move";
        public const string ACTION_DOWNLOAD = "Download";
        public const string ACTION_RENAME = "Rename";
        public const string ACTION_UPDATE = "Update";
        public const string ACTION_SET_PUBLIC = "SetPublic";
        public const string ACTION_SET_PRIVATE = "SetPrivate";
        public const string ACTION_VIEW = "View";
        public const string ACTION_RESTORE = "Restore";
        public const string ACTION_ADD = "Add";
        public const string ACTION_REMOVE = "Remove";
        public const string ACTION_CREATE = "Create";
        public const string ACTION_VIEW_FILE_LINK = "ViewFileLink";

        public const string ACTION_GRANT_READ = "GrantView";
        public const string ACTION_GRANT_EDIT = "GrantEdit";
        public const string ACTION_GRANT_SHARE = "GrantShare";

        public const string ACTION_REMOVE_READ = "RemoveViewPermission";
        public const string ACTION_REMOVE_EDIT = "RemoveEditPermission";
        public const string ACTION_REMOVE_SHARE = "RemoveSharePermission";

        public const string ACTION_ADD_PARTICIPANT = "AddParticipant";
        public const string ACTION_REMOVE_PARTICIPANT = "RemoveParticipant";

        public const string ACTION_STAGED_ACCESS = "StageAccess";
        public const string ACTION_SENT_INVITE = "SendInviteEmail";
        public const string ACTION_ENABLED_ACCESS = "EnableAccess";

        public const string ACTION_SELF_ENTERED = "SelfEntered";
        public const string ACTION_ADDED_MEMBER = "AddedMember";

        public const string ACTION_REMOVE_DEAL_SUMMARY_PERMS = "RemoveDealSummaryPermissions";

        public static string DetermineActivityType(UserActivityType type)
        {
            switch (type)
            {
                case UserActivityType.AddFile:
                    return ACTION_UPLOAD;
                case UserActivityType.DownloadFile:
                    return ACTION_DOWNLOAD;
                case UserActivityType.DeleteFile:
                    return ACTION_DELETE;
                case UserActivityType.MoveFile:
                    return ACTION_MOVE;
                case UserActivityType.RenameFile:
                    return ACTION_RENAME;
                case UserActivityType.ReplaceFile:
                    return ACTION_UPDATE;
                case UserActivityType.ViewDocument:
                    return ACTION_VIEW;
                case UserActivityType.RestoreFile:
                    return ACTION_RESTORE;
                case UserActivityType.Create:
                    return ACTION_ADD;
                default:
                    return ACTION_UPDATE;
            }
        }

        public static string DetermineActivityType(string permission, bool isAdd)
        {
            switch (permission)
            {
                case "Readable":
                    return isAdd? WorkRoomFileActivity.ACTION_GRANT_READ : WorkRoomFileActivity.ACTION_REMOVE_READ;
                case "Contributable":
                    return isAdd? WorkRoomFileActivity.ACTION_GRANT_EDIT : WorkRoomFileActivity.ACTION_REMOVE_EDIT;
                case "Sharable":
                case "Shareable":
                    return isAdd ? WorkRoomFileActivity.ACTION_GRANT_SHARE : WorkRoomFileActivity.ACTION_REMOVE_SHARE;
            }
            return null;
        }
    }
}