/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public partial class PermissionProfileApplication : DataObjectBase, IPermissionProfileApplication, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionProfileApplicationId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionProfileApplicationId; }
            set { PermissionProfileApplicationId = value; }
        }

        [MaxLength(150)]
        public string ProfileName { get; set; }

        public int PermissionTargetId { get; set; }
        public int UserId { get; set; }
    }
}