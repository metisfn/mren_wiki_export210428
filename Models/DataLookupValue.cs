/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class DataLookupValue : DataObjectBase, IDataLookupValue, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 DataLookupValueId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return DataLookupValueId; } set { DataLookupValueId = value; } }
		public Int32 DataLookupId { get; set; }
		public virtual DataLookup DataLookup { get; set; }
		IDataLookup IDataLookupValue.DataLookup { get { return this.DataLookup; } set { this.DataLookup = (DataLookup)value; } }
		public Boolean IsNumber { get; set; }
		[MaxLength(500)]
		public String Label { get; set; }
		public Int32? NumericValue { get; set; }
		[MaxLength(500)]
		public String Value { get; set; }
		public Int32? SortOrder { get; set; }
		[MaxLength(500)]
		public String ParentValue { get; set; }
	}
}
		