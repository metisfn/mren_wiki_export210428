﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class PropertyChangeNotificationEvent : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PropertyChangeNotificationEventId { get; set; }
        [MaxLength(150)]
        public string TargetType { get; set; }
        [MaxLength(150)]
        public string PropertyName { get; set; }
        [MaxLength(150)]
        public string SubjectTemplateName { get; set; }
        [MaxLength(150)]
        public string ContentTemplateName { get; set; }
        [MaxLength(250)]
        public string PermissionToSendTo { get; set; }
        [MaxLength(250)]
        public string DeliveryType { get; set; }
        [MaxLength(250)]
        public string PermissionToSendMessagesTo { get; set; }
        public bool HasFromValue { get; set; }
        public bool HasToValue { get; set; }
        public string FromValue { get; set; }
        public string ToValue { get; set; }

        public override int PrimaryKey
        {
            get { return PropertyChangeNotificationEventId; }
            set { PropertyChangeNotificationEventId = value; }
        }
    }
}
