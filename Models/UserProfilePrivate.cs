﻿using System;
using System.ComponentModel;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
	[ODataPath("UserProfilePrivates")]
	public class UserProfilePrivate : DataObjectBaseWithHistory<UserProfilePrivate>
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 UserProfilePrivateId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey
		{
			get { return UserProfilePrivateId; }
			set { UserProfilePrivateId = value; }
		}


        //------ user prefs for app go here -------

        public const string PREFERENCE_UIBLADEMODE_COMPACT = "Compact";
        public const string PREFERENCE_UIBLADEMODE_EXPANDED = "Expanded";
        public string UIBladeMode { get; set; }

        public const string PREFERENCE_UITHEME_DARK = "Dark";
        public const string PREFERENCE_UITHEME_LIGHT = "Light";
        public string UITheme { get; set; }
        
        public string UITextZoom { get; set; }

        public const string PREFERENCE_SOUND_OFF = "Off";
        public const string PREFERENCE_SOUND_ON = "On";
        [DefaultValue(PREFERENCE_SOUND_ON)]
		public string Sound { get; set; }

        [NotMapped]
        public bool TwoFactorEnabled { get; set; }
        [NotMapped]
        public string PhoneNumber { get; set; }
        [NotMapped]
        public bool PhoneNumberConfirmed { get; set; }
        [NotMapped]
        public string LoginProvider { get; set; }
        [NotMapped]
        public string LinkedLogins { get; set; }
        [NotMapped]
        public bool HasPassword { get; set; }
		
		public string UIMapMode { get; set; }
        public string UIMapBasemap { get; set; }
        public string UIListMode { get; set; }
        
        public string UIFileView { get; set; }

        [DefaultValue(DigestSubscription.PREFERENCE_FREQUENCY_DAILY), MetisLookup("Frequency"), Obsolete]
        public string EmailFrequency { get; set; }

        [DefaultValue(DigestSubscription.PREFERENCE_FREQUENCY_DAILY), MetisLookup("Frequency"), Obsolete]
        public string UnreadChatEmailFrequency { get; set; }

        public bool? MobileAlerts { get; set; }


        public string ComponentsToured { get; set;  } // ERL 2016.09.21 comma delimited list of component names (so yes, we have data migration to do if we ever change the names of components)
        public bool? SuppressTours { get; set; }

        public string OnboardProgress { get; set; } 


        public int? LastSeenNotificationItemId { get; set; }
        public string NotificationFilter { get; set; }
        //--------------------------------------



        [MetisMetadataDataType("link")]
		public string LinkedInUrl { get; set; }
		public string SelfDescription { get; set; }
		public string PrimaryJobFunction { get; set; }
		public string Expertise { get; set; }
		public string Skills { get; set; }
		public string DegreesAndCredentials { get; set; }
		public string UniversityOrInstitution { get; set; }
		public string TransactionHighlight { get; set; }

		public string PlayerType { get; set; }

		//Company Profile - Show this if they selected "Principal" for PlayerType on signup

		//This doesn't do what I want. I probably just need to do something different than MetisInput.
		[MetisMetadataDataType("link")]
		public string CompanyUrl { get; set; }

		//adding this against my will, I think people will find it confusing. -JAH	
		public string CompanyHeadquarters { get; set; }

		public string City { get; set; }
		public string State { get; set; }

		[MetisLookup("PrimaryBusiness")]
		[Label("Primary Role")]
		public string PrimaryBusiness { get; set; }
		[Label("Other Role")]
		public string OtherBusinessType { get; set; }

		[MetisLookup("TypeUSOrForeign")]
		public string GeographicType { get; set; }
		public string ForeignLocations { get; set; }

		[MetisLookup("PrincipalType")]
		public string PrincipalType { get; set; }
		public string OtherPrincipalType { get; set; }
		

		[MetisLookup("PrivateRealEstateRange")]
		[Label("AUM")]
		public string PrivateRealEstateRange { get; set; }

		[MetisLookup("FamilyOfficeRange")]
		[Label("AUM")]
		public string FamilyOfficeRange { get; set; }

		[MetisLookup("IndividualRange")]
		[Label("Available Capital")]
		public string IndividualRange { get; set; }

		//Company Profile - Show this if they selected "Third Party" for PlayerType on signup
		[MetisLookup("ThirdPartyType")]
		public string ThirdPartyType { get; set; }


		//Maybe now that I write Sulakian typos I'll gain a superhuman ability to abstract stuff? -JAH
		[Label("Number Of Offices")]
		public string NumberOfOfficers { get; set; }
		public string Sector { get; set; }
		public string CompanyPitch { get; set; }
		public string HighlightTransactions { get; set; }

		//Investment Criteria - Show this for everyone
		
		//Interested In (Asset Types)
		public bool InterestedInOffice { get; set; }
		public bool InterestedInRetail { get; set; }
		public bool InterestedInMultifamily { get; set; }
		public bool InterestedInCondo { get; set; }
		public bool InterestedInHospitality { get; set; }
		public bool InterestedInLand { get; set; }
		public bool InterestedInHealthcare { get; set; }
		public bool InterestedInSelfStorage { get; set; }
		public bool InterestedInMixedUse { get; set; }
		public bool InterestedInEntertainment { get; set; }
		public bool InterestedInOther { get; set; }
		public string InterestedInOtherList { get; set; }

		//Market Focuses
		public bool MarketFocusPrimary { get; set; }
		public bool MarketFocusSecondary { get; set; }
		public bool MarketFocusTertiary { get; set; }

		//Currently not shown
		public string Regions { get; set; }

		[MetisLookup("InvestmentRole")]
		public string InvestmentRole { get; set; }

		[MetisLookup("TargetDealSize")]
		public string TargetDealSize { get; set; }

		public string InvestmentSweetSpot { get; set; }
		public string ReferencesList { get; set; }
        public string DismissedLufuOrgs { get; set; }

		public int MetisUserId { get; set; }

		[ForeignKey("MetisUserId")]
		public virtual MetisUser MetisUser { get; set; }

        public int? PersonalGrayLabelId { get; set; }
	}
}