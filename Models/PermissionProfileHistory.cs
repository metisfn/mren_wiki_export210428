/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class PermissionProfileHistory : DataObjectBase, IPermissionProfileHistory, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionProfileHistoryId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionProfileHistoryId; }
            set { PermissionProfileHistoryId = value; }
        }

        public string FullProfileSetup { get; set; }
        public string ProfileHash { get; set; }
    }
}