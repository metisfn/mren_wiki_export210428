﻿using Metis.Core.DataObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public enum FilesystemnodeActivityType
    {
        Unknown = 0,
        Editing = 1,
        Viewing = 2
    }

    public class FilesystemNodeUserActivity : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FilesystemNodeUserActivityId { get; set; }

        public override int PrimaryKey
        {
            get
            {
                return FilesystemNodeUserActivityId;
            }

            set
            {
                FilesystemNodeUserActivityId = value;
            }
        }

        public int FilesystemNodeId { get; set; }
        [ForeignKey(nameof(FilesystemNodeId))]
        public virtual FilesystemNode FileNode { get; set; }

        public int MetisUserId { get; set; }
        [ForeignKey(nameof(MetisUserId))]
        public virtual MetisUser MetisUser { get; set; }
        
        // denormalize for speed
        public string UserNameEmail { get; set; }

        [NotMapped, JsonConverter(typeof(StringEnumConverter))]
        public FilesystemnodeActivityType ActivityType { get; set; }
        [Column(nameof(ActivityType)), JsonIgnore]
        public string ActivityTypeString
        {
            get
            {
                return ActivityType.ToString();
            }

            set
            {
                FilesystemnodeActivityType at;
                if (!Enum.TryParse(value, out at))
                {
                    at = FilesystemnodeActivityType.Unknown;
                }
                ActivityType = at;
            }
        }

        public DateTimeOffset DateLockAcquired { get; set; }

        [NotMapped]
        public bool FileLockAcquired { get; set; }
    }
}
