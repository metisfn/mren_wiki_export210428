﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("RelationshipLinkInvestments")]
    public class RelationshipLinkInvestment : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RelationshipLinkInvestmentId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return RelationshipLinkInvestmentId; }
            set { RelationshipLinkInvestmentId = value; }
        }

        public int RelationshipLinkId { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number")]
        public decimal? Committed { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number")]
        public decimal? Invested { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number")]
        public decimal? Distributed { get; set; }

        /// <summary>
        ///     Simple permissioning.
        /// </summary>
        public bool VisibleToInvestor { get; set; }

        //From RelationshipLinks. Denormed so that Participant can see.
        [NotMapped]
        public int RelationshipLinkTargetPrimaryKeyId { get; set; }

        [NotMapped]
        public string RelationshipLinkConcerningTargetType { get; set; }

        [NotMapped]
        public int RelationshipTargetPrimaryKeyId { get; set; }

        [NotMapped]
        public string RelationshipConcerningTargetType { get; set; }

        [NotMapped]
        public int RelationshipId { get; set; }

        [NotMapped]
        public int? RelationshipMetisUserId { get; set; }

        [NotMapped]
        public int? RelationshipOrganizationId { get; set; }

        [NotMapped]
        public int? RelationshipTargetMetisUserId { get; set; }

        [NotMapped]
        public int? RelationshipTargetOrganizationId { get; set; }

        [NotMapped]
        public int? RelationshipTargetOrganizationProfileId { get; set; }
        /// <summary>
        ///     Means that it's in my commonData source because
        ///     I am the investor.
        /// </summary>
        [NotMapped]
        public bool MyInvestment { get; set; }
    }
}