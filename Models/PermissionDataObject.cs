/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class PermissionDataObject : DataObjectBase, IPermissionDataObject, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionDataObjectId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionDataObjectId; }
            set { PermissionDataObjectId = value; }
        }

        public string Name { get; set; }
        public string TargetType { get; set; }
        public string Permission { get; set; }
        public bool CanRead { get; set; }
        public bool CanWrite { get; set; }
        public bool CanDelete { get; set; }
    }
}