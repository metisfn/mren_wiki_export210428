﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Valuation.Logic.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("OrgTransaction")]
    public partial class OrgTransaction : DataObjectBaseWithHistory<OrgTransaction>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrgTransactionId { get; set; }

        public override int PrimaryKey
        {
            get { return OrgTransactionId; }
            set { OrgTransactionId = value; }
        }
        public int OrganizationId { get; set; }
        public string TransactionId { get; set; } 
        //Not the same thing as the team owner (necessessarily)
        public int ResponsiblePartyMuid { get; set; }
        public string TransactionType { get; set; }
        public string ConcerningTargetType { get; set; }
        public string ConcerningTargetPrimaryKeyIds { get; set; }
        public string NewPlanId { get; set; }
        public int? Delta { get; set; }
        public string AddOnId { get; set; }
        public string Notes { get; set; }
        public bool RemoveAll { get; set; }
        public decimal? TransactionAmount { get; set; }
    }
}