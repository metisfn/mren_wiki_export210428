﻿using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("Contacts")]
    public partial class Contact
    {
        public string Company { get; set; }

        public bool IsGhost { get; set; }

        public string Name { get; set; }
    }
}