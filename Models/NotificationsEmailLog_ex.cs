﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public partial class NotificationsEmailLog
    {
        public const string TYPE_CONVERSATION = "Conversation";
        public const string TYPE_NOTIFICATION = "Notification";
    }
}
