/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.Conversations.Interfaces;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
    public class ConversationSubscription : DataObjectBase, IConversationSubscription, IMetisScaffold
    {
        public virtual Contact Subscriber { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConversationSubscriptionId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ConversationSubscriptionId; }
            set { ConversationSubscriptionId = value; }
        }

        public int ConversationId { get; set; }
        public int? SubscriberContactId { get; set; }

        IContact IConversationSubscription.Subscriber
        {
            get { return Subscriber; }
            set { Subscriber = (Contact) value; }
        }

        public SubscriptionType SubscriptionType { get; set; }
        public int SubscriberPermissionTargetId { get; set; }
        public string SubscriberPermission { get; set; }
        public string DeliveryMethod { get; set; }
    }
}