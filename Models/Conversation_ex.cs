﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Metis.Core.DataObjects.Attributes;
using Metis.Core.DataObjects;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace Valuation.Logic.Models
{
    [ODataPath("Conversations")]
    public partial class Conversation : IConcerning, ISubscribable
    {
        public static string HELP_MESSAGES = "HelpMessages";
        public static string ADD_CONVERSATION = "AddConversation";
        public static string CONVERSATION_TYPE_DIRECT_MESSAGE = "DirectMessage";
        public static string CONVERSATION_TYPE_GROUP = "Group";
        public static string CONVERSATION_TYPE_NOTIFICATIONS = "Notifications";
        public static string CONVERSATION_TYPE_USER_TO_ORGANIZATION = "UserToOrganization";

        [ForeignKey("AuthorUserId")]
        public virtual MetisUser Author { get; set; }

        [MaxLength(1000)]
        public string Topic { get; set; }

        [MaxLength(1000)]
        public string ConcerningName { get; set; }

        public DateTimeOffset? LastMessageDate { get; set; }
        public int? MostRecentConversationItemId { get; set; }

        [MetaIgnore]
        [ForeignKey("MostRecentConversationItemId")]
        public virtual ConversationItem MostRecentConversationItem { get; set; }

        //In the case this is a direct message, there will be two users
        public int? OtherMetisUserId { get; set; }

        [ForeignKey("OtherMetisUserId")]
        public virtual MetisUser OtherMetisUser { get; set; }

        public int? OrganizationId { get; set; }

        [ForeignKey("OrganizationId")]
        public virtual Organization Organization { get; set; }

        public int? OtherOrganizationId { get; set; }

        [ForeignKey("OtherOrganizationId")]
        public virtual Organization OtherOrganization { get; set; }

        [MetisMetadataDataType("MetisUserId")]
        public string InvitedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> InvitedMetisUserIdList => string.IsNullOrEmpty(this.InvitedMetisUserIds) ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.InvitedMetisUserIds);

        [MetisMetadataDataType("MetisUserId")]
        public string ActiveMetisUserIds { get; set; }
        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> ActiveMetisUserIdList => string.IsNullOrEmpty(this.ActiveMetisUserIds) ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.ActiveMetisUserIds);

        [MetisMetadataDataType("MetisUserId")]
        public string SubscribedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> SubscribedMetisUserIdsList => string.IsNullOrEmpty(this.SubscribedMetisUserIds) ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.SubscribedMetisUserIds);

        public bool Archived { get; set; }

        int IConcerning.ConcerningPrimaryKeyId { get; set; }
        Guid IConcerning.ConcerningObjectId { get; set; }

        public IList<MetisUser> ConversationParticipants()
        {
            var list = new List<MetisUser> {Author};
            if (OtherMetisUser != null)
            {
                list.Add(OtherMetisUser);
            }
            return list.OrderBy(l => l.MetisUserId).ToList();
        }

        //If this returns true, the object and users of this convo are equal to that of the one you're testing.
        public bool MatchingConversation(string concerningTargetType, int concerningPrimaryKeyId, int metisUserId1, int metisUserId2)
        {
            if (concerningTargetType != ConcerningTargetType || concerningPrimaryKeyId != ConcerningPrimaryKeyId)
                return false;

            var usersToTest = new List<int> {metisUserId1, metisUserId2};

            var usersOnThisConvo = new List<int> {AuthorUserId};
            if (OtherMetisUserId.HasValue) usersOnThisConvo.Add(OtherMetisUserId.Value);

            return usersOnThisConvo.OrderByDescending(c => c).SequenceEqual(usersToTest.OrderByDescending(c => c));
        }

        public IList<int> ConversationParticipantsMetisUserIds()
        {
            return ConversationParticipants().Select(cp => cp.MetisUserId).ToList();
        }
    }
}