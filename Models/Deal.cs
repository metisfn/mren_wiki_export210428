﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    public partial class Deal : DataObjectBaseWithHistory<Deal>, IFilesystemOwner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DealId { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

		[MetisMetadataDataType("dollar")]
		[Validators("number")]
        public decimal Amount { get; set; }

        [DefaultValue(STATUS_NEGOTIATING)]
        public string State { get; set; }

        public bool IsArchived { get; set; }

        public DateTimeOffset CloseDate { get; set; }

        public int ProjectId { get; set; }

        [ForeignKey("ProjectId")]
        public Project Project { get; set; }

        [ForeignKey("RootFilesystemNodeId")]
        public virtual FilesystemNode RootFilesystemNode { get; set; }

    
        public override int PrimaryKey
        {
            get { return DealId; }
            set { DealId = value; }
        }

        public int? WorkRoomId { get; set; }

        public int? RootFilesystemNodeId { get; set; }

        public int ConversationId { get; set; }

        public string WelcomeMessage { get; set; }
    }
}