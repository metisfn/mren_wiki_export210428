/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class Contact : DataObjectBaseWithHistory<Contact>, IContact, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 ContactId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return ContactId; } set { ContactId = value; } }
		[MaxLength(100)]
		public String AddressLine1 { get; set; }
		public String AddressLine2 { get; set; }
		public String ApartmentOrSuite { get; set; }
		public String City { get; set; }
		public String Country { get; set; }
		public String County { get; set; }
		public String Email { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public String MiddleName { get; set; }
		public String PhoneExt { get; set; }
		public String PhoneNumber { get; set; }
		public String PostalCode { get; set; }
		public String State { get; set; }
		public String Suffix { get; set; }
		public String Title { get; set; }
		public String UserType { get; set; }
		public String CustomData 
		{ 
			get { return JsonConvert.SerializeObject(_CustomDataDictionary); }
			set { _CustomDataDictionary = JsonConvert.DeserializeObject<Dictionary<string,string>>(value); } 
		}
		private Dictionary<string, string> _CustomDataDictionary = new Dictionary<string, string>();
		[NotMapped]
		public IDictionary<string, string> CustomDataDictionary { get { return _CustomDataDictionary; } }
		public Int32? RootFilesystemNodeId { get; set; }
	}
}
		