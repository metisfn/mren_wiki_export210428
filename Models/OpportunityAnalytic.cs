﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("OpportunityAnalytics")]
    public class OpportunityAnalytic : DataObjectBaseWithHistory<OpportunityAnalytic>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OpportuntiyAnalyticId { get; set; }
        public int ProjectId { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }
        public int ViewCount { get; set; }
        public int ConsiderationCount { get; set; }
        public override int PrimaryKey { get { return OpportuntiyAnalyticId; } set { OpportuntiyAnalyticId = value; } }
    }
}
