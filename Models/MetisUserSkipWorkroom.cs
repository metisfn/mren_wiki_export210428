﻿using Metis.Core.DataObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("MetisUserSkipWorkRooms")]

    public class MetisUserSkipWorkRoom : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MetisUserSkipWorkRoomId { get; set; }
        public override int PrimaryKey
        {
            get
            {
                return MetisUserSkipWorkRoomId;
            }

            set
            {
                MetisUserSkipWorkRoomId = value;
            }
        }

        [Index("Idx_MetisUser_WorkRoom", IsUnique = true, Order = 1)]
        public int MetisUserId { get; set; }
        [ForeignKey("MetisUserId")]
        public virtual MetisUser MetisUser { get; set; }
        [Index("Idx_MetisUser_WorkRoom", IsUnique = true, Order = 2)]
        public int WorkRoomId { get; set; }
        [ForeignKey("WorkRoomId")]
        public virtual WorkRoom WorkRoom { get; set; }
        [Index("Idx_MetisUser_WorkRoom", IsUnique = true, Order = 3), DefaultValue("ACT")]
        public new string StatusCode { get; set; }
    }
}
