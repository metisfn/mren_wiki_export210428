﻿using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    [ODataPath("DigestSubscriptions")]
    public class DigestSubscription : DataObjectBase, IConcerning
    {
        public const string DELIVERY_EMAIL = "Email";
        public const string DELIVERY_SMS = "Sms";
        public const string DELIVERY_PUSH = "Push";
        
        [Obsolete]
        public const string DIGEST_CHATS = "Chats";                         // dm, help, mrooms and user to org chats

        public const string PREFERENCE_FREQUENCY_NEVER = "Never";
        public const string PREFERENCE_FREQUENCY_HOURLY = "Hourly";
        public const string PREFERENCE_FREQUENCY_DAILY = "Daily";
        public const string PREFERENCE_FREQUENCY_WEEKLY = "Weekly";
        public const string PREFERENCE_FREQUENCY_EVERY5MIN = "Every_5_minutes";
        public const string PREFERENCE_FREQUENCY_INSTANT = "Instant";

        public const string DIGEST_TYPE_ALL_CHATS = "AllChats";
        public const string DIGEST_TYPE_NOTIFICATIONS = "Notifications";
        public const string DIGEST_TYPE_SUBSCRIBED_CHATS = "SubscribedChats";
        public const string DIGEST_TYPE_MENTIONED_CHATS = "DirectChats";

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DigestSubscriptionId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return DigestSubscriptionId; }
            set { DigestSubscriptionId = value; }
        }

        public int MetisIdentityId { get; set; }
        //[ForeignKey(nameof(MetisIdentityId)), JsonIgnore]
        //public virtual MetisIdentity MetisIdentity { get; set; }

        [Obsolete]
        public int? MetisUserId { get; set; }
        [Obsolete, ForeignKey(nameof(MetisUserId)), JsonIgnore]
        public virtual MetisUser MetisUser { get; set; }

        [MetisLookup("DigestType")]
        public string DigestType { get; set; }

        [MetisLookup("DeliveryType")]
        public string DeliveryType { get; set; }
        [DefaultValue(PREFERENCE_FREQUENCY_DAILY), MetisLookup("Frequency")]
        public string DeliveryFrequency { get; set; }
        public string DeliveryEmail { get; set; }
        public string DeliveryPhone { get; set; }
        [DefaultValue(true)]
        public bool DestinationConfirmed { get; set; }
        
        #region IConcerning
        [Obsolete]
        public Guid ConcerningObjectId { get; set; }
        [Obsolete]
        public int ConcerningPrimaryKeyId { get; set; }
        [Obsolete]
        public Guid? ConcerningRevisionId { get; set; }
        /// <summary>
        /// Site == global
        /// </summary>
        [Obsolete]
        public string ConcerningTargetType { get; set; }
        #endregion
    }
}
