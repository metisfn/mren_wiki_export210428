/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.Conversations.Interfaces;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Newtonsoft.Json;
using System.Linq;
using System.Text.RegularExpressions;

namespace Valuation.Logic.Models
{
    public partial class ConversationItem : DataObjectBase, IConversationItem, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConversationItemId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ConversationItemId; }
            set { ConversationItemId = value; }
        }

        public int ConversationId { get; set; }

        IConversation IConversationItem.Conversation
        {
            get { return Conversation; }
            set { Conversation = (Conversation) value; }
        }

        public int AuthorId { get; set; }
        public string Content { get; set; }
        public string Subject { get; set; }
        public string Data { get; set; }

        public void SetData(object data)
        {
            Data = ConvertToStringData(data);
        }

        private static Regex _dirtyJsonRegex = new Regex("\\\\\\\"", RegexOptions.Compiled);
        public T GetData<T>()
        {
            var dirtyJson = Data.Split('#').Last().Replace("}\"", "}");
            var cleanJson = _dirtyJsonRegex.Replace(dirtyJson, "\"");
            return JsonConvert.DeserializeObject<T>(cleanJson);
        }

        public object GetData()
        {
            var parts = Data.Split('#');
            var dirtyJson = Data.Split('#').Last().Replace("}\"", "}");
            var cleanJson = _dirtyJsonRegex.Replace(dirtyJson, "\"");
            var typeFullname = parts.Reverse().Skip(1).First().Replace("\"", string.Empty);

            var type = Type.GetType(typeFullname);
            var ret = JsonConvert.DeserializeObject(cleanJson, type);
            return ret;
        }

        public int? RootFilesystemNodeId { get; set; }

        public static string ConvertToStringData(object data)
        {
            var type = data.GetType();
            while (type != typeof (object) && type.Assembly.IsDynamic) type = type.BaseType;
            return type.FullName + "#" + JsonConvert.SerializeObject(data, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }
    }
}