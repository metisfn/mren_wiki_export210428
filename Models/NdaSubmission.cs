﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("NdaSubmissions")]
    public class NdaSubmission : DataObjectBase, IFilesystemOwner
    {
        public NdaSubmission()
        {
        }

        ////CER 2016.03.10 DEPRECATED
        //public NdaSubmission(int projectId, int organizationId, int? metisUserId, string ndaTxt)
        //{
        //    ProjectId = projectId;
        //    OrganizationId = organizationId;
        //    MetisUserId = metisUserId;
        //    NdaText = ndaTxt;
        //}

        public NdaSubmission(int workRoomId, int projectId, int organizationId, int? metisUserId)
        {
            ProjectId = projectId;
            WorkRoomId = workRoomId;
            OrganizationId = organizationId;
            MetisUserId = metisUserId;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NdaSubmissionId { get; set; }
        public override int PrimaryKey
        {
            get { return NdaSubmissionId; }
            set { NdaSubmissionId = value; }
        }

        public int? ProjectId { get; set; }
        public int? WorkRoomId { get; set; }
        public int? MetisUserId { get; set; }
        public virtual MetisUser MetisUser { get; set; }
        public int? OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
        public string NdaText { get; set; }

        public int? RootFilesystemNodeId { get; set; }
    }
}