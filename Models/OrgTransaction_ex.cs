﻿namespace Valuation.Logic.Models
{
    public partial  class OrgTransaction
    {
        public const string TARGET_TYPE_PROJECT = "Project";
        public const string TARGET_TYPE_ORGANIZATION = "Organization";
        public const string TARGET_TYPE_ORGANIZATION_INVITE = "OrganizationInvite";

        public const string TRANSACTION_TYPE_UPGRADE = "Upgrade";
        public const string TRANSACTION_TYPE_TRANSFER = "Transfer";
        public const string TRANSACTION_TYPE_CANCEL = "Cancel";
        public const string TRANSACTION_TYPE_PAYMENT_UPDATE = "PaymentUpdate";
    }
}
