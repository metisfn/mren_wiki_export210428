﻿using Metis.Core.DataObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valuation.Logic.Providers;

namespace Valuation.Logic.Models
{
    public class RemoteHangfireQueueFallback : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RemoteHangfireQueueFallbackId { get; set; }

        public override int PrimaryKey
        {
            get
            {
                return RemoteHangfireQueueFallbackId;
            }

            set
            {
                RemoteHangfireQueueFallbackId = value;
            }
        }

        public string MessageType { get; set; }
        [MaxLength(8000)]
        public string MessageJson { get; set; }
        [MaxLength(8000)]
        public string ErrorCausingFallback { get; set; }
        public string EnqueuedByAppName { get; set; }
        public string DequeuedByAppName { get; set; }
        public DateTimeOffset MessageDequeuedOnDate { get; set; }
        /// <summary>
        /// lack of failure messages on the item doesn't guarantee that the message was in fact processed successfully
        /// because current implementation puts this message on background queue so it goes outside of the scope.
        /// What lack of the error suggests is that fallback queue was processed successfully.
        /// </summary>
        [MaxLength(8000)]
        public string FallbackProcessingError { get; set; }
    }
}
