﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    /// <summary>
    /// For when a MetisIdentity has multiple users and more than one of those users belong to the same team.
    /// Determine which of the identity's users are the acting user.
    /// </summary>
    public class OrganizationPreference : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrganizationPreferenceId { get; set; }
        public int MetisUserId { get; set; }
        public int OrganizationId { get; set; }
        public int MetisIdentityId { get; set; }
        public override int PrimaryKey
        {
            get { return OrganizationPreferenceId; }
            set { OrganizationPreferenceId = value; }
        }
    }
}
