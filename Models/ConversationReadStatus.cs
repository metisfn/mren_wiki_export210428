﻿using Metis.Core.DataObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public enum MergeAction
    {
        Insert = 0,
        Update = 1,
        Noop = 2,
    }

    public class ConversationReadStatus : DataObjectBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConversationReadStatusId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ConversationReadStatusId; }
            set { ConversationReadStatusId = value; }
        }

        public int ConversationId { get; set; }
        [ForeignKey(nameof(ConversationId)), JsonIgnore, MetaIgnore]
        public virtual Conversation Conversation { get; set; }

        [JsonIgnore]
        public int MetisUserId { get; set; }
        [ForeignKey(nameof(MetisUserId)), JsonIgnore, MetaIgnore]
        public virtual MetisUser MetisUser { get; set; }

        public bool Mentioned { get; set; }

        public int LastReadItemId { get; set; }
        [ForeignKey(nameof(LastReadItemId)), JsonIgnore, MetaIgnore]
        public virtual ConversationItem ConversationItem { get; set; }

        [NotMapped, JsonIgnore, MetaIgnore]
        public string Action { get; set; }

        [NotMapped, JsonIgnore, MetaIgnore]
        public MergeAction MergeAction
        {
            get
            {
               return string.IsNullOrEmpty(Action) ? MergeAction.Noop : Action.ToLowerInvariant() == "update" ? MergeAction.Update : MergeAction.Insert;
            }
        }

        public static ConversationReadStatus GetDefault(int conversationId, int metisUserId)
        {
            return new ConversationReadStatus
            {
                StatusCode = "ACT",
                ConversationId = conversationId,
                ConversationReadStatusId = 0,
                MetisUserId = metisUserId,
                LastReadItemId = 0
            };
        }

        public const string ConversationMetisUserIdxName = "IX_MetisUser_Conversation";

        /// <summary>
        /// Create index for faster retrievals for ConversationReadStatuses.
        /// Query by MetisUserId + StatusCode or MetisUserId + ConversationId + StatusCode.
        /// </summary>
        /// <returns></returns>
        public static string GetConversationMetisUserIdxSQLCreate()
        {
            return "CREATE UNIQUE NONCLUSTERED INDEX " + ConversationMetisUserIdxName +
                @" ON ConversationReadStatus (MetisUserId, StatusCode, ConversationId)
                INCLUDE (" + nameof(LastReadItemId) + @")
                WITH (IGNORE_DUP_KEY = OFF) ";
        }

        public static string GetConversationMetisUserIdxSQLDrop()
        {
            return @"DROP INDEX IF EXISTS [dbo].[ConversationReadStatus]." + ConversationMetisUserIdxName;
        }
    }
}
