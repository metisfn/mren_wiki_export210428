﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public partial class MobileNotificationLog
    {
        public const string STATUS_QUEUED = "Queued";
        public const string STATUS_SENT = "Sent";
        public const string STATUS_CLICKED = "Clicked";
        public const string STATUS_DELETED = "Deleted";
        public const string STATUS_FAILED = "Failed";
        public const string STATUS_DISPLAYED = "Displayed";
    }
}
