﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Threading.Tasks;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Metis.Core.FileRepository;
using Metis.Core.FileRepository.Interfaces;
using Newtonsoft.Json;

namespace Valuation.Logic.Models
{
    public partial class InviteJobsLog
    {
        public const string STATUS_IN_PROGRESS = "InProgress";
        public const string STATUS_FAILED = "Failed";
        public const string STATUS_COMPLETED = "Completed";

        public const string READ_STATUS_READ = "Read";
    }

    [ODataPath("InviteJobsLogs")]
    public partial class InviteJobsLog : DataObjectBase, IConcerning, IFilesystemOwner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InviteJobsLogId { get; set; }

        public int CurrentIteration { get; set; }
        public int TotalCount { get; set; }

        public string Status { get; set; }

        public int CreatedByMetisUserId { get; set; }

        public string ReadStatus { get; set; }

        public override int PrimaryKey
        {
            get { return InviteJobsLogId; }
            set { InviteJobsLogId = value; }
        }

        public string Result { get; set; }
        public string InviteDto { get; set; }

        public int? DownloadFilesystemNodeId { get; set; }

        public bool HasFailures { get; set; }

        public bool HasDto { get; set; }

        private string BaseUrl { get; set; }

        // need to keep it for now for proper migrations
        [Obsolete]
        public int? OriginalId { get; set; }

        [Obsolete]
        public Guid RevisionId { get; set; }

        public int ConcerningPrimaryKeyId { get; set; }
        public string ConcerningTargetType { get; set; }
        public Guid ConcerningObjectId { get; set; }
        public Guid? ConcerningRevisionId { get; set; }

        public int? RootFilesystemNodeId { get; set; }

        public void CompleteWithSuccess()
        {
            CurrentIteration = TotalCount;
            Status = STATUS_COMPLETED;
        }

        public async Task CreateInviteDtoFile(IValuationDataContext db, string content)
        {
            var repo = await FileRepository.GetRepository(this, db);
            var repoRoot = repo.GetRoot();
            IFilesystemNode relaNode = null;
            using (var ms = new MemoryStream())
            using (var streamWriter = new StreamWriter(ms))
            {
                // we are using stream writer so we dont have to worry about string encoding
                await streamWriter.WriteAsync(content);
                await streamWriter.FlushAsync();
                // make sure underlying stream position is at the beginning after write to make sure we are starting
                // from the beginning when uploading this stream
                ms.Position = 0;
                relaNode =
                    await repo.CreateFileAsync($"invitedto for invitejobslogid {InviteJobsLogId}.json", ms, repoRoot);
            }

            InviteDto = JsonConvert.SerializeObject(relaNode.GetPOCOCopy(db));
            await db.SaveChangesAsync();
        }
    }
}