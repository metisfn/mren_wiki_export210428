﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("Ndas")]
    public class Nda : DataObjectBase, IFilesystemOwner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NdaId { get; set; }

        public override int PrimaryKey
        {
			get { return NdaId; }
			set { NdaId = value; }
        }

        public int? RootFilesystemNodeId { get; set; }
    }
}