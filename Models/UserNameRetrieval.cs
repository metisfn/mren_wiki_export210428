﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("UserNameRetrievals")]
    public partial class UserNameRetrieval : DataObjectBaseWithHistory<UserNameRetrieval>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserNameRetrievalId { get; set; }

        public string Email { get; set; }

        public override int PrimaryKey
        {
            get { return UserNameRetrievalId; }
            set { UserNameRetrievalId = value; }
        }

        public int MetisUserId { get; set; }
        public string Step { get; set; }
        public string Token { get; set; }
        public DateTimeOffset Expiration { get; set; }
        public string BaseUrl { get; set; }
        public bool ProactiveSupportGenerated { get; set; }
    }
}