﻿//using System;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using Metis.Core.DataObjects;

//namespace Valuation.Logic.Models
//{
//    public class ProjectActivationBypassToken : DataObjectBaseWithHistory<ProjectActivationBypassToken>
//    {
//        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
//        public int ProjectActivationBypassTokenId { get; set; }

//        public int OwningMetisUserId { get; set; }

//        public int? ProjectId { get; set; }
    
//        public DateTimeOffset? DateUsed { get; set; }

//        public override int PrimaryKey
//        {
//            get { return ProjectActivationBypassTokenId; }
//            set { ProjectActivationBypassTokenId = value; }
//        }
//    }
//}
