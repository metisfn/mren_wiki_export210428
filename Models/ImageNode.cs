﻿/****************************************************************************************
THIS FILE IS AUTO-GENERATED. (Not really, but will be after we move to a new core).

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;
using Newtonsoft.Json.Converters;

namespace Valuation.Logic.Models
{
    public enum ImageType
    {
        Source = 0,
        Thumbnail = 1,
        Page = 2
    }

    public enum NodeProgressStatus
    {
        NotStarted = 0,
        InProgress = 1,
        Failed = 2,
        /// <summary>
        /// Image of specified type was successfuly generated.
        /// If image type is Source: all doc conversion operations were completed successfully
        /// </summary>
        Succeeded = 3,
        PendingUpdate = 4,
        NotSupported = 5,
        /// <summary>
        /// Applicable to Source ImageType: Document was successfully converted to an image
        /// however, the source doc still has pending operations (i.e. extract pages, etc)
        /// </summary>
        PartialSuccess = 6
    }

    public interface IImageNode : IMetisDataObject
    {
        string ErrorMessage { get; set; }
        string Filename { get; set; }
        int? Height { get; set; }
        Guid? ImageAzureId { get; set; }
        string ImageName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        int ImageNodeId { get; set; }
        ImageType? ImageType { get; set; }
        int? PageNumber { get; set; }
        int? Pages { get; set; }
        [JsonIgnore]
        NodeProgressStatus ProgressStatus { get; set; }
        [Index("IX_SourceAzureId", IsClustered = false, IsUnique = false)]
        Guid SourceAzureId { get; set; }
        int? Width { get; set; }
        int? ConversionAttempts { get; set; }
        long? FileSize { get; set; }
    }

    /// <summary>
    /// TODO use ImageNode from the new Core.
    /// </summary>
    public partial class ImageNode : DataObjectBase, IImageNode, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 ImageNodeId { get; set; }

        [NotMapped]
        public override Int32 PrimaryKey { get { return ImageNodeId; } set { ImageNodeId = value; } }
        public Guid SourceAzureId { get; set; }
        public Guid? ImageAzureId { get; set; }
        public String Filename { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ImageType? ImageType { get; set; }
        public Int32? Pages { get; set; }
        public Int32? PageNumber { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public NodeProgressStatus ProgressStatus { get; set; }
        public String ErrorMessage { get; set; }
        public String ImageName { get; set; }
        public Int32? Width { get; set; }
        public Int32? Height { get; set; }
        public int? ConversionAttempts { get; set; }
        public long? FileSize { get; set; }
    }
}