﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using Valuation.Logic.Attributes;
using Valuation.Logic.DTOs;

namespace Valuation.Logic.Models
{
    [ODataPath("Assets")]
    public class Asset : DataObjectBaseWithHistory<Asset>, IFilesystemOwner, IGalleryOwner, ICustomFieldsOwner, ISubscribable, ITagListOwner, IProfileImageOwner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AssetId { get; set; }

        [MetisRequired]
		[Validators("required")]
        [MetisMetadataDataType("bladeUrlPart")]
        public string Name { get; set; }

        [Copyable]
        public string Type { get; set; }

        [Copyable]
        public string DisplayOrder { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Appraised/Internal Value"), CustomField("G107", "Value", "Appraised/Internal Value", "Summary", "dollar", -13, false, new[] { "number" }, glIdsToIgnore: new int[] { 36 })]
        public decimal Value { get; set; }
        [Copyable, CustomField("G108", "APN", "APN", "Summary", "string", -12, false, glIdsToIgnore: new int[] { 36 } )]
        public string APN { get; set; }

        [Label("Date"), Copyable, CustomField("G200", "CalendarDate", "Date", "Summary", "string", -21, true)] // ERL 2017.07.03 utility date field for use with the calendar
        public DateTimeOffset? CalendarDate { get; set; }


        [Copyable, CustomField("G100", "StreetAddress", "Street Address", "Summary", "string", -20, false)]
        public string StreetAddress { get; set; }

        [Copyable, Label("Street Address 2"), CustomField("G101", "StreetAddress2", "Street Address 2", "Summary", "string", -19, false)]
        public string StreetAddress2 { get; set; }

        [Copyable, CustomField("G102", "City", "City", "Summary", "string", -18, false)]
        public string City { get; set; }

        //This is a cool idea once we're smart enough to switch to State when we set Country to USA
        //  [MetisLookup("state")]
        [Copyable, CustomField("G103", "State", "State/Region", "Summary", "string", -17, false)]
        public string State { get; set; }
        [Copyable, CustomField("G104", "ZipCode", "Zip/Postal Code", "Summary", "string", -16, false)]
        public string ZipCode { get; set; }
        [Copyable, CustomField("G105", "Country", "Country", "Summary", "string", -15, false)]
        public string Country { get; set; }
        [Copyable]
        public string Occupancy { get; set; }

		[MetisMetadataDataType("decimal"),Validators("number"), Copyable, CustomField("G109", "Size", "Size", "Summary", "number", -11, false, new[] {"number"}, glIdsToIgnore: new int[] { 36 })]
        public decimal Size { get; set; }
        [DefaultValue("SFT"), MetisLookup("AssetSizeType"), Copyable, CustomField("G110", "SizeType", "Size Type", "Summary", "string", -10, false, null, false, null, "G109", glIdsToIgnore: new int[] { 36 })]
        public string SizeType { get; set; }

		[MetisMetadataDataType("decimal"),Validators("number"), Copyable, CustomField("G111", "LandSize", "Land Size", "Summary", "number", -9, false, new[] { "number" }, glIdsToIgnore: new int[] { 36 })]
        public decimal LandSize { get; set; }
        [DefaultValue("Acres"), MetisLookup("AssetLandSizeType"), Copyable, CustomField("G112", "LandSizeType", "Land Size Type", "Summary", "string", -8, false, null, false, null, "G111", glIdsToIgnore: new int[] { 36 })]
        public string LandSizeType { get; set; }


        [MetisMetadataDataType("year"),Validators("year"), Copyable, CustomField("G113", "YearBuilt", "YearBuilt", "Summary", "year", -7, false, new[] { "year" }, glIdsToIgnore: new int[] { 36, 109 })]
        public int YearBuilt { get; set; }
        [Copyable, CustomField("G114", "Zoning", "Zoning", "Summary", "string", -6, false, glIdsToIgnore: new int[] { 36 })]
        public string Zoning { get; set; }
        [Copyable]
        public string LeaseType { get; set; }

        [MetisLookup("AssetOwnershipInterest"), Copyable, CustomField("G115", "OwnershipInterest", "Ownership Interest", "Summary", "string", -5, false, glIdsToIgnore: new int[] { 36 })]
        public string OwnershipInterest { get; set; }

        [DefaultValue("Office"), MetisLookup("AssetType"), Copyable, CustomField("G106", "AssetType", "Asset Type", "Summary", "string", -14, false)]
        public string AssetType { get; set; }

        public int OrganizationId { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        [Copyable]//, CustomField("G116", "Description", "Description", "Summary", "string", -4, false, null, true, orgIdsToIgnore: new[] { 25688, 26051, 26053, 26055 })]
        public string Description { get; set; }
        [Copyable]//, CustomField("G117", "FinancialHighlights", "Financial Highlights", "Summary", "string", -3, false, null, true, orgIdsToIgnore: new[] { 25688, 26051, 26053, 26055 })]
        public string FinancialHighlights { get; set; }
        [Copyable]//, CustomField("G118", "MarketHighlights", "Market Highlights", "Summary", "string", -2, false, null, true, orgIdsToIgnore: new[] { 25688, 26051, 26053, 26055 })]
        public string MarketHighlights { get; set; }

        [ForeignKey("OrganizationId")]
        public virtual Organization Organization { get; set; }

        [ForeignKey("RootFilesystemNodeId")]
        public virtual FilesystemNode RootFilesystemNode { get; set; }

        [DefaultValue(false)]
        public bool IsArchived { get; set; }

        [MetaIgnore]
        public virtual ICollection<Project> Projects { get; set; }

        public override int PrimaryKey
        {
            get { return AssetId; }
            set { AssetId = value; }
        }

        public int? AddressId { get; set; }

        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }

        public int? RootFilesystemNodeId { get; set; }

        [ForeignKey("GalleryId")]
        public virtual Gallery Gallery { get; set; }

        public int? GalleryId { get; set; }
        
        [Label("Status"), MetisLookup("AssetStatus"), Copyable]
        public string Status { get; set; }

        public int? OriginalAssetId { get; set; }

        [Copyable]
        public string CustomMeta { get; set; }
        [NotMapped, JsonIgnore]
        public IList<CustomMeta> CustomMetaList { get; set; }

        [Copyable]
        public string CustomFields { get; set; }

        [Copyable]
        public string CustomFieldsFromImport { get; set; }

        [NotMapped]
        public Dictionary<string, object> CustomFieldsDict { get; set; } 

        public int? WorkRoomId { get; set; }

        [ForeignKey("WorkRoomId")]
        public virtual WorkRoom WorkRoom { get; set; }



        [Label("Contact(s)"), MetisMetadataDataType("MetisUserId"), CustomField("G99", "ContactUserIds", "Contact(s)", "Summary", "MetisUserId", -22, false)]
        public string ContactUserIds { get; set; }

        // ----------- TLH fields -------------

        [Label("Property Type"), Copyable, Obsolete]
        public string PropertyType { get; set; }


        [Label("No. Buildings"), Obsolete]
        [Copyable]
        public decimal? NumBuildings { get; set; }

        [MetisMetadataDataType("decimal"), Validators("number"), Label("Industrial Area (SF)"), Copyable, Obsolete]
        public decimal? IndustrialArea { get; set; }

        [MetisMetadataDataType("decimal"), Validators("number"), Label("Office Area (SF)"), Copyable, Obsolete]
        public decimal? OfficeArea { get; set; }

        [MetisMetadataDataType("decimal"), Validators("number"), Label("Land Area (Acres)"), Copyable, Obsolete]
        public decimal? LandArea { get; set; }
        
        [MetisMetadataDataType("decimal"), Validators("number"), Label("Building Area (SF)"), Copyable, Obsolete]
        public decimal? BuildingArea { get; set; }

        [Label("No. Stories (Above Grade)"), Copyable, Obsolete]
        public decimal? NumStories { get; set; }

        [Label("Year Built"), Copyable, Obsolete]
        public string Tlh_YearBuilt { get; set; }

        [Label("Year Renovated"), Copyable, Obsolete]
        public string Tlh_YearRenovated { get; set; }

        // public string Zoning { get; set; }
        [Copyable, Obsolete]
        public string Structure { get; set; }

        [Validators("number"), Label("Hours of Operation"), Copyable, Obsolete]
        public decimal? HoursOfOperation { get; set; }


        [MetisMetadataDataType("decimal"),Validators("number"), Label("SF Leased"), Copyable, Obsolete]
        public decimal? SFLeased { get; set; }


        [Label("Lease Exp Date"), Copyable, Obsolete]
        public DateTimeOffset? LeaseExpiration { get; set; }

        [Label("Lease Notice Date"), Copyable, Obsolete]
        public DateTimeOffset? LeaseNotice { get; set; }

        [Label("Listed For Sale"), Copyable, Obsolete]
        public bool? ListedForSale { get; set; }

        [Label("Target Sale Date"), Copyable, Obsolete]
        public DateTimeOffset? TargetSale { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Contract Annual Rent"), Copyable, Obsolete]
        public decimal? ContractAnnualRent { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Annual Rent PSF"), Copyable, Obsolete]
        public decimal? AnnualRentPSF { get; set; }

        //public string LeaseType { get; set; }
        [Copyable, Obsolete]
        public string OtherLeaseTerms { get; set; }
        [Copyable, Obsolete]
        public string RenewalOptions { get; set; }

        [Label("Status"), Copyable, Obsolete]
        public string Tlh_Status { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Asking Price"), Copyable, Obsolete]
        public decimal? AskingPrice { get; set; }


        // ------------------------------------





        [Copyable, Obsolete]
        public int? ExtCorId { get; set; }

        [Label("Strategy Asset Num(s)"), Copyable, Obsolete]
        public string Trimont_StrategyAssetNum { get; set; }

        [Label("Cost Basis for Building"), Copyable, Obsolete]
        public decimal? CostBasis { get; set; }

        [NotMapped]
        public int? ProjectId { get; set; }
        [NotMapped]
        public bool IsPublic { get; set; }


        [MetisMetadataDataType("MetisUserId")]
        public string AssignedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> AssignedMetisUserIdsList => this.AssignedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.AssignedMetisUserIds);

        [MetisMetadataDataType("MetisUserId")]
        public string SubscribedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> SubscribedMetisUserIdsList => this.SubscribedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.SubscribedMetisUserIds);

        public string TagList { get; set; }

        public string ProfileImageUrl { get; set; }
    }
}