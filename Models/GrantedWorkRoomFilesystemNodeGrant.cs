using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class GrantedWorkRoomFilesystemNodeGrant : DataObjectBase, IWorkRoomFilesystemNodeGrant
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GrantedWorkRoomFilesystemNodeGrantId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return GrantedWorkRoomFilesystemNodeGrantId; }
            set { GrantedWorkRoomFilesystemNodeGrantId = value; }
        }

        public int? FilesystemNodeId { get; set; }
        public string TagText { get; set; }
        public int OwnedByWorkRoomId { get; set; }
        public int? GrantedToWorkRoomId { get; set; }
        public bool Readable { get; set; }
        public bool Shareable { get; set; }
        public bool Contributable { get; set; }
        [Obsolete("Cannot delete this prop because we first must drop index that uses it")]
        public bool RestrictDownloadable { get; set; }
        public int? GrantedToMetisUserId { get; set; }
    }
}