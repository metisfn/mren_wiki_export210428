/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using Metis.Core.Address;
using Metis.Core.Calendar;
using Metis.Core.Conversations.Interfaces;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.FileRepository.Interfaces;
using Metis.Core.Permissions.Interfaces;
using Metis.Core.Process.Interfaces;

namespace Valuation.Logic.Models
{
    public interface IBaseDataContext : IMetisDataContext, IMetisScaffold, IMetadataDataContext,
        IConversationDataContext, ICalendarDataContext, IFilesystemRepositoryContext, IPermissionDataContext,
        IProcessDataContext, IAddressDataContext
    {
    }
}