﻿using Autofac;
using Metis.Core.DataObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public interface IPerfDataContext
    {
        IQueryable<PerfTest> PerfTests { get; }
    }

    public class PerfDataContext : MetisDataContextBase, IPerfDataContext
    {
        public PerfDataContext(string connectionName) : this(null, null, null, connectionName)
        {
            Database.CommandTimeout = int.MaxValue;
        }

        public PerfDataContext(string connString, IEnumerable<IMetisDbContextPreSavePiplineAction> preSaveActions,
            IEnumerable<IMetisDbContextPostSavePiplineAction> postSaveActions, IComponentContext afContext)
            : base(connString, preSaveActions, postSaveActions, afContext) { }

        public PerfDataContext(IEnumerable<IMetisDbContextPreSavePiplineAction> preSaveActions,
            IEnumerable<IMetisDbContextPostSavePiplineAction> postSaveActions, IComponentContext afContext, string connectionName = "PerfResultsConnection")
	        : this(connectionName, preSaveActions, postSaveActions, afContext)
        {
            Database.CommandTimeout = int.MaxValue;
        }

        public PerfDataContext() : this(null, null, null)
        {
            Database.CommandTimeout = int.MaxValue;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            Database.CommandTimeout = int.MaxValue;
        }

        public DbSet<PerfTest> PerfTests { get; set; }

        IQueryable<PerfTest> IPerfDataContext.PerfTests
        {
            get { return PerfTests; }
        }
    }
}
