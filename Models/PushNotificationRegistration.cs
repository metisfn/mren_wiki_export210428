﻿using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Microsoft.Azure.NotificationHubs;

namespace Valuation.Logic.Models
{
    public class PushNotificationRegistration : DataObjectBase
    {
        public PushNotificationRegistration()
        {
        }

        public PushNotificationRegistration(AppleRegistrationDescription registration, int metisUserId)
        {
            DeviceHandle = registration.DeviceToken;
            MetisUserId = metisUserId;
            HubRegistrationId = registration.RegistrationId;
        }

        public int PushNotificationRegistrationId { get; set; }


        public int MetisUserId { get; set; }

        //unique for a given device (iPhone or iPad or Android phone)
        public string DeviceHandle { get; set; }

        //should be only one for a given device for a given hub.
        public string HubRegistrationId { get; set; }

        //We need this tag to fire notifications through Azure's hub
        [NotMapped]
        public string Tag
        {
            get { return "metisuserid:" + MetisUserId; }
        }

        public override int PrimaryKey
        {
            get { return PushNotificationRegistrationId; }
            set { PushNotificationRegistrationId = value; }
        }
    }
}