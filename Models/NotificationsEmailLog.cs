﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using System;

namespace Valuation.Logic.Models
{
    public partial class NotificationsEmailLog : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationEmailLogId { get; set; }

        public int UserId { get; set; }

        public string BaseUrl { get; set; }

        [Obsolete]
        public bool HasBeenEmailed { get; set; }
        public bool HasBeenSent { get; set; }

        public string UnreadChatDto { get; set; }

        public int NotificationJobLogId { get; set; }

        public string Type { get; set; }
        public string DeliveryType { get; set; }

        public override int PrimaryKey
        {
            get { return NotificationEmailLogId; }
            set { NotificationEmailLogId = value; }
        }

        public string EmailItemType { get; set; }
        public string EmailItemPrimaryKeyIds { get; set; }
    }
}