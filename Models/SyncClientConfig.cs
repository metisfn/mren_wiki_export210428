﻿using Metis.Core.DataObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public class SyncClientConfig : DataObjectBase
    {
        [JsonIgnore]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SyncClientConfigId { get; set; }
        [JsonIgnore]
        public override int PrimaryKey
        {
            get { return SyncClientConfigId; }
            set { SyncClientConfigId = value; }
        }

        // this will come from the current user binding
        [NotMapped]
        public bool IsLoggedIn { get; set; }
        public bool DisableSync { get; set; }
        public int? IntervalInSeconds { get; set; }
    }
}
