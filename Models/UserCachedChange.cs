﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class UserCachedChanges : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserCachedChangesId { get; set; }

        public override int PrimaryKey
        {
            get { return UserCachedChangesId; }
            set { UserCachedChangesId = value; }
        }

        public int? MetisUserId { get; set; }
        public string CachedData { get; set; }
        public string BrowserCacheId { get; set; }
    }
}