/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class DataLookup : DataObjectBase, IDataLookup, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 DataLookupId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return DataLookupId; } set { DataLookupId = value; } }
		public virtual ICollection<DataLookupValue> Values { get; set; }
		#region IDataLookup.Values
		IEnumerable<IDataLookupValue> IDataLookup.Values { get { return this.Values; } set { this.Values = new Collection<DataLookupValue>(value.Cast<DataLookupValue>().ToList()); } }
		void IDataLookup.AddValue(IDataLookupValue toAdd) 
		{
			if(this.Values == null)
			{
				this.Values = new List<DataLookupValue>(); 
			}
			this.Values.Add((DataLookupValue)toAdd); 
		}
		IDataLookupValue IDataLookup.CreateValue() { return new DataLookupValue(); }
		#endregion
		public String DataLookupName { get; set; }
		public Int32? ConfigurationId { get; set; }
		public String ParentLookupName { get; set; }
	}
}
		