﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public partial class WorkRoomInvite
    {
        public const string PERMISSION_SUGGEST_TEAM = "SuggestTeam";

        public const string STATUS_DEFERRED = "Deferred";
        public const string STATUS_PENDING_SUGGESTION = "Pending";
        public const string STATUS_DISCONNECTED = "Disconnected";
        public const string STATUS_SENT = "Sent";
        public const string STATUS_SENT_EMAIL = "SentEmail";        
        public const string STATUS_INVITE_READ = "Read";
        public const string STATUS_VIEWED = "MRoom Viewed";
        public const string STATUS_RESENT = "Resent";
    }
}
