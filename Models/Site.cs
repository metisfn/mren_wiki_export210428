﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public interface ISite : IPermissionRoot, IFilesystemOwner
    {
        int SiteId { get; set; }
    }

    public partial class Site : DataObjectBase, ISite, IPermissionRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SiteId { get; set; }

        public override int PrimaryKey
        {
            get { return SiteId; }
            set { SiteId = value; }
        }

        [NotMapped]
        public int PermissionRootId
        {
            get { return SiteId; }
            set { SiteId = value; }
        }


        public string PermissionRootName { get; set; }

        public int? RootFilesystemNodeId { get; set; }
    }
}