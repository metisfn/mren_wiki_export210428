﻿using System;

namespace Valuation.Logic.Models
{
    public class LabelAttribute : Attribute
    {
        public string Label { get; set; }

        public LabelAttribute(string label)
        {
            Label = label;
        }
    }
}