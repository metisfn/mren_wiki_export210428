﻿using System;
using System.Linq;
using Metis.Core.DataObjects;
using Metis.Core.Permissions;

namespace Valuation.Logic.Models
{
    public class ValuationPermStrings : PermStringProvider
    {
        public static string PLAYER_OWNER = "OwnerPlayer";
        public static string PLAYER_INVOLVED = "InvolvedPlayer";
        public static string PLAYER = "Player";

        public ValuationPermStrings(IMetisDataContext db)
            : base(db)
        {
        }

        public string Function(SiteFunctions function)
        {
            return Enum.GetName(typeof (SiteFunctions), function);
        }
    }

    public enum SiteFunctions
    {
        ViewUserAdmin,
        ViewContactListings,

        InviteSiteAdmin,
        InviteReadonly,

        MakeUser,
        InviteGeneral
    }

    public static class SiteFunctionsExtensions
    {
        public static ProfileBuilderTargetType SiteFunctions<TProfileTarget>(
            this ProfileBuilder<TProfileTarget> builder, params SiteFunctions[] functions)
            where TProfileTarget : IMetisDataObject
        {
            var perms = functions.Select(function => Enum.GetName(typeof (SiteFunctions), function)).ToArray();
            return builder.TargetType<Site>()
                .Perm(perms);
        }
    }
}