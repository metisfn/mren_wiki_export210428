﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using Metis.Core.DataObjects;
using Valuation.Logic.Providers;

namespace Valuation.Logic.Models
{
    public class WorkRoomEdge : DataObjectBase
    {

        public WorkRoomEdge()
        {
            
        }

        public WorkRoomEdge(int parentWorkRoomId, int workRoomId)
        {
            ParentWorkRoomId = parentWorkRoomId;
            WorkRoomId = workRoomId;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRoomEdgeId { get; set; }
        public int? ParentWorkRoomId { get; set; }
        public int WorkRoomId { get; set; }

        public override int PrimaryKey
        {
            get { return WorkRoomEdgeId; }
            set { WorkRoomEdgeId = value; }
        }

        public static void Create(IValuationDataContext db, int? parentWorkRoomId, int workRoomId)
        {
            var found = MetisCaches.WorkRoomEdges.Get().FirstOrDefault(e => e.ParentWorkRoomId == parentWorkRoomId && e.WorkRoomId == workRoomId);

            if (found != null) return;

            found = new WorkRoomEdge { ParentWorkRoomId = parentWorkRoomId, WorkRoomId = workRoomId };

            db.AddToContext(found);
            db.SaveChanges();

            MetisCaches.WorkRoomEdges.Add(found);
        }

        public static void Delete(IValuationDataContext db, int? parentWorkRoomId, int workRoomId)
        {
            var edgesToDelete = MetisCaches.WorkRoomEdges.Get().Where(e => e.ParentWorkRoomId == parentWorkRoomId && e.WorkRoomId == workRoomId)
                    .ToList();

            foreach (var edge in edgesToDelete)
            {
                edge.Delete();
                db.AddToContext(edge);
                MetisCaches.WorkRoomEdges.Remove(edge);
            }
            db.SaveChanges();
        }
    }

    public static class WorkRoomEdgeExtensions
    {
        public static IList<WorkRoomEdge> GetReciprocalEdges(this IList<WorkRoomEdge> theseEdges)
        {
            return theseEdges.Where(
                    e =>
                        theseEdges.Any(
                            e2 => e2.ParentWorkRoomId == e.WorkRoomId && e2.WorkRoomId == e.ParentWorkRoomId)).ToList();
        }

        public static WorkRoomEdge FindReciprocal(this IList<WorkRoomEdge> theseEdges, WorkRoomEdge edgeToMatch)
        {
            return
                theseEdges.FirstOrDefault(
                    e => e.ParentWorkRoomId == edgeToMatch.WorkRoomId && e.WorkRoomId == edgeToMatch.ParentWorkRoomId);
        }
    }
}