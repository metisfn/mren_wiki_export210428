﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using Valuation.Logic.DTOs;

namespace Valuation.Logic.Models
{
    [ODataPath("GrayLabels")]
    public partial class GrayLabel : DataObjectBase, ICustomMetaOwner, IProfileImageOwner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GrayLabelId { get; set; }

        public string Subdomain { get; set; }
        public string CompanyName { get; set; }

        public override int PrimaryKey
        {
            get { return GrayLabelId; }
            set { GrayLabelId = value; }
        }

        // the de facto 'superorg'
        public int? OrganizationId { get; set; }
        [ForeignKey("OrganizationId"), JsonIgnore]
        public virtual Organization Organization { get; set; }

        public string CustomHtml { get; set; }
        public string CustomMeta { get; set; }
        [NotMapped, JsonIgnore]
        public IList<CustomMeta> CustomMetaList { get; set; }
        public string ProjectTableColumns { get; set; }
        public string CustomLookups { get; set; }
        public string KanbanOpts { get; set; }
        public string Support { get; set; }

        public string Description { get; set; }

        public string DefaultUITheme { get; set; }
        public string DefaultMapMode { get; set; }
        public string DefaultUIBladeMode { get; set; }
        public string DefaultMapId { get; set; }
        public string NavbarBannerSource { get; set; }
        public string NavbarBannerSourceLight { get; set; }
        public string PageBgSource { get; set; }
        public bool AllOrgsAreHosts { get; set; }
        public string LoginImageSource { get; set; }
        public string SideImageSource { get; set; }
        public string EmailBannerSource { get; set; }
        public int? GalleryId { get; set; }
        [ForeignKey("GalleryId"), JsonIgnore]
        public virtual Gallery Gallery {get; set;}
        public string MiscSettings { get; set; }
        //In minutes
        public int TimeoutDuration { get; set; }

        public string CustomNomenclature { get; set; }

        public int? ProjectDefaultFoldersRootFsnId { get; set; }
        public int? AssetDefaultFoldersRootFsnId { get; set; }
        public string ProfileImageUrl { get; set; }

        public bool V2Enabled { get; set; }
    }

    public class GrayLabelOrganization : DataObjectBase
    {
        public int GrayLabelOrganizationId { get; set; }
        public int OrganizationId { get; set; }
        public int GrayLabelId { get; set; }

        [ForeignKey("OrganizationId"), JsonIgnore]
        public virtual Organization Organization { get; set; }

        [ForeignKey("GrayLabelId"), JsonIgnore]
        public virtual GrayLabel GrayLabel { get; set; }

        public override int PrimaryKey
        {
            get { return GrayLabelOrganizationId; }
            set { GrayLabelOrganizationId = value; }
        }
    }

    public class GrayLabelNdaAcceptance : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GrayLabelNdaAcceptanceId { get; set; }
        public int GrayLabelId { get; set; }
        public int MetisUserId { get; set; }

        [ForeignKey("GrayLabelId"), JsonIgnore]
        public virtual GrayLabel GrayLabel { get; set; }

        [ForeignKey("MetisUserId"), JsonIgnore]
        public virtual MetisUser MetisUser { get; set; }

        public override int PrimaryKey
        {
            get { return GrayLabelNdaAcceptanceId; }
            set { GrayLabelNdaAcceptanceId = value; }
        }
    }

    public partial class GrayLabel
    {
        public const int CONNECT_GL_ID = 79;
        //Defaults
        public const string DEFAULT_LOGIN_HTML = "<div class=\"\">" +
                                          "<div class=\"col-xs-12 col-md-6 col-md-offset-3\" style=\"margin-top:1vh;\">" +
                                          "<img src=\"{0}\" style=\"display:block; margin:auto; max-width:100%; max-height:20vh; padding-top:2vh; padding-bottom:2vh; \" />" +
                                          "</div>" +
                                          "</div>";
        public const string DEFAULT_BACKGROUND_SRC = "mren.jpg";
        public const string DEFAULT_LOGIN_SRC = "mren.png";
        public const string DEFAULT_NAVBAR_BANNER_SRC = "mren.png";
        public const string DEFAULT_NAVBAR_BANNER_SRC_LIGHT = "mren_light.png";
        public const string DEFAULT_SIDE_IMG_SRC = "https://cdn.storage.mren.com/ncd-dev/7fb338ee-9edb-434e-b675-5ffaaf3e18ee";
        public const string DEFAULT_EMAIL_BANNER_SRC = "https://cdn.storage.mren.com/ncd-dev/0a51e1ac-a530-4444-a1d6-68de946d13ae";

        //Paths
        public const string PATH_NAVBAR_BANNER = "/assets/images/";
        public const string PATH_BACKGROUND = "/assets/images/backgrounds/";
        public const string PATH_LOGIN = "/assets/images/logos/";

        //V2 Stuff

        public const string DEFAULT_V2_CUSTOM_META = @"
            [
	            {
		            ""targetType"": ""Project"",
		            ""customFields"": [
			            {
				            ""id"": ""G13"",
				            ""name"": ""Label"",
				            ""label"": ""Status"",
				            ""type"": ""string"",
				            ""section"": ""OS1"",
				            ""isInternal"": false,
				            ""isServerTrue"": null,
				            ""hideNull"": null,
				            ""txtAreaOptions"": null,
				            ""options"": [
					            {
						            ""value"": ""To Do"",
                                    ""label"": ""To Do"",
                                    ""order"": 0
					            },
					            {
						            ""value"": ""Doing"",
                                    ""label"": ""Doing"",
                                    ""order"": 1
					            },
					            {
						            ""value"": ""Done"",
                                    ""label"": ""Done"",
                                    ""order"": 2
					            }
				            ],
				            ""isUnitOf"": null,
				            ""sortOrder"": -100,
				            ""statusCode"": null
			            },
			            {
				            ""id"": ""G103"",
				            ""name"": ""OfferDescription"",
				            ""label"": ""Description"",
				            ""type"": ""string"",
				            ""section"": ""OS1"",
				            ""isInternal"": false,
				            ""isServerTrue"": true,
				            ""hideNull"": null,
				            ""txtAreaOptions"": {
					            ""rows"": 10
				            },
				            ""options"": null,
				            ""isUnitOf"": null,
				            ""sortOrder"": -12,
				            ""statusCode"": null
			            },
			            {
				            ""id"": ""G200"",
				            ""name"": ""CalendarDate"",
				            ""label"": ""Date"",
				            ""type"": ""date"",
				            ""section"": ""OS1"",
				            ""isInternal"": true,
				            ""isServerTrue"": true,
				            ""hideNull"": null,
				            ""txtAreaOptions"": null,
				            ""options"": null,
				            ""isUnitOf"": null,
				            ""sortOrder"": -21,
				            ""statusCode"": null
			            }
		            ],
		            ""sections"": [
			            {
				            ""id"": ""OS1"",
				            ""name"": ""Summary"",
				            ""isInternal"": false,
				            ""sortOrder"": 0
			            }
		            ],
		            ""extraFields"": {
			            ""internal"": null,
			            ""public"": null
		            }
	            },
	            {
		            ""targetType"": ""Asset"",
		            ""customFields"": [
			            {
				            ""id"": ""G19"",
				            ""name"": ""G19_Status"",
				            ""label"": ""Status"",
				            ""type"": ""string"",
				            ""section"": ""OS1"",
				            ""isInternal"": false,
				            ""isServerTrue"": null,
				            ""hideNull"": null,
				            ""txtAreaOptions"": null,
				            ""options"": [
					            {
						            ""value"": ""To Do"",
                                    ""label"": ""To Do"",
                                    ""order"": 0
					            },
					            {
						            ""value"": ""Doing"",
                                    ""label"": ""Doing"",
                                    ""order"": 1
					            },
					            {
						            ""value"": ""Done"",
                                    ""label"": ""Done"",
                                    ""order"": 2
					            }
				            ],
				            ""isUnitOf"": null,
				            ""sortOrder"": -100,
				            ""statusCode"": null
			            },
			            {
				            ""id"": ""G200"",
				            ""name"": ""CalendarDate"",
				            ""label"": ""Date"",
				            ""type"": ""date"",
				            ""section"": ""OS1"",
				            ""isInternal"": true,
				            ""isServerTrue"": true,
				            ""hideNull"": null,
				            ""txtAreaOptions"": null,
				            ""options"": null,
				            ""isUnitOf"": null,
				            ""sortOrder"": -21,
				            ""statusCode"": null
			            }
		            ],
		            ""sections"": [
			            {
				            ""id"": ""OS1"",
				            ""name"": ""Summary"",
				            ""isInternal"": false,
				            ""sortOrder"": 0
			            }
		            ],
		            ""extraFields"": {
			            ""internal"": null,
			            ""public"": null
		            }
	            }
            ]";
            

        [NotMapped, ForSeed]
        public bool RemoveMarket { get; set; }
        [NotMapped, ForSeed]
        public bool RemoveNda { get; set; }
        [NotMapped, ForSeed]
        public bool RemoveDealSummarySettings { get; set; }
        [NotMapped, ForSeed]
        public bool EnableInvestorFeature { get; set; }
        [NotMapped, ForSeed]
        public bool AllowDealNotesByMRoom { get; set; }
        [NotMapped, ForSeed]
        public bool HideNewUserRegistration { get; set; }
        [NotMapped, ForSeed]
        public bool ExposeMetisUserId { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> GrayLabelOrganizationIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> HostOrganizationIds { get; set; }
        [NotMapped]
        public bool NeedsToSignNda { get; set; }
        [NotMapped, MetaIgnore]
        public virtual IDictionary<string, string> MiscSettingsDict { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> ActiveMetisUserIds { get; set; }
    }

    //Only needed for the inital seeding of the gray label configs to the database. 
    public class ForSeedAttribute : Attribute
    {
    }
}