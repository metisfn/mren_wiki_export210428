﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public partial class WorkRoom
    {
        public const string PERMISSION_GROUPING_WORKROOM_CONVERSATION = "MainWorkRoomConversation";
        public const string PERMISSION_GROUPING_INVITED_USERS = "InvitedUsers";
        public const string PERMISSION_GROUPING_CHILD_ROOMS = "ChildRooms";
        public const string PERMISSION_GROUPING_PARTICIPATING_POSTS = "ParticipatingPosts";
        public const string PERMISSION_GROUPING_POST_NDA_MROOM = "PostNDAMRoom";

        //public const string PERMISSION_NDA_READ = "NdaRead";
        public const string PERMISSION_INVITED_USER = "InvitedUser";
        public const string PERMISSION_CREATE_CHILD = "CreateChild";
        public const string PERMISSION_CREATER = "Creater";
        public const string PERMISSION_TENANT = "Tenant";
        public const string PERMISSION_CREATE_CONFERENCE = "CreateConference";
        public const string PERMISSION_CREATE_BREAKOUT = "CreateBreakout";

        public const string WORKROOM_TYPE_TEAM_MROOM = "TeamMroom";
        public const string WORKROOM_TYPE_TWO_PARTY = "TwoParty"; 
        public const string WORKROOM_TYPE_OPEN = "Open";
        public const string WORKROOM_TYPE_POST_NDA = "PostNDA";
        public const string WORKROOM_TYPE_PRE_NDA = "PreNDA";

        public const string WORKROOM_TYPE_BREAKOUT = "Breakout";
        public const string WORKROOM_TYPE_CONFERENCE = "Conference";
        public const string WORKROOM_TYPE_POST = "Post";

        public const string INVITE_MODE_TEAM = "Team";
        public const string INTITE_MODE_ANYONE = "Anyone";

        public const UserActivityType WORKROOM_ACTION_INVITE = UserActivityType.InviteToWorkRoom;
        public const UserActivityType WORKROOM_ACTION_ENTER = UserActivityType.EnterWorkRoom;
        public const UserActivityType WORKROOM_ACTION_LEAVE = UserActivityType.LeaveWorkRoom;
        public const UserActivityType WORKROOM_ACTION_REMOVE = UserActivityType.RemoveFromWorkRoom;

        public static ICollection<WorkRoom> MapEdges(ICollection<WorkRoom> workRooms, ICollection<WorkRoomEdge> edges)
        {
            var edgesByWr = edges.ToLookup(wre => wre.WorkRoomId);
            var edgesByParent = edges.ToLookup(wre => wre.ParentWorkRoomId);


            var final = workRooms.ToList().Select(w =>
            {
                w.ParentWorkRoomIds = 
                    edgesByWr[w.WorkRoomId].Where(e => e.ParentWorkRoomId.HasValue).Select(e => e.ParentWorkRoomId).Distinct().ToList();

                w.ChildWorkRoomIds =
                    edgesByParent[w.WorkRoomId].Select(e => e.WorkRoomId).Distinct().ToList();

                return w;
            }).ToList();

            return final;
        }
    }
}
