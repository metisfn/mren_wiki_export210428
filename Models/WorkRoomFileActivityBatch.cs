﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class WorkRoomFileActivityBatch : DataObjectBase, IConcerning
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRoomFileActivityBatchId { get; set; }
        public override int PrimaryKey
        {
            get { return WorkRoomFileActivityBatchId; }
            set { WorkRoomFileActivityBatchId = value; }
        }
        public int ConcerningPrimaryKeyId { get; set; }
        public string ConcerningTargetType { get; set; }
        public Guid ConcerningObjectId { get; set; }
        public Guid? ConcerningRevisionId { get; set; }
        public string Step { get; set; }
    }

    public partial class WorkRoomFileActivityBatch
    {
        public const string STEP_QUEUED = "Queued";
        public const string STEP_LISTENING = "Listening";
        public const string STEP_PROCESSING = "Processing";
        public const string STEP_COMPLETED = "Completed";
        public const string STEP_FAILED= "Failed";
    }
}
