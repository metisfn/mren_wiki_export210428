﻿using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("OrganizationInvites")]
    public partial class OrganizationInvite
    {
        public const string STATE_PENDING = "Pending";
        public const string STATE_ACCEPTED = "Accepted";
        public const string STATE_REJECTED = "Rejected";
        public const string STATE_REVOKED = "Revoked";
        public const string STATE_DEFERRED = "Deferred";
        public const string STATE_READ = "Read";
    }
}