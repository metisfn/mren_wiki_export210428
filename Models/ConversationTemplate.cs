/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.Conversations.Interfaces;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
    public class ConversationTemplate : DataObjectBase, IConversationTemplate, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConversationTemplateId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ConversationTemplateId; }
            set { ConversationTemplateId = value; }
        }

        public string Name { get; set; }
        public string Template { get; set; }
    }
}