﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("RelationshipLinks")]
    public class RelationshipLink : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RelationshipLinkId { get; set; }

		[NotMapped]
		public override int PrimaryKey
        {
			get { return RelationshipLinkId; }
			set { RelationshipLinkId = value; }
        }


		public int RelationshipId { get; set; }
		[ForeignKey("RelationshipId"),MetaIgnore,NotMapped]
		public virtual Relationship Relationship { get; set; }


		//The thing this link is about (NOTE: do not confuse this with the pkId and ctt on the relationship itself, which represent the other person/org in the relationship with this MetisUser)
		public int TargetPrimaryKeyId { get; set; }
		public string ConcerningTargetType { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Notes { get; set; }
        public string Role { get; set; }
    }
}