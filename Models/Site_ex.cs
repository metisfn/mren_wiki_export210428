﻿namespace Valuation.Logic.Models
{
    public partial class Site
    {
        public const string PERMISSION_GROUPING_USER_ADMIN = "UserAdmin";
        public const string PERMISSION_GROUPING_PUBLIC_OPPORTUNITIES = "PublicOpportunities";
        public const string PERMISSION_GROUPING_ACTIVE_DEALS = "ActiveDeals";
        public const string PERMISSION_GROUPING_ARCHIVED_DEALS = "ArchivedDeals";
    }
}