﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("ProjectGrants")]
    public class ProjectGrant : DataObjectBaseWithHistory<ProjectGrant>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectGrantId { get; set; }

        public int ProjectId { get; set; }
        public int FromOrganizationId { get; set; }
        public int? ToOrganizationId { get; set; }
        public int? ToWorkRoomId { get; set; } 
        public bool Contributable { get; set; }
        public bool Sharable { get; set; }
        public bool Readable { get; set; }

        public override int PrimaryKey
        {
            get { return ProjectGrantId; }
            set { ProjectGrantId = value; }
        }
    }

    public class ProjectGrantRequest
    {
        public int fromOrgId { get; set; }
        public int? toOrgId { get; set; }
        public int? toWorkRoomId { get; set; }
        public int projectId { get; set; }
        public string grant { get; set; }
    }
}