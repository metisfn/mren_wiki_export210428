﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class UserSubscription : DataObjectBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserSubscriptionId { get; set; }

        public int MetisUserId { get; set; }

        [ForeignKey("MetisUserId")]
        public MetisUser MetisUser { get; set; }

        public string SubscriberId { get; set; }
        public string UserId { get; set; }

        public bool Subscribed { get; set; }

        public override int PrimaryKey
        {
            get { return UserSubscriptionId; }
            set { UserSubscriptionId = value; }
        }
    }
}