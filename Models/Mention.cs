﻿using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Valuation.Logic.Models
{
    [ODataPath("Mentions")]
    public class Mention : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MentiondId { get; set; }

        public int OwnerPrimaryKeyId { get; set; }
        public string OwnerTargetType { get; set; }

        public int ConcerningPrimaryKeyId { get; set; }

        public string ConcerningTargetType { get; set; }

        public string BackupText { get; set; }

        public override int PrimaryKey
        {
            get { return MentiondId; }
            set { MentiondId = value;  }
        }
    }
}
