﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using Valuation.Logic.Attributes;
using Valuation.Logic.DTOs;

namespace Valuation.Logic.Models
{
    public partial class Project : DataObjectBaseWithHistory<Project>, IFilesystemOwner, IGalleryOwner, INdaOwner, ICustomFieldsOwner, ISubscribable, ITagListOwner, IProfileImageOwner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ShellProperty]
        public int ProjectId { get; set; }

        [MetisMetadataDataType("bladeUrlPart"), ShellProperty]
        public string Name { get; set; }

        [Label("Date"), Copyable] // ERL 2017.07.03 utility date field for use with the calendar
        public DateTimeOffset? CalendarDate { get; set; }

        [DefaultValue(STATUS_ON_MARKET)]
        public string Status { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), CustomField("G109", "TargetFundRaise", "Target Fund Raise", "Summary", "dollar", -7, false, new[] { "number" }, false, "true", glIdsToIgnore: new[] { 109 })]
        public decimal? TargetFundRaise { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Appraised/Internal Value"), Copyable, CustomField("G111", "Value", "Appraised/Internal Value", "Summary", "dollar", -5, false, new[] { "number" }, false, "true", glIdsToIgnore: new[] { 109 })]
        public decimal? Value { get; set; }

        public bool ShowProgress { get; set; }

        [CustomField("G102", "TagLine", "Tag Line", "Summary", "string", -13, false, null, false, "true", glIdsToIgnore: new[] { 109 })]
        public string TagLine { get; set; }
        public bool IsPublic { get; set; }

        [DefaultValue(MARKETVISIBILITY_PRIVATE),ShellProperty]
        public string MarketVisibility { get; set; }

        [DefaultValue(true)]
        public bool IsOpportunity { get; set; }

        [ShellProperty]
        public int OrganizationId { get; set; }

        [ForeignKey("OrganizationId")]
        public virtual Organization Organization { get; set; }

        [ForeignKey("RootFilesystemNodeId")]
        public virtual FilesystemNode RootFilesystemNode { get; set; }

        [MetisLookup("AssetType"), Copyable, CustomField("G112", "AssetType", "Asset Type", "Summary", "string", -8, false, null, false, "true", glIdsToIgnore: new[] { 109 })]
        public string AssetType { get; set; }

        [Copyable]
        public string ParcelNumber { get; set; }

        [Label("# of Units"), Validators("number"), Copyable]
        public int NumUnits { get; set; }

        [MetisRequired, Label("Status"), MetisLookup("ProjectLabel")]
        public string Label { get; set; }

        [MetisMetadataDataType("decimal"),Validators("number"), Copyable, CustomField("G106", "Size", "Size", "Summary", "string", -10, false, null, false, "true", glIdsToIgnore: new[] { 109 })]
        public decimal? Size { get; set; }

        [MetisRequired, MetisLookup("AssetSizeType"), Copyable, CustomField("G107", "SizeType", "Size Type", "Summary", "string", -9, false, null, false, "true", "G106", glIdsToIgnore: new[] { 109 })]
        public string SizeType { get; set; }

        [MetisRequired, MetisLookup("ProjectDealType"), Copyable, CustomField("G101", "DealType", "Deal Type", "Summary", "string", -14, false, null, false, glIdsToIgnore: new[] { 109 })]
        public string DealType { get; set; }

        [Label("Contact(s)"), MetisMetadataDataType("MetisUserId"), CustomField("G100", "ContactUserIds", "Contact(s)", "Summary", "MetisUserId", -15, false)]
        public string ContactUserIds { get; set; }

        [CustomField("G110", "Deadline", "Deadline", "Summary", "date", -6, false, null, false, "true", glIdsToIgnore: new[] { 109 })]
        public DateTimeOffset? Deadline { get; set; }

        [Copyable, CustomField("G104", "Location", "Location", "Summary", "string", -11, false, null, false, "true", glIdsToIgnore: new[] { 109})]
        public string Location { get; set; }

        [Copyable, CustomField("G105", "State", "State", "Summary", "string", -10, false, null, false, "true")]
        public string State { get; set; }

        [Label("Description"), Copyable, CustomField("G103", "OfferDescription", "Description", "Summary", "string", -12, false, null, true, glIdsToIgnore: new[] { 109 })] // 2017-02-07 DJK: So front end knows to call this "Description" by default - but override if GL meta specifies. 
        public string OfferDescription { get; set; }

        [Copyable]
        public string FinancialHighlight { get; set; }

        [Copyable]
        public string MarketHighlight { get; set; }

        [Copyable]
        public string OfferInstructions { get; set; }

        [Copyable]
        public string OfferWelcomeMessage { get; set; }

        [Copyable]
        public double? Latitude { get; set; }

        [Copyable]
        public double? Longitude { get; set; }

        [ForeignKey("NdaNodeId")]
        public virtual FilesystemNode NdaNode { get; set; }

        public int PointPersonUserId { get; set; }
        public int? NdaNodeId { get; set; }
        public string NdaText { get; set; }

        [NotMapped]
        public bool InConsideration { get; set; }

        public virtual ICollection<Asset> Assets { get; set; }

        public override int PrimaryKey
        {
            get { return ProjectId; }
            set { ProjectId = value; }
        }

        public int? WorkRoomId { get; set; }

        [ForeignKey("WorkRoomId")]
        public virtual WorkRoom WorkRoom { get; set; }

        public int? OpenWorkRoomId { get; set; }

        [ForeignKey("OpenWorkRoomId")]
        public virtual WorkRoom OpenWorkRoom { get; set; }

        public int? PostNDAWorkRoomId { get; set; }

        [ForeignKey("PostNDAWorkRoomId")]
        public virtual WorkRoom PostNDAWorkRoom { get; set; }

        [DefaultValue(false)]
        public bool AcceptingOffers { get; set; }

        [DefaultValue(false)]
        public bool IsArchived { get; set; }

        [DefaultValue(false)]
        public bool IsDead { get; set; }

        [DefaultValue(false)]
        public bool NdaRequiredToOffer { get; set; }

        [Copyable]
        public string CustomData { get; set; }
        // Partner - multiuse for OPs, GPs, or just to reveal team info (e.g. if on public marketplace)
        [Label("Company")]
        public string PartnerName { get; set; }

        [Label("Website"), MetisMetadataDataType("link")]
        public string PartnerUrl { get; set; }

        [Label("Description")]
        public string PartnerDescription { get; set; }

        // capraise fields -- from A&A
        [MetisMetadataDataType("dollar"), Validators("number"), Copyable, Obsolete]
        public decimal? ProjectSize { get; set; }

        [Copyable, Obsolete]
        public string InitialTerm { get; set; }
        [Copyable, Obsolete]
        public string FirstExtension { get; set; }
        [Copyable, Obsolete]
        public string SecondExtension { get; set; }

        [MetisMetadataDataType("percentage"), Validators("number"), Copyable, Obsolete]
        public double? ProjectLeveredIRR { get; set; }

        [MetisMetadataDataType("percentage"), Validators("number"), Copyable, Obsolete]
        public double? InvestorNetLeveredIRR { get; set; }

        [Validators("number"), Copyable, Obsolete]
        public decimal? Multiple { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Copyable, Obsolete]
        public decimal? TotalEquity { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Copyable, Obsolete]
        public decimal? SeniorDebt { get; set; }

        // dashboard fields
        [MetisMetadataDataType("year"), Validators("year"), Copyable, Obsolete]
        public int YearBuilt { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Copyable, Obsolete]
        public decimal? PurchasePrice { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Copyable, Obsolete]
        public decimal? PurchasePricePerUnit { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("All-In Cost"), Copyable, Obsolete]
        public decimal? AllInCost { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("All-In Cost Per Unit"), Copyable, Obsolete]
        public decimal? AllInCostPerUnit { get; set; }

        [MetisMetadataDataType("percentage"), Validators("number"), Copyable, Obsolete]
        public double? Occupancy { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Copyable, Obsolete]
        public decimal? NOI { get; set; }

        [MetisMetadataDataType("percentage"), Validators("number"), Copyable, Obsolete]
        public double? CapRate { get; set; }

        [Copyable, Obsolete]
        public string Anchors { get; set; }
        [Copyable, Obsolete]
        public string EquityInvestor { get; set; }
        [Copyable, Obsolete]
        public string Lender { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("$ Invested"), Copyable, Obsolete]
        public decimal? Invested { get; set; }

        [MetisMetadataDataType("percentage"), Validators("number"), Copyable, Obsolete]
        public double? ExpectedIRR { get; set; }

        [Validators("number"), Copyable, Obsolete]
        public decimal? ExpectedMultiple { get; set; }

        [Copyable, Obsolete]
        public string Originator { get; set; }
        [Copyable, Obsolete]
        public string DealManager { get; set; }
        [Copyable, Obsolete]
        public string Underwriter { get; set; }
        [Copyable, Obsolete]
        public string Legal { get; set; }
        [Copyable, Obsolete]
        public DateTimeOffset? DateReceived { get; set; }

        [Label("Date PSA Executed"), Copyable, Obsolete]
        public DateTimeOffset? DatePSAExecuted { get; set; }

        [Copyable, Obsolete]
        public string LoanAmount { get; set; }
        [Copyable, Obsolete]
        public string InterestRate { get; set; }
        [Copyable, Obsolete]
        public string FutureFunding { get; set; }
        [Copyable, Obsolete]
        public string Maturity { get; set; }
        [Copyable, Obsolete]
        public DateTimeOffset? DateClosed { get; set; }
        [Copyable, Obsolete]
        public string Source { get; set; }
        [Copyable, Obsolete]
        public string Notes { get; set; }

        public int? InvestmentSummaryNodeId { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Offering Price"), Copyable, Obsolete]
        public decimal? OfferingPrice { get; set; }

        [Label("Interest Level"), Copyable, Obsolete]
        public int? InterestLevel { get; set; }

        [Label("Sourced By"), Obsolete]
        [Copyable]
        public string SourcedBy { get; set; }

        [Copyable, Obsolete]
        public string Neighborhood { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Price / Door"), Copyable, Obsolete]
        public decimal? PricePerDoor { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Gross Revenue"), Copyable, Obsolete]
        public decimal? GrossRevenue { get; set; }

        [Validators("number"), Label("Gross Rent Multiplier"), Copyable, Obsolete]
        public decimal? GrossRentMultiplier { get; set; }

        [ForeignKey("InvestmentSummaryNodeId"), Copyable, Obsolete]
        public virtual FilesystemNode InvestmentSummaryNode { get; set; }

        [Copyable]
        public string CustomFields { get; set; }
        //Investment area - Originally done for PineTree
        [MetisMetadataDataType("dollar"), Validators("number"), Label("NAV"), Copyable, Obsolete]
        public decimal? NetAssetValue { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Copyable, Obsolete]
        public decimal? TotalCapitalization { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Cash Flow"), Copyable, Obsolete]
        public decimal? CashFlow { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Gain/Loss"), Copyable, Obsolete]
        public decimal? GainLoss { get; set; }

        [MetisMetadataDataType("percentage"), Validators("number"), Label("Underwritten IRR"), Copyable, Obsolete]
        public decimal? UnderwrittenIRR { get; set; }

        [Validators("number"), Label("Underwritten Multiple"), Copyable, Obsolete]
        public decimal? UnderwrittenMultiple { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Committed"), Copyable, Obsolete]
        public decimal? CommittedTotal { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Invested"), Copyable, Obsolete]
        public decimal? InvestedTotal { get; set; }

        [MetisMetadataDataType("dollar"), Validators("number"), Label("Distributed"), Copyable, Obsolete]
        public decimal? DistributedTotal { get; set; }

        [ShellProperty]
        public bool IsActivatedUnderSubscription { get; set; }
        public virtual List<AssetShell> AssetShells { get; set; }
        public int? RootFilesystemNodeId { get; set; }

        [ForeignKey("GalleryId")]
        public virtual Gallery Gallery { get; set; }

        [ShellProperty]
        public int? GalleryId { get; set; }

        [ForeignKey("NdaId")]
        public virtual Nda Nda { get; set; }

        public int? NdaId { get; set; }

        [Copyable, Obsolete]
        public int? ExtCorId { get; set; }

        [Copyable, Obsolete]
        public int? AccountNumber { get; set; }

        [Label("Strategy Asset Num(s)"), Obsolete]
        public string Trimont_StrategyAssetNum { get; set; }

        [Copyable]
        public string CustomMeta { get; set; }

        [NotMapped, JsonIgnore]
        public IList<CustomMeta> CustomMetaList { get; set; }

        [NotMapped]
        public int GrayLabelId { get; set; }

        [MetisMetadataDataType("MetisUserId")]
        public string GuestMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> GuestMetisUserIdsList => this.GuestMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.GuestMetisUserIds);

        [MetisMetadataDataType("MetisUserId")]
        public string AssignedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> AssignedMetisUserIdsList => this.AssignedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.AssignedMetisUserIds);

        [MetisMetadataDataType("MetisUserId")]
        public string SubscribedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> SubscribedMetisUserIdsList => this.SubscribedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.SubscribedMetisUserIds);

        public string TagList { get; set; }

        public string ProfileImageUrl { get; set; }
    }

    public class AssetShell
    {
        public AssetShell()
        {
            AssetShellId = Guid.NewGuid();
        }

        [Key]
        public Guid AssetShellId { get; set; }

        public int AssetId { get; set; }
        public int OrganizationId { get; set; }
        public string Name { get; set; }
        public bool IsArchived { get; set; }
        public virtual Gallery Gallery { get; set; }
        public int? GalleryId { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? WorkRoomId { get; set; }

        public DateTimeOffset? CalendarDate { get; set; }
        public string Type { get; set; }
        public string ContactUserIds { get; set; }

    }
}