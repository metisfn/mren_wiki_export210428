﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class TagColor : DataObjectBase
    {
        private static readonly string[] _tagPallete =
        {
            "#006064",
            "#2196F3",
            "#43A047",
            "#F44336",
            "#673AB7",
            "#33691E",
            "#EF6C00",
            "#009688",
            "#795548",
            "#607D8B",
            "#3F51B5",
            "#E91E63",
            "#3E2723",
            "#C62828",
            "#311B92",
            "#BF360C",
            "#37474F ",
            "#4A148C",
            "#689F38",
            "#FF5722",
            "#004D40",
            "#AD1457"
        };

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TagColorId { get; set; }

        public override int PrimaryKey
        {
            get { return TagColorId; }
            set { TagColorId = value; }
        }

        [NotMapped]
        public string Color
        {
            get { return _tagPallete[TagColorId%_tagPallete.Length]; }
        }

        public DateTimeOffset? Expires { get; set; }

        public string Text { get; set; }
        public int MetisUserId { get; set; }
        public int? OrganizationId { get; set; }

        [ForeignKey("MetisUserId")]
        public virtual MetisUser MetisUser { get; set; }
    }
}