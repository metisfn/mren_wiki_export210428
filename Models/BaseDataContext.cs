/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Metis.Core.Address;
using Metis.Core.Calendar;
using Metis.Core.Conversations.Interfaces;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.FileRepository;
using Metis.Core.FileRepository.Interfaces;
using Metis.Core.Permissions;
using Metis.Core.Permissions.Interfaces;
using Metis.Core.Process.Interfaces;

namespace Valuation.Logic.Models
{
    public class BaseDataContext : MetisDataContextBase, IBaseDataContext
    {
        public BaseDataContext() : this(null, null, null)
        {
            Database.CommandTimeout = int.MaxValue;
        }

        public BaseDataContext(string connectionName) : this(null, null, null, connectionName)
        {
            Database.CommandTimeout = int.MaxValue;
        }

        public BaseDataContext(IEnumerable<IMetisDbContextPreSavePiplineAction> preSaveActions,
            IEnumerable<IMetisDbContextPostSavePiplineAction> postSaveActions, IComponentContext afContext,
            string connectionName = "DefaultConnection")
            : this(connectionName, preSaveActions, postSaveActions, afContext)

        {
            Database.CommandTimeout = int.MaxValue;
        }

        public BaseDataContext(string connString, IEnumerable<IMetisDbContextPreSavePiplineAction> preSaveActions,
            IEnumerable<IMetisDbContextPostSavePiplineAction> postSaveActions, IComponentContext afContext)
            : base(connString, preSaveActions, postSaveActions, afContext)
        {
        }

        public virtual DbSet<DataObject> DataObjects { get; set; }
        public virtual DbSet<DataColumn> DataColumns { get; set; }
        public virtual DbSet<DataLookup> DataLookups { get; set; }
        public virtual DbSet<DataLookupValue> DataLookupValues { get; set; }
        public virtual DbSet<ActivityLogEntry> ActivityLogEntries { get; set; }
        public virtual DbSet<MetisUser> MetisUsers { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Conversation> Conversations { get; set; }
        public virtual DbSet<ConversationItem> ConversationItems { get; set; }
        public virtual DbSet<ConversationItemReadStatus> ConversationItemReadStatus { get; set; }
        public virtual DbSet<ConversationItemDeliveryStatus> ConversationItemDeliveryStatus { get; set; }
        public virtual DbSet<ConversationSubscription> ConversationSubscriptions { get; set; }
        public virtual DbSet<ConversationTemplate> ConversationTemplates { get; set; }
        public virtual DbSet<CalendarEvent> CalendarEvents { get; set; }
        public virtual DbSet<CalendarEventOccurrence> CalendarEventOccurrences { get; set; }
        public virtual DbSet<FilesystemNode> FilesystemNodes { get; set; }
        public virtual DbSet<PermissionTarget> PermissionTargets { get; set; }
        public virtual DbSet<Site> Sites { get; set; }
        public virtual DbSet<PermissionGrant> PermissionGrants { get; set; }
        public virtual DbSet<PermissionGrouping> PermissionGroupings { get; set; }
        public virtual DbSet<DataObjectRevisionVisibility> DataObjectRevisionVisibilities { get; set; }
        public virtual DbSet<PermissionLink> PermissionLinks { get; set; }
        public virtual DbSet<UserPermissionCache> UserPermissionCaches { get; set; }
        public virtual DbSet<PermissionDataColumn> PermissionDataColumns { get; set; }
        public virtual DbSet<PermissionDataObject> PermissionDataObjects { get; set; }
        public virtual DbSet<PermissionProfileItem> PermissionProfileItems { get; set; }
        public virtual DbSet<PermissionProfileApplication> PermissionProfileApplications { get; set; }
        public virtual DbSet<PermissionProfileHistory> PermissionProfileHistories { get; set; }
        public virtual DbSet<ProcessFlow> ProcessFlows { get; set; }
        public virtual DbSet<ProcessInProgress> ProcessInProgresses { get; set; }
        public virtual DbSet<ProcessStep> ProcessSteps { get; set; }
        public virtual DbSet<ProcessStepAction> ProcessStepActions { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Entity<Address>().Property(a => a.Longitude).HasPrecision(18, 10);
            modelBuilder.Entity<Address>().Property(a => a.Latitude).HasPrecision(18, 10);
            Database.CommandTimeout = int.MaxValue;
        }

        #region Interface Queryables

        IQueryable<IDataObject> IMetadataDataContext.DataObjects
        {
            get { return DataObjects; }
        }

        IQueryable<IDataColumn> IMetadataDataContext.DataColumns
        {
            get { return DataColumns; }
        }

        IQueryable<IDataLookup> IMetadataDataContext.DataLookups
        {
            get { return DataLookups; }
        }

        IQueryable<IDataLookupValue> IMetadataDataContext.DataLookupValues
        {
            get { return DataLookupValues; }
        }

        IQueryable<IActivityLogEntry> IMetadataDataContext.ActivityLogEntries
        {
            get { return ActivityLogEntries; }
        }

        IQueryable<IMetisUser> IMetadataDataContext.MetisUsers
        {
            get { return MetisUsers; }
        }

        IQueryable<IContact> IMetadataDataContext.Contacts
        {
            get { return Contacts; }
        }

        IQueryable<IConversation> IConversationDataContext.Conversations
        {
            get { return Conversations; }
        }

        IQueryable<IConversationItem> IConversationDataContext.ConversationItems
        {
            get { return ConversationItems; }
        }

        IQueryable<IConversationItemReadStatus> IConversationDataContext.ConversationItemReadStatus
        {
            get { return ConversationItemReadStatus; }
        }

        IQueryable<IConversationItemDeliveryStatus> IConversationDataContext.ConversationItemDeliveryStatus
        {
            get { return ConversationItemDeliveryStatus; }
        }

        IQueryable<IConversationSubscription> IConversationDataContext.ConversationSubscriptions
        {
            get { return ConversationSubscriptions; }
        }

        IQueryable<IConversationTemplate> IConversationDataContext.ConversationTemplates
        {
            get { return ConversationTemplates; }
        }

        IQueryable<ICalendarEvent> ICalendarDataContext.CalendarEvents
        {
            get { return CalendarEvents; }
        }

        IQueryable<ICalendarEventOccurrence> ICalendarDataContext.CalendarEventOccurrences
        {
            get { return CalendarEventOccurrences; }
        }

        IQueryable<IFilesystemNode> IFilesystemRepositoryContext.FilesystemNodes
        {
            get { return FilesystemNodes; }
        }

        IQueryable<IPermissionTarget> IPermissionDataContext.PermissionTargets
        {
            get { return PermissionTargets; }
        }

        IQueryable<IPermissionRoot> IPermissionDataContext.PermissionRoots
        {
            get { return Sites; }
        }

        IQueryable<IPermissionGrant> IPermissionDataContext.PermissionGrants
        {
            get { return PermissionGrants; }
        }

        IQueryable<IPermissionGrouping> IPermissionDataContext.PermissionGroupings
        {
            get { return PermissionGroupings; }
        }

        IQueryable<IDataObjectRevisionVisibility> IPermissionDataContext.DataObjectRevisionVisibilities
        {
            get { return DataObjectRevisionVisibilities; }
        }

        IQueryable<IPermissionLink> IPermissionDataContext.PermissionLinks
        {
            get { return PermissionLinks; }
        }

        IQueryable<IUserPermissionCache> IPermissionDataContext.UserPermissionCaches
        {
            get { return UserPermissionCaches; }
        }

        IQueryable<IPermissionDataColumn> IPermissionDataContext.PermissionDataColumns
        {
            get { return PermissionDataColumns; }
        }

        IQueryable<IPermissionDataObject> IPermissionDataContext.PermissionDataObjects
        {
            get { return PermissionDataObjects; }
        }

        IQueryable<IPermissionProfileItem> IPermissionDataContext.PermissionProfileItems
        {
            get { return PermissionProfileItems; }
        }

        IQueryable<IPermissionProfileApplication> IPermissionDataContext.PermissionProfileApplications
        {
            get { return PermissionProfileApplications; }
        }

        IQueryable<IPermissionProfileHistory> IPermissionDataContext.PermissionProfileHistories
        {
            get { return PermissionProfileHistories; }
        }

        IQueryable<IProcessFlow> IProcessDataContext.ProcessFlows
        {
            get { return ProcessFlows; }
        }

        IQueryable<IProcessInProgress> IProcessDataContext.ProcessInProgresses
        {
            get { return ProcessInProgresses; }
        }

        IQueryable<IProcessStep> IProcessDataContext.ProcessSteps
        {
            get { return ProcessSteps; }
        }

        IQueryable<IProcessStepAction> IProcessDataContext.ProcessStepActions
        {
            get { return ProcessStepActions; }
        }

        IQueryable<IAddress> IAddressDataContext.Addresses
        {
            get { return Addresses; }
        }

        #endregion

        #region Interface Methods

        IEnumerable<DataObjectRelationship> IMetadataDataContext.GetRelationships()
        {
            return this.GetRelationships();
        }

        Task<List<IFilesystemNode>> IFilesystemRepositoryContext.GetFilesystemNodesFromRoot(int filesystemNodeRootId)
        {
            return this.GetFilesystemNodesFromRoot(filesystemNodeRootId);
        }

        Task<List<IFilesystemNode>> IFilesystemRepositoryContext.GetFilesystemNodesFromLeaf(int filesystemNodeId)
        {
            return this.GetFilesystemNodesFromLeaf(filesystemNodeId);
        }

        Task<List<IUserPermissionCache>> IFilesystemRepositoryContext.GetFilesystemPemrissionsFromRoot(
            int filesystemNodeId, int userId)
        {
            return this.GetFilesystemPemrissionsFromRoot(filesystemNodeId, userId);
        }

        Task<List<PermissionSetItem>> IPermissionDataContext.GetPermissionTreeUpAndDownForTargetAsync(int userId,
            IMetisDataObject target, int? minLoadDepth)
        {
            return this.GetPermissionTreeUpAndDownForTargetAsync(userId, target, minLoadDepth);
        }

        #endregion
    }
}