/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class PermissionGrant : DataObjectBaseWithHistory<PermissionGrant>, IPermissionGrant, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionGrantId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionGrantId; }
            set { PermissionGrantId = value; }
        }

        public int UserId { get; set; }

        [MaxLength(100)]
        public string Permission { get; set; }

        public int PermissionTargetId { get; set; }

        [MaxLength(1000)]
        public string Path { get; set; }
    }
}