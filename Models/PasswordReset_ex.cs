﻿namespace Valuation.Logic.Models
{
    public partial class PasswordReset
    {
        public const string RESET_SENT = "ResetSent";
        public const string RESET_USED = "ResetUsed";
    }
}