﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class WorkRoomFileActivity : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRoomFileActivityId { get; set; }

        public string FileActivityType { get; set; }

        //public string ActingTeam { get; set; }

        [NotMapped]
        public string RealActingTeam { get; set; }

        [NotMapped]
        public List<int?> PossibleTeamIds { get; set; }

        [NotMapped]
        public bool UserInVacinity { get; set; }

        public int ActingUser { get; set; }

        public int WorkRoomId { get; set; }

        public int? FilesystemNodeId { get; set; }

        public DateTimeOffset ActivityDate { get; set; }

        public int? OldData { get; set; }

        [NotMapped]
        public string UiOldData { get; set; }

        //For when we delete files, the FilesystemNodeId will be the folder, but the filename will be the deleted file.
        public string FileName { get; set; }

        public override int PrimaryKey
        {
            get { return WorkRoomFileActivityId; }
            set { WorkRoomFileActivityId = value; }
        }

        //Willing to take a bit of denormalization here to greatly reduce complexity later on...
        public int? TargetOrganizationId { get; set; }

        public string TargetMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> TargetMetisUserIdsList => this.TargetMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.TargetMetisUserIds);

        public string IPAddress { get; set; }

        [MaxLength(50), Index("IX_Concerning", 1, IsUnique = false, IsClustered = false)]
        public string ConcerningTargetType { get; set; }
        [Index("IX_Concerning", 2, IsUnique = false, IsClustered = false)]
        public int ConcerningPrimaryKeyId { get; set; }

        [Index("IX_OrganizationId", 1, IsUnique = false, IsClustered = false)]
        public int? OrganizationId { get; set; }
    }
}