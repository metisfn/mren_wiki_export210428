﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class MetisEmailLog : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MetisEmailLogId { get; set; }

        public string Subject { get; set; }

        public string Content { get; set; }

        //Probably doesn't matter
        public string FromAddress { get; set; }

        public string ToAddresses { get; set; }

        //Some emails are BCCed for privacy
        public string BccAddresses { get; set; }

        //Easier to search if it's all in one field
        public string AllRecipients { get; set; }

        public override int PrimaryKey
        {
            get { return MetisEmailLogId; }
            set { MetisEmailLogId = value; }
        }
    }
}
