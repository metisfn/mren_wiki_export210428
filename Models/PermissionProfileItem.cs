/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class PermissionProfileItem : DataObjectBase, IPermissionProfileItem, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionProfileItemId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionProfileItemId; }
            set { PermissionProfileItemId = value; }
        }

        [MaxLength(150)]
        public string ProfileName { get; set; }

        [MaxLength(150)]
        public string TargetType { get; set; }

        [MaxLength(150)]
        public string TargetPermissionGrouping { get; set; }

        [MaxLength(150)]
        public string ConcerningTargetType { get; set; }

        [MaxLength(1000)]
        public string Path { get; set; }

        [MaxLength(100)]
        public string Permission { get; set; }
    }
}