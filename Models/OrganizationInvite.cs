﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class OrganizationInvite : DataObjectBaseWithHistory<OrganizationInvite>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrganizationInviteId { get; set; }

        public int OrganizationId { get; set; }

        [ForeignKey("OrganizationId")]
        public virtual Organization Organization { get; set; }

        public int MetisUserId { get; set; }

        [ForeignKey("MetisUserId")]
        public virtual MetisUser MetisUser { get; set; }

        public string State { get; set; }
        public string BaseUrl { get; set; }
        public bool Owner { get; set; }
        public bool OrganizationAdministrator { get; set; }
        public bool CanInvite { get; set; }
        public bool PeerVisibility { get; set; }

        public override int PrimaryKey
        {
            get { return OrganizationInviteId; }
            set { OrganizationInviteId = value; }
        }

        // denormalized: capture email at the time of entry so we can display without over-exposing user's true/normalized contact info
        public string Email { get; set; }
        public DateTimeOffset? LastInviteSent { get; set; }
        public bool UseMetisUsersNameInEmail { get; set; }
        public string CustomMessage { get; set; }
        public int? FromMetisUserId { get; set; }
        public string SendAsEmail { get; set; }
        public string SendAsName { get; set; }
        public string CustomSubject { get; set; }
        public DateTimeOffset? LastTimeViewedFromEmail { get; set; }
    }
}