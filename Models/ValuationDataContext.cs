﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Dapper;
using Metis.Core.DataObjects;
using System.Configuration;
using Metis.Core.FileRepository.Interfaces;

namespace Valuation.Logic.Models
{
    public class WriteableDataContext: ValuationDataContext
    {
        public WriteableDataContext(IComponentContext afContext) : base()
        {
            SetAutofacContext(afContext);
        }

        public WriteableDataContext(string connectionName, IComponentContext afContext) : base(connectionName)
        {
            SetAutofacContext(afContext);
        }

        public static WriteableDataContext CreateWriteableContext(IComponentContext afContext = null)
        {
            // fallback to default connection string (null) if readonly is not available
            var ret = string.IsNullOrEmpty(MetisAppSettings.WriteableConnectionName) ?
                new WriteableDataContext(afContext) : new WriteableDataContext(MetisAppSettings.WriteableConnectionName, afContext);
            return ret;
        }
    }

    public class ValuationReadOnlyDataContext : ValuationDataContext
    {
        private const string ReadOnlyCtxExMsg = "This context is read-only!";
        
        public bool IsStandAloneROConnString { get; private set; }

        public static ValuationReadOnlyDataContext CreateReadOnlyContext(IComponentContext afContext = null)
        {
            // fallback to default connection string (null) if readonly is not available
            return string.IsNullOrEmpty(MetisAppSettings.ReadOnlyConnectionName) ?
                new ValuationReadOnlyDataContext(afContext) : new ValuationReadOnlyDataContext(MetisAppSettings.ReadOnlyConnectionName, afContext);
        }

        public static string ReadOnlyConnString
        {
            get
            {
                return string.IsNullOrEmpty(MetisAppSettings.ReadOnlyConnectionName) ?
                    null :
                    ConfigurationManager.ConnectionStrings[MetisAppSettings.ReadOnlyConnectionName].ConnectionString;
            }
        }

        public ValuationReadOnlyDataContext(IComponentContext afContext) : base()
        {
            IsStandAloneROConnString = false;
            Configuration.AutoDetectChangesEnabled = false;
            SetAutofacContext(afContext);
        }

        public ValuationReadOnlyDataContext(string connectionName, IComponentContext afContext) : base(connectionName)
        {
            IsStandAloneROConnString = true;
            Configuration.AutoDetectChangesEnabled = false;
            SetAutofacContext(afContext);
        }

        public override Task<int> SaveChangesAsync()
        {
            throw new InvalidOperationException(ReadOnlyCtxExMsg);
        }

        public override int SaveChanges()
        {
            throw new InvalidOperationException(ReadOnlyCtxExMsg);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            throw new InvalidOperationException(ReadOnlyCtxExMsg);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }

    public class ValuationDataContext : BaseDataContext, IValuationDataContext
    {
        public ValuationDataContext() : base(null, null, null)
        {
            Database.CommandTimeout = int.MaxValue;
        }

        public ValuationDataContext(string connectionName) : base(null, null, null, connectionName)
        {
            Database.CommandTimeout = int.MaxValue;
        }


        public ValuationDataContext(IEnumerable<IMetisDbContextPreSavePiplineAction> preSaveActions,
            IEnumerable<IMetisDbContextPostSavePiplineAction> postSaveActions, IComponentContext afContext)
            : base(preSaveActions, postSaveActions, afContext)
        {
            Database.CommandTimeout = int.MaxValue;
        }

        public ValuationDataContext(string connString, IEnumerable<IMetisDbContextPreSavePiplineAction> preSaveActions,
            IEnumerable<IMetisDbContextPostSavePiplineAction> postSaveActions, IComponentContext afContext)
            : base(connString, preSaveActions, postSaveActions, afContext)
        {
            Database.CommandTimeout = int.MaxValue;
        }

        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Deal> Deals { get; set; }
        public DbSet<OpportunityInvite> OpportunityInvites { get; set; }
        public DbSet<NdaSubmission> NdaSubmissions { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Nda> Ndas { get; set; }
        public DbSet<Relationship> Relationships { get; set; }
        public DbSet<RelationshipLink> RelationshipLinks { get; set; }
        public DbSet<UserInvite> UserInvites { get; set; }
        public DbSet<UserEmailConfirmation> UserEmailConfirmations { get; set; }
        public DbSet<OrganizationInvite> OrganizationInvites { get; set; }
        public DbSet<PasswordReset> PasswordResets { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }
        public DbSet<PropertyChangeNotificationEvent> PropertyChangeNotificationEvents { get; set; }
        public DbSet<UnderConsideration> UnderConsiderations { get; set; }
        public DbSet<ScheduledNotification> ScheduledNotifications { get; set; }
        public DbSet<UserProfilePublic> UserProfilePublics { get; set; }
        public DbSet<UserProfilePrivate> UserProfilePrivates { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagColor> TagColors { get; set; }
        public DbSet<OrganizationProfile> OrganizationProfiles { get; set; }
        public DbSet<OpportunityAnalytic> OpportunityAnalytics { get; set; }
        public DbSet<WorkRoom> WorkRooms { get; set; }
        public DbSet<WorkRoomInvite> WorkRoomInvites { get; set; }
        public DbSet<WorkRoomParticipantInvite> WorkRoomParticipantInvites { get; set; }
        public DbSet<OwnedWorkRoomFilesystemNodeGrant> OwnedWorkRoomFilesystemNodeGrants { get; set; }
        public DbSet<GrantedWorkRoomFilesystemNodeGrant> GrantedWorkRoomFilesystemNodeGrants { get; set; }
        public DbSet<NotificationJobLog> NotificationJobLogs { get; set; }
        public DbSet<NotificationsEmailLog> NotificationEmailLogs { get; set; }
        public DbSet<WorkRoomFileActivity> WorkRoomFileActivities { get; set; }
        public DbSet<MetisOneTimeDataMigration> MetisOneTimeDataMigrations { get; set; }
        public DbSet<MetisEmailLog> MetisEmailLogs { get; set; }
        public DbSet<ImageNode> ImageNodes { get; set; }
        public DbSet<GrayLabel> GrayLabels { get; set; }
        public DbSet<GrayLabelOrganization> GrayLabelOrganizations { get; set; }
        public DbSet<GrayLabelNdaAcceptance> GrayLabelNdaAcceptances { get; set; }
        public DbSet<TeaserInvite> TeaserInvites { get; set; }
        public DbSet<InviteJobsLog> InviteJobsLogs { get; set; }
        public DbSet<WorkRoomEdge> WorkRoomEdges { get; set; }
        public DbSet<ProjectGrant> ProjectGrants { get; set; }
        public DbSet<OrgSuggestion> OrgSuggestions { get; set; }
        public DbSet<MetisDevOpsNotification> MetisDevOpsNotifications { get; set; }
        public DbSet<UsernameConfirmation> UsernameConfirmations { get; set; }
        public DbSet<UserSubscription> UserSubscriptions { get; set; }
        public DbSet<MobileNotificationLog> MobileNotificationLogs { get; set; }
        public DbSet<DesktopSyncActivity> DesktopSyncActivities { get; set; }
        public DbSet<SyncClientConfig> SyncClientConfigs { get; set; }
        public DbSet<RelationshipLinkInvestment> RelationshipLinkInvestments { get; set; }
        public DbSet<OrgSubscription> OrgSubscriptions { get; set; }
        public DbSet<OrgTransaction> OrgTransactions { get; set; }
        public DbSet<OrgProfilePrivate> OrgProfilePrivates { get; set; }
        public DbSet<FilesystemNodeRead> FilesystemNodeReads { get; set; }
        public DbSet<UserFavorite> UserFavorites { get; set; }
        //public DbSet<ProjectActivationBypassToken> ProjectActivationBypassTokens { get; set; }
        public DbSet<PushNotificationRegistration> PushNotificationRegistrations { get; set; }
        public DbSet<FilesystemNodeUserActivity> FilesystemNodeUserActivities { get; set; }
        public DbSet<MetisIdentity> MetisIdentities { get; set; }
        public DbSet<UserNameRetrieval> UserNameRetrievals { get; set; }
        public DbSet<OrganizationPreference> OrganizationPreferences { get; set; }

        public DbSet<MetisKeyValuePair> MetisKeyValuePairs { get; set; }
        public DbSet<MetisUserSkipWorkRoom> MetisUserSkipWorkrooms { get; set; }
        public DbSet<WorkRoomFileActivityBatch> WorkRoomFileActivityBatches { get; set; }
        public DbSet<ConversationReadStatus> ConversationReadStatus { get; set; }

        public DbSet<RemoteHangfireQueueFallback> RemoteHangfireQueueFallbacks { get; set; }
        public DbSet<ActivityAlert> ActivityAlerts { get; set; }
        public DbSet<SendGridEvent> SendGridEvents { get; set; }
        public DbSet<DigestSubscription> DigestSubscriptions { get; set; }
        public DbSet<UserCredentialLog> UserCredentialLogs { get; set; }
        public DbSet<SmsDelivery> SmsDeliveries { get; set; }
        public DbSet<UserCachedChanges> UserCachedChanges { get; set; }
        public DbSet<Webhook> Webhooks { get; set; }
        public DbSet<Mention> Mentions { get; set; }

        public DbSet<PinnedItem> PinnedItems { get; set; }
        public DbSet<ProjectAssetPair> ProjectAssetPairs { get; set; }
        public DbSet<DistroMessage> DistroMessages { get; set; }
        public DbSet<FilesystemNodeGrant> FilesystemNodeGrants { get; set; }
        public DbSet<DistroMessageMetisUser> DistroMessageMetisUsers { get; set; }

        public DbSet<DistroMessageLog> DistroMessageLogs { get; set; }

        IQueryable<Gallery> IValuationDataContext.Galleries
        {
            get { return Galleries; }
        }

        IQueryable<Nda> IValuationDataContext.Ndas
        {
            get { return Ndas; }
        }

        IQueryable<Relationship> IValuationDataContext.Relationships
        {
            get { return Relationships; }
        }

        IQueryable<RelationshipLink> IValuationDataContext.RelationshipLinks
        {
            get { return RelationshipLinks; }
        }

        IQueryable<OpportunityInvite> IValuationDataContext.OpportunityInvites
        {
            get { return OpportunityInvites; }
        }

        IQueryable<UnderConsideration> IValuationDataContext.UnderConsiderations
        {
            get { return UnderConsiderations; }
        }

        IQueryable<NdaSubmission> IValuationDataContext.NdaSubmissions
        {
            get { return NdaSubmissions; }
        }

        public MetisUser AdminUser
        {
            get
            {
                return
                    MetisUsers.FirstOrDefault(
                        mu => mu.Email.Trim().Equals("admin@metisfn.com", StringComparison.OrdinalIgnoreCase));
            }
        }

        IQueryable<Asset> IValuationDataContext.Assets
        {
            get { return Assets; }
        }

        IQueryable<Deal> IValuationDataContext.Deals
        {
            get { return Deals; }
        }

        IQueryable<Project> IValuationDataContext.Projects
        {
            get { return Projects; }
        }

        IQueryable<Organization> IValuationDataContext.Organizations
        {
            get { return Organizations; }
        }

        IQueryable<ScheduledNotification> IValuationDataContext.ScheduledNotifications
        {
            get { return ScheduledNotifications; }
        }

        IQueryable<OrganizationInvite> IValuationDataContext.OrganizationInvites
        {
            get { return OrganizationInvites; }
        }

        IQueryable<UserInvite> IValuationDataContext.UserInvites
        {
            get { return UserInvites; }
        }

        IQueryable<UserEmailConfirmation> IValuationDataContext.UserEmailConfirmations
        {
            get { return UserEmailConfirmations; }
        }

        IQueryable<PasswordReset> IValuationDataContext.PasswordResets
        {
            get { return PasswordResets; }
        }

        IQueryable<UserActivity> IValuationDataContext.UserActivities
        {
            get { return UserActivities; }
        }

        IQueryable<UserProfilePublic> IValuationDataContext.UserProfilePublics
        {
            get { return UserProfilePublics; }
        }

        IQueryable<TeaserInvite> IValuationDataContext.TeaserInvites
        {
            get { return TeaserInvites; }
        }

        IQueryable<UserProfilePrivate> IValuationDataContext.UserProfilePrivates
        {
            get { return UserProfilePrivates; }
        }

        IQueryable<Tag> IValuationDataContext.Tags
        {
            get { return Tags; }
        }

        IQueryable<TagColor> IValuationDataContext.TagColors
        {
            get { return TagColors; }
        }

        IQueryable<OrganizationProfile> IValuationDataContext.OrganizationProfiles
        {
            get { return OrganizationProfiles; }
        }

        IQueryable<OpportunityAnalytic> IValuationDataContext.OpportunityAnalytics
        {
            get { return OpportunityAnalytics; }
        }

        IQueryable<PropertyChangeNotificationEvent> IValuationDataContext.PropertyChangeNotificationEvents
        {
            get { return PropertyChangeNotificationEvents; }
        }

        IQueryable<WorkRoom> IValuationDataContext.WorkRooms
        {
            get { return WorkRooms; }
        }

        IQueryable<WorkRoomInvite> IValuationDataContext.WorkRoomInvites
        {
            get { return WorkRoomInvites; }
        }

        IQueryable<WorkRoomParticipantInvite> IValuationDataContext.WorkRoomParticipantInvites
        {
            get { return WorkRoomParticipantInvites; }
        }

        IQueryable<OwnedWorkRoomFilesystemNodeGrant> IValuationDataContext.OwnedWorkRoomFilesystemNodeGrants
        {
            get { return OwnedWorkRoomFilesystemNodeGrants; }
        }

        IQueryable<GrantedWorkRoomFilesystemNodeGrant> IValuationDataContext.GrantedWorkRoomFilesystemNodeGrants
        {
            get { return GrantedWorkRoomFilesystemNodeGrants; }
        }

        IQueryable<NotificationJobLog> IValuationDataContext.NotificationJobLogs
        {
            get { return NotificationJobLogs; }
        }

        IQueryable<NotificationsEmailLog> IValuationDataContext.NotificationsEmailLogs
        {
            get { return NotificationEmailLogs; }
        }

        IQueryable<WorkRoomFileActivity> IValuationDataContext.WorkRoomFileActivities
        {
            get { return WorkRoomFileActivities; }
        }

        IQueryable<ImageNode> IValuationDataContext.ImageNodes
        {
            get { return ImageNodes; }
        }

        IQueryable<MetisOneTimeDataMigration> IValuationDataContext.MetisOneTimeDataMigrations
        {
            get { return MetisOneTimeDataMigrations; }
        }

        IQueryable<MetisEmailLog> IValuationDataContext.MetisEmailLogs
        {
            get { return MetisEmailLogs; }
        }

        IQueryable<GrayLabel> IValuationDataContext.GrayLabels
        {
            get { return GrayLabels; }
        }

        IQueryable<GrayLabelOrganization> IValuationDataContext.GrayLabelOrganizations
        {
            get { return GrayLabelOrganizations; }
        }

        IQueryable<GrayLabelNdaAcceptance> IValuationDataContext.GrayLabelNdaAcceptances
        {
            get { return GrayLabelNdaAcceptances; }
        }

        IQueryable<InviteJobsLog> IValuationDataContext.InviteJobsLogs
        {
            get { return InviteJobsLogs; }
        }

        IQueryable<WorkRoomEdge> IValuationDataContext.WorkRoomEdges
        {
            get { return WorkRoomEdges; }
        }

        IQueryable<ProjectGrant> IValuationDataContext.ProjectGrants
        {
            get { return ProjectGrants; }
        }

        IQueryable<OrgSuggestion> IValuationDataContext.OrgSuggestions
        {
            get { return OrgSuggestions; }
        }

        IQueryable<MetisDevOpsNotification> IValuationDataContext.MetisDevOpsNotifications
        {
            get { return MetisDevOpsNotifications; }
        }

        IQueryable<UsernameConfirmation> IValuationDataContext.UsernameConfirmations
        {
            get { return UsernameConfirmations; }
        }

        IQueryable<UserSubscription> IValuationDataContext.UserSubscriptions
        {
            get { return UserSubscriptions; }
        }

        IQueryable<MobileNotificationLog> IValuationDataContext.MobileNotificationLogs
        {
            get { return MobileNotificationLogs; }
        }

        IQueryable<OrgSubscription> IValuationDataContext.OrgSubscriptions
        {
            get { return OrgSubscriptions; }
        }

        IQueryable<OrgTransaction> IValuationDataContext.OrgTransactions
        {
            get { return OrgTransactions; }
        }

        IQueryable<OrgProfilePrivate> IValuationDataContext.OrgProfilePrivates
        {
            get { return OrgProfilePrivates; }
        }

        IQueryable<DesktopSyncActivity> IValuationDataContext.DesktopSyncActivities
        {
            get { return DesktopSyncActivities; }
        }

        IQueryable<SyncClientConfig> IValuationDataContext.SyncClientConfigs
        {
            get { return SyncClientConfigs; }
        }

        IQueryable<RelationshipLinkInvestment> IValuationDataContext.RelationshipLinkInvestments
        {
            get { return RelationshipLinkInvestments; }
        }

        IQueryable<FilesystemNodeRead> IValuationDataContext.FilesystemNodeReads
        {
            get { return FilesystemNodeReads; }
        }

        IQueryable<UserFavorite> IValuationDataContext.UserFavorites
        {
            get { return UserFavorites; }
        }

        //IQueryable<ProjectActivationBypassToken> IValuationDataContext.ProjectActivationBypassTokens
        //{
        //    get { return ProjectActivationBypassTokens; }
        //}

        IQueryable<PushNotificationRegistration> IValuationDataContext.PushNotificationRegistrations
        {
            get { return PushNotificationRegistrations; }
        }

        IQueryable<FilesystemNodeUserActivity> IValuationDataContext.FilesystemNodeUserActivities
        {
            get { return FilesystemNodeUserActivities; }
        }

        IQueryable<MetisIdentity> IValuationDataContext.MetisIdentities
        {
            get { return MetisIdentities; }
        }

        IQueryable<UserNameRetrieval> IValuationDataContext.UserNameRetrievals
        {
            get { return UserNameRetrievals; }
        }

        IQueryable<OrganizationPreference> IValuationDataContext.OrganizationPreferences
        {
            get { return OrganizationPreferences; }
        }

        IQueryable<MetisKeyValuePair> IValuationDataContext.MetisKeyValuePairs
        {
            get { return MetisKeyValuePairs; }
        }

        IQueryable<MetisUserSkipWorkRoom> IValuationDataContext.MetisUserSkipWorkrooms
        {
            get { return MetisUserSkipWorkrooms; }
        }

        IQueryable<WorkRoomFileActivityBatch> IValuationDataContext.WorkRoomFileActivityBatches
        {
            get { return WorkRoomFileActivityBatches; }
        }

        IQueryable<ConversationReadStatus> IValuationDataContext.ConversationReadStatus
        {
            get { return ConversationReadStatus; }
        }

        IQueryable<RemoteHangfireQueueFallback> IValuationDataContext.RemoteHangfireQueueFallbacks
        {
            get { return RemoteHangfireQueueFallbacks; }
        }

        IQueryable<ActivityAlert> IValuationDataContext.ActivityAlerts
        {
            get { return ActivityAlerts; }
        }

        IQueryable<SendGridEvent> IValuationDataContext.SendGridEvents
        {
            get { return SendGridEvents; }
        }

        IQueryable<DigestSubscription> IValuationDataContext.DigestSubscriptions
        {
            get { return DigestSubscriptions; }
        }

        IQueryable<UserCredentialLog> IValuationDataContext.UserCredentialLogs
        {
            get { return UserCredentialLogs; }
        }

        IQueryable<SmsDelivery> IValuationDataContext.SmsDeliveries
        {
            get { return SmsDeliveries; }
        }
        IQueryable<UserCachedChanges> IValuationDataContext.UserCachedChanges
        {
            get { return UserCachedChanges; }
        }

        IQueryable<Webhook> IValuationDataContext.Webhooks
        {
            get { return Webhooks; }
        }

        IQueryable<Mention> IValuationDataContext.Mentions
        {
            get { return Mentions; }
        }

        IQueryable<PinnedItem> IValuationDataContext.PinnedItems
        {
            get { return PinnedItems; }
        }

        IQueryable<ProjectAssetPair> IValuationDataContext.ProjectAssetPairs
        {
            get { return ProjectAssetPairs; }
        }

        IQueryable<DistroMessage> IValuationDataContext.DistroMessages
        {
            get { return DistroMessages; }
        }

        IQueryable<FilesystemNodeGrant> IValuationDataContext.FilesystemNodeGrants
        {
            get { return FilesystemNodeGrants; }
        }

        IQueryable<DistroMessageMetisUser> IValuationDataContext.DistroMessageMetisUsers
        {
            get { return DistroMessageMetisUsers; }
        }

        IQueryable<DistroMessageLog> IValuationDataContext.DistroMessageLogs
        {
            get { return DistroMessageLogs; }
        }

        public async Task<List<IFilesystemNode>> GetFilesystemNodesFromRootIncludingDeleted(
            int filesystemNodeRootId)
        {
            var connection = Database.Connection;
            List<IFilesystemNode> ret = null;

            try
            {
                var cmd = new CommandDefinition("spFilesystemNode_GetFilesystemNodesFromRootIncludingDeleted",
                    new {FilesystemNodeRootId = filesystemNodeRootId},
                    commandType: CommandType.StoredProcedure);

                var ifsnType = Create<IFilesystemNode>().GetType();

                if (connection.State != ConnectionState.Open) await connection.OpenAsync();
                using (var reader = await connection.QueryMultipleAsync(cmd))
                {
                    ret = (await reader.ReadAsync(ifsnType)).Cast<IFilesystemNode>().ToList();
                }
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
            }

            return ret;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Project>()
                .HasMany(s => s.Assets)
                .WithMany(c => c.Projects)
                .Map(cs =>
                {
                    cs.MapLeftKey("ProjectId");
                    cs.MapRightKey("AssetId");
                    cs.ToTable("ProjectAssets");
                });
        }

        public override int SaveChanges()
        {
            return (MetisAppSettings.AM_I_READ_ONLY_APP) ? 0 : base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync()
        {
            return (MetisAppSettings.AM_I_READ_ONLY_APP) ?
                Task<int>.Factory.StartNew(() => 0) :
                base.SaveChangesAsync();
        }

        /// <summary>
        /// Skips RO check before saving
        /// </summary>
        /// <returns></returns>
        public Task<int> ForceSaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }

    public static class MetisDataObjectExtensions
    {
        private static readonly Type[] typesToCopy = {typeof (DateTimeOffset), typeof (Guid), typeof (string)};

        public static TRet GetPOCOCopy<TRet>(this TRet toCopy, IMetisDataContext sourceForImplementations,
            bool topLevelOnly = true, Type typeHint = null) where TRet : IMetisDataObject
        {
            return
                (TRet)
                    GetPOCOCopy_Internal(typeHint ?? typeof (TRet), toCopy, new HashSet<object>(), sourceForImplementations,
                        topLevelOnly);
        }

        private static object GetPOCOCopy_Internal(Type toCopyType, object toCopy, HashSet<object> visited,
            IMetisDataContext sourceForImplementations, bool topLevelOnly)
        {
            Type targetType;
            if (toCopyType.IsInterface)
            {
                targetType = sourceForImplementations.GetImplementationForInterface(toCopyType);
            }
            else
            {
                targetType = toCopyType;
            }

            if (visited.Contains(toCopy))
            {
                return null;
            }

            var destination = Activator.CreateInstance(targetType);
            visited.Add(toCopy);

            var props = targetType.GetProperties(
                BindingFlags.Public |
                BindingFlags.SetProperty |
                BindingFlags.Instance)
                .ToArray();
            var srcType = toCopy.GetType();

            foreach (var prop in props)
            {
                if (prop.SetMethod == null) continue;

                var typeToCheck = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;

                if (topLevelOnly && !typeToCheck.IsPrimitive && !typesToCopy.Contains(typeToCheck) &&
                    !typeof (Enum).IsAssignableFrom(typeToCheck))
                    continue;

                var sourceProp = srcType.GetProperty(prop.Name);
                object value = null;
                if (sourceProp != null)
                {
                    value = sourceProp.GetValue(toCopy);

                    if (value != null && sourceProp.PropertyType.GetInterfaces().Contains(typeof(IMetisDataObject)))
                    {
                        if (topLevelOnly)
                        {
                            value = null;
                        }
                        else
                        {
                            var type = sourceProp.PropertyType;
                            if (type.IsInterface)
                            {
                                type = sourceForImplementations.GetImplementationForInterface(type);
                            }
                            var childObject = value as IMetisDataObject;
                            value = GetPOCOCopy_Internal(type, childObject, visited, sourceForImplementations, topLevelOnly);
                        }
                    }
                }
                prop.SetValue(destination, value);

            }
            return destination;
        }
    }
}