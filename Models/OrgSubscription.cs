﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("OrgSubscription")]
    public class OrgSubscription : DataObjectBaseWithHistory<OrgSubscription>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrgSubscriptionId { get; set; }

        public override int PrimaryKey
        {
            get { return OrgSubscriptionId; }
            set { OrgSubscriptionId = value; }
        }

        public int OrganizationId { get; set; }

        [ForeignKey("OrganizationId")]
        public virtual Organization Organization { get; set; }

        public string SubscriptionId { get; set; }
        //This should NEVER be set to false by us, only ever by a braintree webhook.
        //This prevents us from accidentally turning off features that someone paid for.
        public bool SubscriptionActive { get; set; }
        //Not the same thing as the team owner (necessessarily)
        public int ResponsiblePartyMuid { get; set; }
    }
}