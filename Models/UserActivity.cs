﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Valuation.Logic.Models
{
    public enum UserActivityType
    {
        ViewPage = 1,
        Update = 2,
        Create = 3,
        Delete = 4,
        ChangeDependencies = 5,
        InviteUser = 6,
        ResolveInviteUser = 7,
        DownloadFile = 8,
        AcceptDeal = 9,
        RejectDeal = 10,
        RequestAssetTransfer = 11,
        ModifyProject = 12,
        DealInviteRequest = 13,
        ChangeUserType = 14,
        AddFile = 15,
        DeleteFile = 16,
        BuyingPointPersonChange = 17,
        DeleteChild = 18,
        CreateChild = 19,
        SellingPointPersonChange = 20,
        SignedNda = 21,
        ImpendingDate = 22,
        SuggestOrganization = 23,
        FileVisibility = 24,
        Revoke = 25,
        CreateDealRoom = 26,
        BuyerExitDeal = 27,
        SellerExitDeal = 28,
        SelectTeam = 29,
        MoveFile = 30,
        RenameFile = 31,
        InitialLoad = 32,
        AddNdaFile = 33,
        AcceptOrgInvite = 34,
        RejectOrgInvite = 35,
        DocumentImagesCreated = 36,
        DownloadProtectedFilePage = 37,
        AddFileNodesBackgroundProc = 38,
        InviteTeam = 39,
        DownloadWatermarkedDocument = 40,
        OfferedRedirectToGrayLabelSites = 41,
        ReplaceFile = 42,
        CreatePost = 43,
        ViewDocument = 44,
        RestoreFile = 45,
        PaymentMethodUpdated = 46,
        MoneySpent = 47,
        InviteToWorkRoom = 48,
        EnterWorkRoom = 49,
        LeaveWorkRoom = 50,
        RemoveFromWorkRoom = 51,
        FeatureUsed = 52,
        Timeout = 53,
        CopyFile = 54,
        AddGrantedPolicy = 55,
        RemoveGrantedPolicy = 56,
        RemoveFromWpl = 57,
        ResetPassword = 58,
        DroppedEmail = 59,
        DashboardLoad = 60,
        NavError = 61,
        ViewModel = 62,
        ClickEvent = 63,
        Mention = 64,
        ChatMessageFromEmail = 65,
        ChatMessageFromUrl = 66,
        ChatMessageFromBitBucket = 67
    }

    [ODataPath("UserActivities")]
    public class UserActivity : DataObjectBase
    {
        public UserActivity()
        {
            SourceServer = MetisAppSettings.SiteBaseUrl;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserActivityId { get; set; }

        public override int PrimaryKey
        {
            get { return UserActivityId; }
            set { UserActivityId = value; }
        }

        public string BaseUrl { get; set; }

        public string Url { get; set; }

        public int ConcerningPrimaryKeyId { get; set; }
        public string ConcerningTargetType { get; set; }
        public Guid ConcerningObjectId { get; set; }

        [ForeignKey("UserId")]
        public virtual MetisUser User { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserContactId")]
        public virtual Contact UserContact { get; set; }

        public int UserContactId { get; set; }

        [JsonConverter(typeof (StringEnumConverter))]
        [MetisLookup("user_activity_type")]
        public UserActivityType UserActivityType { get; set; }

        [MetisMetadataDataType("datetime")]
        public DateTimeOffset ActivityDate { get; set; }

        public int? ParentPrimaryKeyId { get; set; }
        public string ParentTargetType { get; set; }
        public Guid? ParentObjectId { get; set; }

        [DefaultValue(false)]
        public bool IsActionable { get; set; }

        [DefaultValue(false)]
        public bool HasBeenActedOn { get; set; }

        public string CustomData { get; set; }

        [MaxLength(150)]
        public string ClientToken { get; set; }

        public string BrowserInfo { get; set; }

        public string ViewName { get; set; }

        public int? Seconds { get; set; }
        public string Feature { get; set; }

        public string LogLevel { get; set; }

        [JsonIgnore]
        public string SourceServer { get; set; }

        [JsonIgnore]
        public string IPAddress { get; set; }
    }
}