﻿using System;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Valuation.Logic.Models
{
    public class SmsDelivery : DataObjectBase
    {
        public const string SMS_STATUS_NOT_PROCESSED = "NotProcessed";
        public const string SMS_STATUS_FAILED = "Failed";
        public const string SMS_STATUS_ACCEPTED = "Accepted";
        public const string SMS_STATUS_DELIVERED = "Delivered";

        public int SmsDeliveryId { get; set; }

        public override int PrimaryKey
        {
            get { return SmsDeliveryId; }
            set { SmsDeliveryId = value; }
        }
        [Index("IX_MetisUserIdPhone", IsClustered = false, IsUnique = false, Order = 1)]
        public int? MetisUserId { get; set; }
        public string MessageId { get; set; }
        [MaxLength(30), Index("IX_MetisUserIdPhone", IsClustered = false, IsUnique = false, Order = 2)]
        public string DestinationNumber { get; set; }
        public string ProcessedByMessageServiceId { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string Content { get; set; }

        public string ConcerningPrimaryKeyIds { get; set; }
        public string ConcerningTargetType { get; set; }
        [MaxLength(3), Index("IX_MetisUserIdPhone", IsClustered = false, IsUnique = false, Order = 3)]
        public new string StatusCode { get; set; } 

        public SmsDelivery()
        {
            Status = SMS_STATUS_NOT_PROCESSED;
            StatusCode = DataObjectStatusCodes.ACTIVE;
        }
    }
}