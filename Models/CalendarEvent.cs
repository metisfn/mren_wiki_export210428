/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.Calendar;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class CalendarEvent : DataObjectBase, ICalendarEvent, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 CalendarEventId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return CalendarEventId; } set { CalendarEventId = value; } }
		public String Title { get; set; }
		public String Description { get; set; }
		public DateTimeOffset EventStartTime { get; set; }
		public DateTimeOffset EventEndTime { get; set; }
		public DateTimeOffset RecurrenceStartTime { get; set; }
		public DateTimeOffset RecurrenceEndTime { get; set; }
		public Recurrence Recurrence { get; set; }
		public Int32 ConcerningObjectPrimaryKeyId { get; set; }
		[MaxLength(150)]
		public String ConcerningObjectTargetType { get; set; }
		public virtual ICollection<CalendarEventOccurrence> CalendarEventOccurrences { get; set; }
		#region ICalendarEvent.CalendarEventOccurrences
		IEnumerable<ICalendarEventOccurrence> ICalendarEvent.CalendarEventOccurrences { get { return this.CalendarEventOccurrences; } set { this.CalendarEventOccurrences = new Collection<CalendarEventOccurrence>(value.Cast<CalendarEventOccurrence>().ToList()); } }
		void ICalendarEvent.AddCalendarEventOccurrence(ICalendarEventOccurrence toAdd) 
		{
			if(this.CalendarEventOccurrences == null)
			{
				this.CalendarEventOccurrences = new List<CalendarEventOccurrence>(); 
			}
			this.CalendarEventOccurrences.Add((CalendarEventOccurrence)toAdd); 
		}
		ICalendarEventOccurrence ICalendarEvent.CreateCalendarEventOccurrence() { return new CalendarEventOccurrence(); }
		#endregion
		public Int32? RootFilesystemNodeId { get; set; }
	}
}
		