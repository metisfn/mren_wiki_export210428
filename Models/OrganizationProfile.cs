﻿using System;
using System.Collections.Generic;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using Valuation.Logic.DTOs;

namespace Valuation.Logic.Models
{
    public class OrganizationProfileShell
    {
        public OrganizationProfileShell(OrganizationProfile op)
        {
            OrganizationProfileShellId = Guid.NewGuid();
            OrganizationProfileId = op.OrganizationProfileId;
            TeamName = op.TeamName;
            OrganizationId = op.OrganizationId;
            GalleryId = op.GalleryId;
            MetisUserId = op.MetisUserId;
            CustomMeta = op.CustomMeta;
            CustomLookups = op.CustomLookups;
            ProjectTableColumns = op.ProjectTableColumns;
            KanbanOpts = op.KanbanOpts;
            UITheme = op.UITheme;
            GrayLabelId = op.GrayLabelId;
    }

        [Key]
        public Guid OrganizationProfileShellId { get; set; }
        public int OrganizationProfileId { get; set; }
        public string TeamName { get; set; } 
        public int OrganizationId { get; set; }
        public int? GalleryId { get; set; }
        public int? MetisUserId { get; set; }  
        public string CustomMeta { get; set; }
        public string CustomLookups { get; set; }
        public string ProjectTableColumns { get; set; }
        public string KanbanOpts { get; set; }
        public string UITheme { get; set; }
        public bool IsOpen { get; set; }
        public int? GrayLabelId { get; set; }
    }

	[ODataPath("OrganizationProfiles")]
	public class OrganizationProfile : DataObjectBaseWithHistory<OrganizationProfile>, ICustomMetaOwner, IProfileImageOwner
	{

	    public const string PERMISSION_GROUPING_EXTERNAL_INQUIRIES = "ExternalInquiries";

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 OrganizationProfileId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return OrganizationProfileId; } set { OrganizationProfileId = value; } }


		//General Info
		public string CompanyPitch { get; set; }
		[MetisMetadataDataType("link")]
		public string CompanyUrl { get; set; }
		//public string CompanyType { get; set; }
		
		//This is the all purpose data field outlined in the doom II....outline.
		public string Info { get; set; }
		public string TeamName { get; set; }

		//Physical location info
		public string City { get; set; }
		public string State { get; set; }
		
        public string EmailDomain { get; set; }

		public int OrganizationId { get; set; }
		[ForeignKey("OrganizationId")]
		public virtual Organization Organization { get; set; }

		public int? GalleryId { get; set; }
		[ForeignKey("GalleryId")]
		public virtual Gallery Gallery { get; set; }

        // just hard link if it's personal org...
        public int? MetisUserId { get; set; }

        public string CustomMeta { get; set; }
        [NotMapped, JsonIgnore]
        public IList<CustomMeta> CustomMetaList { get; set; } 
        public string CustomLookups { get; set; }
        public string ProjectTableColumns { get; set; }
        public string KanbanOpts { get; set; }
        public bool HideAssets { get; set; }

        public string UITheme { get; set; }
        public bool IsOpen { get; set; }
        public string ProfileImageUrl { get; set; }
        public int? GrayLabelId { get; set; }
    }
}
