/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class PermissionDataColumn : DataObjectBase, IPermissionDataColumn, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionDataColumnId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionDataColumnId; }
            set { PermissionDataColumnId = value; }
        }

        public string ObjectName { get; set; }
        public string ColumnName { get; set; }
        public string Permission { get; set; }
        public bool CanRead { get; set; }
        public bool CanWrite { get; set; }
    }
}