﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire.Storage.Monitoring;

namespace Valuation.Logic.Models
{
    public partial class ActivityAlert
    {
        public const string ALERT_TYPE_BOUNCED_EMAIL = "BouncedEmail";
        public const string ALERT_TYPE_GLOBAL_FSN = "UsedGlobalFsnGrant";
        //public const string ALERT_TYPE_BASE_LEVEL_FSN = "UsedDealLevelFsnGrant";
        public const string ALERT_TYPE_ADD_EMAIL_RELA = "AddedEmailToRelationship";
        public const string ALERT_TYPE_RESEND_INVITE = "ResentEmail";
        public const string ALERT_TYPE_ARCHIVE_DEAL = "DealArchived";
        public const string ALERT_TYPE_TRIAL_STARTED = "TrialStarted";
        public const string ALERT_TYPE_2FA_CHANGED = "2FAChanged";
        public const string ALERT_TYPE_2FA_FAIL = "2FAFail";
        public const string ALERT_TYPE_SMS_FAIL = "SMSFail";
        public const string ALERT_TYPE_NAV_ERROR = "NavError";
        public const string ALERT_TYPE_STALE_LOOP = "StaleRefresherLoop";
        public const string ALERT_TYPE_STALE_CONVO_LOOP = "StaleConversationRefresherLoop";
        public const string ALERT_TYPE_STALE_CONVO_STOPPED_SYNC_LOADING = "StaleConversationRefresherStoppedSyncLoading";
        public const string ALERT_TYPE_WRONG_FRONTEND_VERSION = "WrongFrontendVersion";

        public static string ALERT_TYPE_USER_RETRY(string feature)
        {
            return "RetriedFeature::" + feature;
        }
    }
}
