﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("Galleries")]
    public class Gallery : DataObjectBase, IFilesystemOwner
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GalleryId { get; set; }

        public override int PrimaryKey
        {
            get { return GalleryId; }
            set { GalleryId = value; }
        }

        public int? RootFilesystemNodeId { get; set; }
    }
}