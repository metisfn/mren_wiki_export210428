﻿using Metis.Core.DataObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public interface IPerfTest
    {
        DateTimeOffset SessionTimestamp { get; set; }
        string TestSuiteName { get; set; }
        string RunName { get; set; }
        string Key { get; set; }
        string Col { get; set; }
        string Val { get; set; }
        string Hostname { get; set; }
        int? MetisUserId { get; set; }
    }

    public class PerfTest : DataObjectBase, IPerfTest
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PerfTestId { get; set; }

        [NotMapped, JsonIgnore]
        public override int PrimaryKey
        {
            get { return PerfTestId; }
            set { PerfTestId = value; }
        }

        public DateTimeOffset SessionTimestamp { get; set; }
        public string TestSuiteName { get; set; }
        public string Key { get; set; }
        public string Col { get; set; }
        public string Val { get; set; }
        public string RunName { get; set; }
        public string Hostname { get; set; }
        public int? MetisUserId { get; set; }

        public void CopyFrom(IPerfTest source)
        {
            if (source == null) return;

            SessionTimestamp = source.SessionTimestamp;
            TestSuiteName = source.TestSuiteName;
            Key = source.Key;
            Col = source.Col;
            Val = source.Val;
            RunName = source.RunName;
            Hostname = source.Hostname;
            MetisUserId = source.MetisUserId;
        }
    }
    
    public class PerfTestDTO : IPerfTest
    {
        public DateTimeOffset SessionTimestamp { get; set; }
        public string TestSuiteName { get; set; }
        public string Key { get; set; }
        public string Col { get; set; }
        public string Val { get; set; }
        public string RunName { get; set; }
        public string Hostname { get; set; }
        public int? MetisUserId { get; set; }

    }
}
