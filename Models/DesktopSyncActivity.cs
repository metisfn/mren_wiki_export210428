﻿using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public enum DesktopActivityType
    {
        Info = 0,
        Error = 1,
        Update = 2,
    }

    public interface IDesktopSyncActivityDto
    {
        string DeviceId { get; set; }
        string AppVersion { get; set; }
        DesktopActivityType ActivityType { get; set; }
        int? ResolutionWidth { get; set; }
        int? ResolutionHeight { get; set; }
        string OS { get; set; }
        string RAM { get; set; }
        string Message { get; set; }
        string CPU { get; set; }
        string Exception { get; set; }
        string[] RunningProcs { get; set; }
        int? AppCPUUsage { get; set; }
        int? TotalCPUUsage { get; set; }
    }

    public class DesktopSyncActivityDto : IDesktopSyncActivityDto
    {
        public string DeviceId { get; set; }
        public string AppVersion { get; set; }
        public DesktopActivityType ActivityType { get; set; }
        public int? ResolutionWidth { get; set; }
        public int? ResolutionHeight { get; set; }
        public string OS { get; set; }
        public string RAM { get; set; }
        public string Message { get; set; }
        public string CPU { get; set; }
        public string Exception { get; set; }
        public string[] RunningProcs { get; set; }
        public int? AppCPUUsage { get; set; }
        public int? TotalCPUUsage { get; set; }
    }

    public class DesktopSyncActivity : DataObjectBase, IDesktopSyncActivityDto
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DesktopSyncActivityId { get; set; }
        public override int PrimaryKey
        {
            get { return DesktopSyncActivityId; }
            set { DesktopSyncActivityId = value; }
        }
        public int? MetisUserId { get; set; }
        [ForeignKey("MetisUserId")]
        public virtual MetisUser MetisUser { get; set; }
        public string IP { get; set; }

        [MaxLength(450), Index("IX_DeviceId", IsUnique = false)]
        public string DeviceId { get; set; }
        public string AppVersion { get; set; }
        public DesktopActivityType ActivityType { get; set; }
        public int? ResolutionWidth { get; set; }
        public int? ResolutionHeight { get; set; }
        public string OS { get; set; }
        public string RAM { get; set; }
        public string Message { get; set; }
        public string CPU { get; set; }
        public string Exception { get; set; }
        [NotMapped]
        public string[] RunningProcs { get; set; }
        public int? AppCPUUsage { get; set; }
        public int? TotalCPUUsage { get; set; }

        private string _userProcs;
        public string UserProcesses {
            get
            {
                if (_userProcs == null)
                {
                    _userProcs = string.Join(", ", RunningProcs ?? new string[0]);
                }
                return _userProcs;
            }
            set
            {
                _userProcs = value;
            }
        }


        public DesktopSyncActivity() { }
        public DesktopSyncActivity(IDesktopSyncActivityDto dto) : this()
        {
            DeviceId         = dto.DeviceId;
            AppVersion       = dto.AppVersion;
            ActivityType     = dto.ActivityType;
            ResolutionWidth  = dto.ResolutionWidth;
            ResolutionHeight = dto.ResolutionHeight;
            OS               = dto.OS;
            RAM              = dto.RAM;
            Message          = dto.Message;
            CPU              = dto.CPU;
            Exception        = dto.Exception;
            RunningProcs     = dto.RunningProcs;
            AppCPUUsage      = dto.AppCPUUsage;
            TotalCPUUsage    = dto.TotalCPUUsage;
        }
    }
}
