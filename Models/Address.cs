/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.Address;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class Address : DataObjectBase, IAddress, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 AddressId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return AddressId; } set { AddressId = value; } }
		public String StreetAddress { get; set; }
		public String City { get; set; }
		public String Region { get; set; }
		public String Subregion { get; set; }
		public String Postal { get; set; }
		public String Country { get; set; }
		public Decimal Longitude { get; set; }
		public Decimal Latitude { get; set; }
		public Int32? RootFilesystemNodeId { get; set; }
	}
}
		