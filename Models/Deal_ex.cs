﻿using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("Deals")]
    public partial class Deal
    {
        public const string PERMISSION_ACCEPT = "Accept";
        public const string PERMISSION_INVITED_USER = "InvitedUser";
        public const string PERMISSION_BUYING_POINT_PERSON = "BuyingPointPerson";
        public const string PERMISSION_SELLING_POINT_PERSON = "SellingPointPerson";
        public const string PERMISSION_POINT_PERSON = "PointPerson";
        public const string PERMISSION_INVITE_USERS = "InviteUsers";
        public const string PERMISSION_GROUPING_INVITED_USERS = "InvitedUsers";
        public const string PERMISSION_GROUPING_MAIN_DEAL_CONVERSATION = "MainDealConversation";

        public const string TYPE_SALE = "Sale";
        public const string TYPE_CAP_RAISE = "CapRaise";

        public const string STATUS_ROOM_REQUESTED = "RoomRequested";
        public const string STATUS_HOST_REQUESTED = "HostRequested";
        public const string STATUS_NEGOTIATING = "Negotiating";
        public const string STATUS_DEAD = "Dead";

        //Sale specific deals
        public const string STATUS_MADE_OFFER = "MadeOffer";
        public const string STATUS_ACCEPTED = "Accepted";

        //Cap Raise specific deals
        public const string STATUS_COMMITTED = "Committed";
        public const string STATUS_COMMITMENT_CONFIRMED = "CommitmentConfirmed";
        public const string STATUS_FUNDED = "Funded";

        public int BuyingOrgId { get; set; }
    }
}