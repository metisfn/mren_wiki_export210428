﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("TeaserInvites")]
    public class TeaserInvite : DataObjectBaseWithHistory<TeaserInvite>
    {
        public const string STATUS_ACTIVE = "Active";
        public const string STATUS_EXPIRED = "Expired";

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TeaserInviteId { get; set; }

        public override int PrimaryKey
        {
            get { return TeaserInviteId; }
            set { TeaserInviteId = value; }
        }

        public string Token { get; set; }
        public int InvitedToId { get; set; }
        public int InvitingUserId { get; set; }
        public string BaseUrl { get; set; }

        //Not strictly necessary but makes the table easier to read later
        public int InvitedOrgId { get; set; }

        public bool IsNoAuth { get; set; }

        //Right now only using this to say "hey this is really about files and not projects", but probably should've designed it this way originally
        public string ConcerningTargetType { get; set; }


        public string Status { get; set; }


        public int ViewCount { get; set; }
    }
}