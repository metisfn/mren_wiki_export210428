﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;
using Valuation.Logic.Handlers;

namespace Valuation.Logic.Models
{
    public partial class ActivityAlert : DataObjectBase, IConcerning
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ActivityAlertId { get; set; }

        public string AlertType { get; set; }

        public override int PrimaryKey
        {
            get { return ActivityAlertId; }
            set { ActivityAlertId = value; }
        }

        public int ConcerningPrimaryKeyId { get; set; }
        public string ConcerningTargetType { get; set; }
        public Guid ConcerningObjectId { get; set; }
        public Guid? ConcerningRevisionId { get; set; }
        public int? MetisUserId { get; set; }
    }
}
