﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using Valuation.Logic.Attributes;
using Valuation.Logic.DTOs;

namespace Valuation.Logic.Models
{
    [ODataPath("Relationships")]
    public class Relationship : DataObjectBaseWithHistory<Relationship>, IFilesystemOwner, ICustomFieldsOwner, ITagListOwner
    {
        /// <summary>
        /// ERL 2016.05.11 for copying org relas to personal contacts
        /// </summary>
        /// <returns></returns>
        public Relationship ShallowCopy()
        {
            var toAdd = new Relationship
            {
                TargetPrimaryKeyId = this.TargetPrimaryKeyId,
                ConcerningTargetType = this.ConcerningTargetType,

                Notes = this.Notes,
                ApartmentOrSuite = this.ApartmentOrSuite,
                City = this.City,
                Country = this.Country,
                County = this.County,
                Email = this.Email,
                FirstName = this.FirstName,
                LastName = this.LastName,
                MiddleName = this.MiddleName,
                Name = this.Name,
                PhoneExt = this.PhoneExt,
                PhoneNumber = this.PhoneNumber,
                PostalCode = this.PostalCode,
                State = this.State,
                Suffix = this.Suffix,
                Title = this.Title,
                Company = this.Company,
                Role = this.Role,
                InvitedUser = this.InvitedUser,
                CustomFields = this.CustomFields,
                CustomFieldsFromImport = this.CustomFieldsFromImport,
                TargetPrimaryKeyIds = this.TargetPrimaryKeyIds
            };


            return toAdd;
        }

        public RelationshipLite GetRelationshipLite()
        {
            return new RelationshipLite
            {
                RelationshipId = this.RelationshipId,
                ConcerningTargetType = this.ConcerningTargetType,
                TargetPrimaryKeyId = this.TargetPrimaryKeyId,
                OrganizationId = this.OrganizationId,
                MetisIdentityId = this.MetisIdentityId,
                TargetPrimaryKeyIds = this.TargetPrimaryKeyIds,
                MetisUserId = this.MetisUserId,
                FirstName = this.FirstName,
                LastName = this.LastName,
                Name = this.Name
            };
        }

        public Relationship() { }

        public Relationship(RelationshipLite relaLite)
        {
            if (relaLite == null) return;

            RelationshipId = relaLite.RelationshipId;
            ConcerningTargetType = relaLite.ConcerningTargetType;
            TargetPrimaryKeyId = relaLite.TargetPrimaryKeyId;
            OrganizationId = relaLite.OrganizationId;
            MetisIdentityId = relaLite.MetisIdentityId;
            TargetPrimaryKeyIds = relaLite.TargetPrimaryKeyIds;
            MetisUserId = relaLite.MetisUserId;
            FirstName = relaLite.FirstName;
            LastName = relaLite.LastName;
            Name = relaLite.Name;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RelationshipId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return RelationshipId; }
            set { RelationshipId = value; }
        }

        [Column(TypeName = "varchar(MAX)")]
        public string Notes { get; set; }

        public bool? InMyNetwork { get; set; }
        // copied from contact since perms are stupid
        public string ApartmentOrSuite { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string County { get; set; }

        [MetisRequired]
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Name { get; set; }
        public string PhoneExt { get; set; }
        public string PhoneNumber { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string Suffix { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Role { get; set; }
        public bool InvitedUser { get; set; }
        //This means the user owns the relationship
        public int? MetisUserId { get; set; }
        //This means the Organization owns the relationship
        public int? OrganizationId { get; set; }
        public int TargetPrimaryKeyId { get; set; }
        public string ConcerningTargetType { get; set; }
        public string CustomMeta { get; set; }
        [NotMapped, JsonIgnore]
        public IList<CustomMeta> CustomMetaList { get; set; }
        public string CustomFields { get; set; }
        public string CustomFieldsFromImport { get; set; }
        public int? RootFilesystemNodeId { get; set; }
        public int? MetisIdentityId { get; set; }


        //Archived is better than 'DEL' in this case because it means they don't want
        //it to be regened.
        [DefaultValue(false)]
        public bool IsArchived { get; set; }

        public string TargetPrimaryKeyIds { get; set; }

        [NotMapped, JsonIgnore]
        public IList<int> TargetPrimaryKeyIdsList => this.TargetPrimaryKeyIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.TargetPrimaryKeyIds);

        public string Website { get; set; }

        public string TagList { get; set; }
    }

    public class RelationshipLite
    {
        public int RelationshipId { get; set; }
        public string ConcerningTargetType { get; set; }
        public int TargetPrimaryKeyId { get; set; }
        public int? OrganizationId { get; set; }
        public int? MetisIdentityId { get; set; }
        public string TargetPrimaryKeyIds { get; set; }
        public int? MetisUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
    }

    public static class RelationshipLiteExtensions
    {
        public static IList<int> SplitStringIntoIds(string idString)
        {
            return string.IsNullOrEmpty(idString) ? new List<int>() : idString.Replace("*", "0").Split(',').Select(int.Parse).ToList();
        }

        public static IList<int> TargetPrimaryKeyIdsList(this RelationshipLite relationshipLite)
        {
            return relationshipLite == null ? new List<int>() : SplitStringIntoIds(relationshipLite.TargetPrimaryKeyIds);
        }
    }
}