﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
	[ODataPath("UnderConsiderations")]
    public partial class UnderConsideration : DataObjectBase
    {
        public UnderConsideration()
        {
        }

		public UnderConsideration(int projectId, int metisUserId)
        {
            ProjectId = projectId;
            MetisUserId = metisUserId;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int UnderConsiderationId { get; set; }

        public override int PrimaryKey
        {
			get { return UnderConsiderationId; }
			set { UnderConsiderationId = value; }
        }



        public int ProjectId { get; set; }
		[ForeignKey("ProjectId")]
		public virtual Project Project { get; set; }
        public int MetisUserId { get; set; }
		[ForeignKey("MetisUserId")]
        public virtual MetisUser MetisUser { get; set; }

    }
}