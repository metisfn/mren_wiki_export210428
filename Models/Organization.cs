﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;

namespace Valuation.Logic.Models
{
    public partial class Organization : DataObjectBaseWithHistory<Organization>, IFilesystemOwner, ISubscribable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrganizationId { get; set; }

        [MetisMetadataDataType("bladeUrlPart")]
        public string Name { get; set; }
        public bool IsPersonal { get; set; }
        public int? WorkRoomId { get; set; }

        public override int PrimaryKey
        {
            get { return OrganizationId; }
            set { OrganizationId = value; }
        }

        [ForeignKey("RootFilesystemNodeId")]
        public virtual FilesystemNode RootFilesystemNode { get; set; }
        public int? RootFilesystemNodeId { get; set; }
        public bool IsUnlimited { get; set; }

        [NotMapped]
        public int? PreferredMetisUserId { get; set; }


        public string Description { get; set; }

        public int? ExtCorId { get; set; }
        public bool IsOpen { get; set; }
        public int GrayLabelId { get; set; }

        [MetisMetadataDataType("MetisUserId")]
        public string MemberMetisUserIds { get; set; }

        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> MemberMetisUserIdList => MemberMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(MemberMetisUserIds);


        [MetisMetadataDataType("MetisUserId")]
        public string SubscribedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> SubscribedMetisUserIdsList => this.SubscribedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.SubscribedMetisUserIds);

        

        public string UrlId { get; set; } // short uuid for sharing
        // URL_PRIVACY_PUBLIC = "Public"; // anyone with link
        // URL_PRIVACY_GL = "GrayLabel";  // GL members... will still depend on open/private?
        // URL_PRIVACY_DISABLED = "Disabled"; // no link at all; explicit invites only
        public string UrlPrivacy { get; set; } 

    }
}