﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    public interface IUserEmailConfirmation : IMetisDataObjectWithHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int UserEmailConfirmationId { get; set; }

        string Email { get; set; }
        int UserId { get; set; }

        [ForeignKey("UserId")]
        MetisUser User { get; set; }

        string Token { get; set; }
        bool HasConfirmedEmail { get; set; }
    }

    [ODataPath("UserEmailConfirmations")]
    public class UserEmailConfirmation : DataObjectBaseWithHistory<UserEmailConfirmation>, IUserEmailConfirmation
    {
        public string BaseUrl { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserEmailConfirmationId { get; set; }

        public string Email { get; set; }

        public override int PrimaryKey
        {
            get { return UserEmailConfirmationId; }
            set { UserEmailConfirmationId = value; }
        }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual MetisUser User { get; set; }

        public string Token { get; set; }
        public bool HasConfirmedEmail { get; set; }
        [NotMapped]
        public bool NeedsName { get; set; }
    }
}