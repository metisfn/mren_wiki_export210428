﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using System;

namespace Valuation.Logic.Models
{
    [ODataPath("OrgProfilePrivate")]
    public class OrgProfilePrivate : DataObjectBaseWithHistory<OrgProfilePrivate>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrgProfilePrivateId { get; set; }

        public override int PrimaryKey
        {
            get { return OrgProfilePrivateId; }
            set { OrgProfilePrivateId = value; }
        }
        public int OrganizationId { get; set; }
        [ForeignKey("OrganizationId")]
        public virtual Organization Organization { get; set; }

        public string CustomerId { get; set; }

        public string SubscriptionId { get; set; }

        public string PaymentMethods { get; set; }
        public bool DismissedTeamCollabPaywall { get; set; }
        public bool TrialStarted { get; set; }
        public DateTimeOffset? TrialEnds { get; set; }

        public int? ProjectDefaultFoldersRootFsnId { get; set; }
        public int? AssetDefaultFoldersRootFsnId { get; set; }
    }
}