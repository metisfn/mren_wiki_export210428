/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Process.Interfaces;

namespace Valuation.Logic.Models
{
    public class ProcessFlow : DataObjectBase, IProcessFlow, IMetisScaffold
    {
        public virtual ICollection<ProcessStep> Steps { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProcessFlowId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ProcessFlowId; }
            set { ProcessFlowId = value; }
        }

        public string Name { get; set; }

        #region IProcessFlow.Steps

        IEnumerable<IProcessStep> IProcessFlow.Steps
        {
            get { return Steps; }
            set { Steps = new Collection<ProcessStep>(value.Cast<ProcessStep>().ToList()); }
        }

        void IProcessFlow.AddStep(IProcessStep toAdd)
        {
            if (Steps == null)
            {
                Steps = new List<ProcessStep>();
            }
            Steps.Add((ProcessStep) toAdd);
        }

        IProcessStep IProcessFlow.CreateStep()
        {
            return new ProcessStep();
        }

        #endregion
    }
}