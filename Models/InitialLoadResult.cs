using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Valuation.Logic.DTOs;
using Valuation.Logic.Providers;

namespace Valuation.Logic.Models
{
    public class InitialLoadResult
    {
        private static string[] _skipProps = new[] { nameof(UserId) };

        public InitialLoadResult()
        {
            var thisType = GetType();
            var properties = thisType.GetProperties();
            foreach (var p in properties)
            {
                if (_skipProps.Contains(p.Name)) continue;
                var typeOfCollection = thisType.GetProperty(p.Name).PropertyType.GetGenericArguments();
                var listType = typeof (List<>);
                var typedList = listType.MakeGenericType(typeOfCollection);
                var generated = Activator.CreateInstance(typedList);
                thisType.GetProperty(p.Name).SetValue(this, generated);
            }
        }

        [Key]
        public int UserId { get; set; }

        public ICollection<ProjectOrganizationMarketVisibility> ProjectShell { get; set; }
        public ICollection<UnderConsideration> UnderConsiderations { get; set; }
        public ICollection<OpportunityInvite> OpportunityInvites { get; set; }
        public ICollection<OrganizationInvite> OrganizationInvites { get; set; }
        public ICollection<Organization> Organizations { get; set; }
        public ICollection<WorkRoom> WorkRooms { get; set; }
        public ICollection<int> WorkRoomIds { get; set; }
        public ICollection<WorkRoomEdge> WorkRoomEdges { get; set; }
        public ICollection<WorkRoomInvite> WorkRoomInvites { get; set; }
        public ICollection<Gallery> Galleries { get; set; }
        public ICollection<UserProfilePublicShell> UserProfilePublics { get; set; }
        public ICollection<UserProfilePrivate> UserProfilePrivates { get; set; }
        public ICollection<MetisUser> MetisUsers { get; set; }
        public ICollection<OrganizationProfileShell> OrganizationProfiles { get; set; }
        public ICollection<AssetShell> Assets { get; set; }
        public ICollection<ProjectGrant> ProjectGrants { get; set; }
        public ICollection<UserPermissionCache> Permissions { get; set; }
        public ICollection<ConversationWithReads> Conversations { get; set; }
        public ICollection<ConversationItem> ConversationItems { get; set; }
        public ICollection<ConversationItemReadStatus> ConversationItemReadStatuses { get; set; }
        public ICollection<Tag> Tags { get; set; }
        public ICollection<int> ProjectIdsWithActiveSubs { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<DigestSubscription> DigestSubscriptions { get; set; }
        public IEnumerable<ExternalLoginConfig> ExternalLogins { get; set; }
    }

    public class InitialLoad2
    {
        private static string[] _skipProps = new[] { nameof(UserId) };

        public InitialLoad2()
        {
            var thisType = GetType();
            var properties = thisType.GetProperties();
            foreach (var p in properties)
            {
                if (_skipProps.Contains(p.Name)) continue;
                var typeOfCollection = thisType.GetProperty(p.Name).PropertyType.GetGenericArguments();
                var listType = typeof(List<>);
                var typedList = listType.MakeGenericType(typeOfCollection);
                var generated = Activator.CreateInstance(typedList);
                thisType.GetProperty(p.Name).SetValue(this, generated);
            }
        }

        [Key]
        public int UserId { get; set; }

        //public ICollection<ProjectOrganizationMarketVisibility> ProjectShell { get; set; }
        //public ICollection<UnderConsideration> UnderConsiderations { get; set; }
        //public ICollection<OpportunityInvite> OpportunityInvites { get; set; }
        //public ICollection<OrganizationInvite> OrganizationInvites { get; set; }
        //public ICollection<Organization> Organizations { get; set; }
        //public ICollection<WorkRoom> WorkRooms { get; set; }
        //public ICollection<int> WorkRoomIds { get; set; }
        //public ICollection<WorkRoomEdge> WorkRoomEdges { get; set; }
        //public ICollection<WorkRoomInvite> WorkRoomInvites { get; set; }
        //public ICollection<UserProfilePublicShell> UserProfilePublics { get; set; }
        public ICollection<UserProfilePrivate> UserProfilePrivates { get; set; }
        public ICollection<MetisUser> MetisUsers { get; set; }
        //public ICollection<OrganizationProfileShell> OrganizationProfiles { get; set; }
        //public ICollection<AssetShell> Assets { get; set; }
        //public ICollection<ProjectGrant> ProjectGrants { get; set; }
        //public ICollection<UserPermissionCache> Permissions { get; set; }
        //public ICollection<ConversationWithReads> Conversations { get; set; }
        //public ICollection<ConversationItem> ConversationItems { get; set; }
        //public ICollection<Tag> Tags { get; set; }
        //public ICollection<int> ProjectIdsWithActiveSubs { get; set; }
        //public ICollection<Project> Projects { get; set; }
        public ICollection<GrayLabel> GrayLabels { get; set; }
        public ICollection<UserFavorite> UserFavorites { get; set; }
        public ICollection<UserActivity> LastViewed { get; set; }
        public ICollection<DigestSubscription> DigestSubscriptions { get; set; }
        public ICollection<MetisUserSkipWorkRoom> MetisUserSkipWorkRooms { get; set; } 
        public ICollection<ExternalLoginConfig> ExternalLogins { get; set; }
    }
}