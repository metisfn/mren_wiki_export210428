/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.Conversations.Interfaces;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
    public partial class ConversationItemDeliveryStatus : DataObjectBaseWithHistory<ConversationItemDeliveryStatus>,
        IConversationItemDeliveryStatus, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConversationItemDeliveryStatusId { get; set; }

        public virtual Contact ToContact { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ConversationItemDeliveryStatusId; }
            set { ConversationItemDeliveryStatusId = value; }
        }

        public int ConversationSubscriptionId { get; set; }
        public int ConversationItemId { get; set; }
        public string DeliveryStatus { get; set; }
        public int ToContactId { get; set; }

        IContact IConversationItemDeliveryStatus.ToContact
        {
            get { return ToContact; }
            set { ToContact = (Contact) value; }
        }

        public string DeliveryMethod { get; set; }
    }
}