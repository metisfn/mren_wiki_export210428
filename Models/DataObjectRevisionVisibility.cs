/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class DataObjectRevisionVisibility : DataObjectBase, IDataObjectRevisionVisibility, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DataObjectRevisionVisibilityId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return DataObjectRevisionVisibilityId; }
            set { DataObjectRevisionVisibilityId = value; }
        }

        public DateTimeOffset GrantedDate { get; set; }
        public int GrantingUserId { get; set; }
        public string Permission { get; set; }
        public string Reason { get; set; }
        public Guid TargetObjectId { get; set; }
        public int TargetPrimaryKeyId { get; set; }
        public DateTimeOffset? TargetRevisionDate { get; set; }
        public Guid? TargetRevisionId { get; set; }
        public string TargetType { get; set; }
        public int PermissionTargetId { get; set; }
    }
}