using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class OwnedWorkRoomFilesystemNodeGrant : DataObjectBase, IWorkRoomFilesystemNodeGrant
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OwnedWorkRoomFilesystemNodeGrantId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return OwnedWorkRoomFilesystemNodeGrantId; }
            set { OwnedWorkRoomFilesystemNodeGrantId = value; }
        }

        public int? FilesystemNodeId { get; set; }
        public string TagText { get; set; }
        public int OwnedByWorkRoomId { get; set; }
        public bool Readable { get; set; }
        public bool Shareable { get; set; }
        public bool Contributable { get; set; }
        //public bool RestrictDownloadable { get; set; }
    }
}