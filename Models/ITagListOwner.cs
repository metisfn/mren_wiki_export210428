﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public interface ITagListOwner
    {
        string TagList { get; set; }
    }
}
