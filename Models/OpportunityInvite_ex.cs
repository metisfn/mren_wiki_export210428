﻿using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("OpportunityInvites")]
    public partial class OpportunityInvite
    {
        public const string STATUS_REVOKED = "Revoked";
        public const string STATUS_CLOSED = "Deal Closed";
        public const string STATUS_REJECTED = "Rejected";
        public const string STATUS_REQUESTED = "Request";
        public const string STATUS_SENT = "Sent";
        public const string STATUS_SENT_EMAIL = "SentEmail";
        public const string STATUS_INVITE_READ = "Read";
        public const string STATUS_VIEWED = "Deal Viewed";
        public const string STATUS_NDA_SIGNED = "NDA Accepted";
        public const string STATUS_NDA_BYPASSED = "NDA Bypassed";
        public const string STATUS_NDA_DOWNLOADED = "NDA Downloaded";
        public const string STATUS_MADE_OFFER = "Offer Made";
        public const string STATUS_DEFERRED = "Deferred";
        public const string STATUS_RESENT = "Resent";

        public const string INVITED_TYPE_USER = "User";
        public const string INVITED_TYPE_TEAM = "Team";
        public const string INVITED_TYPE_CONF_MROOM = "WorkRoom";
    }
}