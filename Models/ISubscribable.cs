﻿using Metis.Core.DataObjects;
using System.Collections.Generic;

namespace Valuation.Logic.Models
{
    public interface ISubscribable : IMetisDataObject
    {
        string SubscribedMetisUserIds { get; set; }
        ICollection<int> SubscribedMetisUserIdsList { get; }
    }
}
