/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Process.Interfaces;
using Newtonsoft.Json;

namespace Valuation.Logic.Models
{
    public class ProcessStep : DataObjectBase, IProcessStep, IMetisScaffold
    {
        private Dictionary<string, string> _DecisionDictionary = new Dictionary<string, string>();
        public virtual ProcessFlow ProcessFlow { get; set; }
        public virtual ICollection<ProcessStepAction> Actions { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProcessStepId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ProcessStepId; }
            set { ProcessStepId = value; }
        }

        public int ProcessFlowId { get; set; }
        public int Order { get; set; }

        IProcessFlow IProcessStep.ProcessFlow
        {
            get { return ProcessFlow; }
            set { ProcessFlow = (ProcessFlow) value; }
        }

        public string StepName { get; set; }
        public string StepData { get; set; }

        public string Decision
        {
            get { return JsonConvert.SerializeObject(_DecisionDictionary); }
            set { _DecisionDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(value); }
        }

        [NotMapped]
        public IDictionary<string, string> DecisionDictionary
        {
            get { return _DecisionDictionary; }
        }

        public void SetStepData(object data)
        {
            var type = data.GetType();
            while (type != typeof (object) && type.Assembly.IsDynamic) type = type.BaseType;
            StepData = type.FullName + "#" + JsonConvert.SerializeObject(data, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }

        public object GetStepData()
        {
            var data = StepData;
            var typeFullname = data.Substring(0, data.IndexOf("#"));
            var serializedData = data.Substring(data.IndexOf("#") + 1);
            var type = Type.GetType(typeFullname);
            var ret = JsonConvert.DeserializeObject(serializedData, type);
            return ret;
        }

        #region IProcessStep.Actions

        IEnumerable<IProcessStepAction> IProcessStep.Actions
        {
            get { return Actions; }
            set { Actions = new Collection<ProcessStepAction>(value.Cast<ProcessStepAction>().ToList()); }
        }

        void IProcessStep.AddAction(IProcessStepAction toAdd)
        {
            if (Actions == null)
            {
                Actions = new List<ProcessStepAction>();
            }
            Actions.Add((ProcessStepAction) toAdd);
        }

        IProcessStepAction IProcessStep.CreateAction()
        {
            return new ProcessStepAction();
        }

        #endregion
    }
}