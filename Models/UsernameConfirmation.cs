﻿using Metis.Core.DataObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public partial class UsernameConfirmation
    {
        public const string PROP_CHANGED_EMAIL = "Email";
        public const string PROP_CHANGED_USERNAME = "Username";
        public const string PROP_CHANGED_2FA = "Two-Step Verification";
    }

    public partial class UsernameConfirmation : DataObjectBaseWithHistory<UsernameConfirmation>
    {
        public int UsernameConfirmationId { get; set; }

        public override int PrimaryKey
        {
            get { return UsernameConfirmationId; }
            set { UsernameConfirmationId = value; }
        }

        public int MetisUserId { get; set; }
        [ForeignKey("MetisUserId")]
        public virtual MetisUser MetisUser { get; set; }

        public string NewUsername { get; set; }
        public string Token { get; set; }
        public bool Confirmed { get; set; }
        public string BaseUrl { get; set; }
        public string PropChanged { get; set; }
    }
}
