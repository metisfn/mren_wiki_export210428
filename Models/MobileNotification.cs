﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class MobileNotificationLog : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MobileNotificationLogId { get; set; }

        public int MetisUserId { get; set; }
        [ForeignKey("MetisUserId")]
        public MetisUser MetisUser { get; set; }
        public string Status { get; set; }
        public string Data { get; set; }
        public string Response { get; set; }

        public DateTimeOffset? DateDisplayed { get; set; }
        public string DisplayedPlayerIds { get; set; }

        public DateTimeOffset? DateClicked { get; set; }
        public string ClickedPlayerIds { get; set; }

        public override int PrimaryKey
        {
            get { return MobileNotificationLogId; }
            set { MobileNotificationLogId = value; }
        }
    }
}
