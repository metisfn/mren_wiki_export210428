﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Valuation.Logic.Models
{
    public class MetisDevOpsNotification
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MetisDevOpsNotificationId { get; set; }

        public string Message { get; set; }
        //When to start showing this message
        public DateTimeOffset BeginNotificationDate { get; set; }
        //When downtime begins
        public DateTimeOffset DowntimeDate { get; set; }
        public int DowntimeMinutes { get; set; }
    }
}