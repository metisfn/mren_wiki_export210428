﻿using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("Projects")]
    public partial class Project
    {
        public const string PERMISSION_NDA_READ = "NdaRead";
        public const string PERMISSION_MANAGER = "Manager";
        public const string PERMISSION_INVITED = "Invited";

        public const string PERMISSION_CONTRIBUTABLE = "Contributable";

        public const string STATUS_STAGING = "Staging";
        public const string STATUS_ON_MARKET = "OnMarket";
        public const string STATUS_FINALIZING = "Finalizing";
        public const string STATUS_CLOSED = "Closed";
        public const string STATUS_TRANSFERRED = "Transferred";

        public const string MARKETVISIBILITY_GLOBAL = "Global";
        public const string MARKETVISIBILITY_GRAYLABEL = "GrayLabel";
        public const string MARKETVISIBILITY_PRIVATE = "Private";

        //All those with teaser access can see the files in this room
        public const string PERMISSION_GROUPING_OPEN_MROOM = "OpenMRoom";

        //All with NdaRead wills ee this one
        public const string PERMISSION_GROUPING_POST_NDA_MROOM = "PostNDAMRoom";
    }
}