﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public partial class WorkRoomParticipantInvite
    {
        public const string PERMISSION_INITIATOR = "Initiator";

        public const string STATUS_PENDING = "Pending";
        public const string STATUS_ACCEPTED = "Accepted";
        public const string STATUS_REJECTED = "Rejected";

    }
}
