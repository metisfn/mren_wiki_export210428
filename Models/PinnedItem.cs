﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class PinnedItem : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRoomPinnedItemId { get; set; }

        public int WorkRoomId { get; set; }
        public string ConcerningTargetType { get; set; }
        public int TargetPrimaryKeyId { get; set; }

        public override int PrimaryKey
        {
            get { return WorkRoomPinnedItemId; }
            set { WorkRoomPinnedItemId = value; }
        }
    }
}

