﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    public partial class ScheduledNotification : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ScheduledNotificationId { get; set; }

        public string Name { get; set; }

        public string ConcerningType { get; set; }

        public int ConcerningPrimaryKey { get; set; }

        [Validators("required")]
        public DateTimeOffset StartDate { get; set; }

        public DateTimeOffset NextDate { get; set; }

        [MetisLookup("NotificationFrequency")]
        public NotificationFrequency Frequency { get; set; }

        public string Subject { get; set; }

        [Validators("required")]
        public string Message { get; set; }

        public override int PrimaryKey 
        { 
            get { return ScheduledNotificationId; } 
            set { ScheduledNotificationId = value; }
        }
    }
}
