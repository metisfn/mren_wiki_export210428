/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Process.Interfaces;
using Newtonsoft.Json;

namespace Valuation.Logic.Models
{
    public class ProcessStepAction : DataObjectBase, IProcessStepAction, IMetisScaffold
    {
        private Dictionary<string, string> _ActionDataDictionary = new Dictionary<string, string>();
        public virtual ProcessStep ProcessStep { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProcessStepActionId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ProcessStepActionId; }
            set { ProcessStepActionId = value; }
        }

        public string ActionName { get; set; }

        public string ActionData
        {
            get { return JsonConvert.SerializeObject(_ActionDataDictionary); }
            set { _ActionDataDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(value); }
        }

        [NotMapped]
        public IDictionary<string, string> ActionDataDictionary
        {
            get { return _ActionDataDictionary; }
        }

        public int Order { get; set; }
        public int ProcessStepId { get; set; }

        IProcessStep IProcessStepAction.ProcessStep
        {
            get { return ProcessStep; }
            set { ProcessStep = (ProcessStep) value; }
        }
    }
}