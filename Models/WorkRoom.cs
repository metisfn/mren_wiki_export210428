﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace Valuation.Logic.Models
{
    [ODataPath("WorkRooms")]
    public partial class WorkRoom : DataObjectBaseWithHistory<WorkRoom>, IFilesystemOwner
    {
        public WorkRoom()
        {
        }

        public WorkRoom(int id)
        {
            WorkRoomId = id;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRoomId { get; set; }

        public string Name { get; set; }

        [ForeignKey("ParentWorkRoomId")]
        public virtual WorkRoom ParentWorkRoom { get; set; }

        public int? ParentWorkRoomId { get; set; }
        public int? SourceOrgId { get; set; }
        public int? TargetOrgId { get; set; }

        public override int PrimaryKey
        {
            get { return WorkRoomId; }
            set { WorkRoomId = value; }
        }

        public int ConversationId { get; set; }
        public string ConcerningTargetType { get; set; }
        public int? ConcerningTargetTypeId { get; set; }
        //temporary until we get Org Profiles....
        public string SourceOrg { get; set; }
        public string TargetOrg { get; set; }
        public string Type { get; set; }
        public string SubName { get; set; }

        public string TargetOrgIds { get; set; }

        public IList<int?> ParentWorkRoomIds { get; set; }
        public IList<int> ChildWorkRoomIds { get; set; }

        [NotMapped, JsonIgnore]
        public List<List<int>> Paths { get; set; }

        //This means the owner of the conf room archived it
        //If an edge is archived, let's say that means the other side archived.
        public bool Archived { get; set; }
        public int? RootFilesystemNodeId { get; set; }

        [DefaultValue(true)]
        public bool AllowChat { get; set; }

        public int? PostNDAWorkRoomId { get; set; }
        public int? PreNDAWorkRoomId { get; set; }

        [DefaultValue(WorkRoom.INVITE_MODE_TEAM)]
        public string InviteMode { get; set; }

        public Guid? VersionId { get; set; }
        public bool Hidden { get; set; }

        [NotMapped]
        public bool IsActivatedUnderSubscription { get; set; }

        public string Description { get; set; }

        public bool IsPublic { get; set; }

        [MetisMetadataDataType("MetisUserId")]
        public string GuestMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> GuestMetisUserIdsList => this.GuestMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.GuestMetisUserIds);

        [MetisMetadataDataType("MetisUserId")]
        public string AssignedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> AssignedMetisUserIdsList => this.AssignedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.AssignedMetisUserIds);

        public string Content { get; set; }
        public int? ContentAuthorId { get; set; }
        public int? ContentEditorId { get; set; }        
    }
}