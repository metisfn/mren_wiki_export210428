namespace Valuation.Logic.Models
{
    public interface IWorkRoomFilesystemNodeGrant
    {
        int? FilesystemNodeId { get; set; }
        string TagText { get; set; }
        int OwnedByWorkRoomId { get; set; }
        bool Readable { get; set; }
        bool Shareable { get; set; }
        bool Contributable { get; set; }
        // AC 20160104 - Needed for restricting wm downloads (Ren stuff)
        //bool RestrictDownloadable { get; set; }
    }
}