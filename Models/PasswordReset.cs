﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    public interface IPasswordReset : IMetisDataObjectWithHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int PasswordResetId { get; set; }

        string Email { get; set; }
        int UserId { get; set; }

        [ForeignKey("UserId")]
        MetisUser User { get; set; }

        string Step { get; set; }
        string Token { get; set; }
    }

    [ODataPath("PasswordResets")]
    public partial class PasswordReset : DataObjectBaseWithHistory<PasswordReset>, IPasswordReset
    {
        public DateTimeOffset Expiration { get; set; }
        public string BaseUrl { get; set; }
        public bool ProactiveSupportGenerated { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PasswordResetId { get; set; }

        public string Email { get; set; }

        public override int PrimaryKey
        {
            get { return PasswordResetId; }
            set { PasswordResetId = value; }
        }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual MetisUser User { get; set; }

        public string Step { get; set; }
        public string Token { get; set; }
    }
}