﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("Tags")]
    public class Tag : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TagId { get; set; }

        public override int PrimaryKey
        {
            get { return TagId; }
            set { TagId = value; }
        }

        public int ConcerningPrimaryKeyId { get; set; }

        public string ConcerningTargetType { get; set; }

        public Guid? ConcerningObjectId { get; set; }

        public Guid? ConcerningRevisionId { get; set; }

        public string Color { get; set; }

        public string Text { get; set; }

        public int? MetisUserId { get; set; }
        public int? OrganizationId { get; set; }
    }
}