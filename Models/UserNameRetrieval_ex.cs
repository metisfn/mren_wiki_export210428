﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public partial class UserNameRetrieval
    {
        public const string RETRIEVAL_SENT = "RetrievalSent";
        public const string RETRIEVAL_USED = "RetrievalUsed";
    }
}
