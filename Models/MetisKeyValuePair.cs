﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Valuation.Logic.Models
{
    public class MetisKeyValuePair
    {
        public const string OPTION_STALECONVOINTERVAL = "StaleConvoInterval";
        public const string OPTION_DISABLE_SENDGRID_INBOUND_EMAIL = "DisableSendGridInboundEmail";
        public const string OPTION_SYNC_DELIVERIES_INTERVAL_SEC = "SyncDeliveriesIntervalSec";
        public const string OPTION_DISABLE_PUSH_LOG = "DisablePushNotificationLogs";

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MetisKeyValuePairId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}