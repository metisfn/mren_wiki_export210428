/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
    public class DataColumn : DataObjectBase, IDataColumn, IMetisScaffold
    {
        public virtual DataObject Object { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DataColumnId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return DataColumnId; }
            set { DataColumnId = value; }
        }

        public string ColumnLabel { get; set; }
        public string ColumnName { get; set; }
        public string ColumnType { get; set; }

        IDataObject IDataColumn.Object
        {
            get { return Object; }
            set { Object = (DataObject) value; }
        }

        public int DataObjectId { get; set; }
        public int? ConfigurationId { get; set; }
        public bool IsLookup { get; set; }
        public bool IsVisible { get; set; }
        public string LookupName { get; set; }
        public string DependsOnProperty { get; set; }
        public int SortOrder { get; set; }
        public string Format { get; set; }
        public string Validation { get; set; }
        public string JsComputed { get; set; }
    }
}