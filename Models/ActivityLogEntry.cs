/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
    public class ActivityLogEntry : DataObjectBase, IActivityLogEntry, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ActivityLogEntryId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ActivityLogEntryId; }
            set { ActivityLogEntryId = value; }
        }

        public string Activity { get; set; }
        public DateTimeOffset ActivityDate { get; set; }
        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public string PageURL { get; set; }
        public int? TargetPrimaryKeyId { get; set; }
        public string TargetType { get; set; }
        public string PageName { get; set; }
        public string IPAddress { get; set; }
        public Guid? TargetRevisionId { get; set; }
        public Guid? ActivityGroupId { get; set; }
        public string ActivityData { get; set; }
    }
}