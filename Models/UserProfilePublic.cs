﻿using System;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects.Attributes;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel;

namespace Valuation.Logic.Models
{
    public class UserProfilePublicShell
    {
        public UserProfilePublicShell(UserProfilePublic upp)
        {
            UserProfilePublicShellId = Guid.NewGuid();
            UserProfilePublicId = upp.UserProfilePublicId;
            MetisUserId = upp.MetisUserId;
            MetisUser = upp.MetisUser;
            GalleryId = upp.GalleryId;
            Gallery = upp.Gallery;
            FirstName = upp.FirstName;
            LastName = upp.LastName;
            Name = upp.Name;
            Email = upp.Email;
        }

        [Key]
        public Guid UserProfilePublicShellId { get; set; }

        public int UserProfilePublicId { get; set; }
        public int MetisUserId { get; set; }
        public virtual MetisUser MetisUser { get; set; }
        public int? GalleryId { get; set; }
        public virtual Gallery Gallery { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String UITheme { get; set; }
    }

    [ODataPath("UserProfilePublics")]
    public class UserProfilePublic : DataObjectBaseWithHistory<UserProfilePublic>, ISubscribable, IProfileImageOwner, ITagListOwner
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 UserProfilePublicId { get; set; }

        [NotMapped]
        public override Int32 PrimaryKey { get { return UserProfilePublicId; } set { UserProfilePublicId = value; } }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public string Name { get; set; }
        public String Company { get; set; }
        public String Title { get; set; }
        public String Email { get; set; }

        public int MetisUserId { get; set; }
        [ForeignKey("MetisUserId")]
        public virtual MetisUser MetisUser { get; set; }

        public int? GalleryId { get; set; }
        [ForeignKey("GalleryId")]
        public virtual Gallery Gallery { get; set; }

        [MetisMetadataDataType("MetisUserId")]
        public string SubscribedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore]
        public virtual ICollection<int> SubscribedMetisUserIdsList => this.SubscribedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.SubscribedMetisUserIds);
        

        public string ProfileImageUrl { get; set; }

        // Ex: AcceptInvite/dkempner@mren.com/<GUID>
        public string InviteUrlPath { get; set; }

        public string TagList { get; set; }

        [DefaultValue(true)]
        public bool CanSearchPublic { get; set; }

        [NotMapped, MetaIgnore, JsonIgnore]
        public IList<string> Tags
        {
            get
            {
                if (string.IsNullOrWhiteSpace(TagList)) return new List<string>();
                 else return TagList.Split(',');
            }
        }
    }
}
