﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valuation.Logic.Models
{
    public partial class NotificationJobLog
    {
        public const string STATUS_IN_PROGRESS = "InProgress";
        public const string STATUS_FAILED = "Failed";
        public const string STATUS_COMPLETED = "Completed";
    }
}
