/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.Conversations.Interfaces;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class Conversation : DataObjectBase, IConversation, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 ConversationId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return ConversationId; } set { ConversationId = value; } }
		public Int32 AuthorUserId { get; set; }
		public Int32? ConcerningPrimaryKeyId { get; set; }
		public String ConcerningTargetType { get; set; }
		public Guid? ConcerningObjectId { get; set; }
		public Guid? ConcerningRevisionId { get; set; }
		public virtual ICollection<ConversationItem> ConversationItems { get; set; }
		#region IConversation.ConversationItems
		IEnumerable<IConversationItem> IConversation.ConversationItems { get { return this.ConversationItems; } set { this.ConversationItems = new Collection<ConversationItem>(value.Cast<ConversationItem>().ToList()); } }
		void IConversation.AddConversationItem(IConversationItem toAdd) 
		{
			if(this.ConversationItems == null)
			{
				this.ConversationItems = new List<ConversationItem>(); 
			}
			this.ConversationItems.Add((ConversationItem)toAdd); 
		}
		IConversationItem IConversation.CreateConversationItem() { return new ConversationItem(); }
		#endregion
		public Int32 ParentPrimaryKeyId { get; set; }
		public String ParentTargetType { get; set; }
		public Guid ParentObjectId { get; set; }
		public Guid? ParentRevisionId { get; set; }
		public String ConversationType { get; set; }
		public Int32? RootFilesystemNodeId { get; set; }
	}
}
		