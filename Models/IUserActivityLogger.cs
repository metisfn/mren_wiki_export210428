﻿using System;
using System.Threading.Tasks;
using Hangfire;
using Metis.Core.DataObjects;
using Newtonsoft.Json;
using Valuation.Logic.EventDetectors;
using System.Collections.Generic;
using Autofac;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions;
using Metis.Core.Permissions.Interfaces;
using Valuation.Logic.HangfireJobs;

namespace Valuation.Logic.Models
{
    /// <summary>
    /// Notsolightweight activity handler
    /// </summary>
    public interface IImmediateUserActivityHandler
    {
        Task OnLogUserActivity<TConcerningObject, TParentDataObject>(TConcerningObject concerning, TParentDataObject parent,
            UserActivityType activityType, string requestUrl = null, bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new()
            where TParentDataObject : class, IMetisDataObject, new();
    }

    /// <summary>
    /// Lightweight activity matcher
    /// </summary>
    public interface IImmediateUserActivityMatcher
    {
        Type Handler { get; }
        Task<bool> DoesMatch<TConcerningObject, TParentDataObject>(TConcerningObject concerning, TParentDataObject parent,
            UserActivityType activityType, string requestUrl = null, bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new()
            where TParentDataObject : class, IMetisDataObject, new();
    }

    public interface IUserActivityLogger
    {
        Task<UserActivity> LogUserActivity<TConcerningObject, TParentDataObject>(TConcerningObject concerning,
            TParentDataObject parent,
            UserActivityType activityType, string requestUrl = null, bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new()
            where TParentDataObject : class, IMetisDataObject, new();

        Task<UserActivity> LogUserActivity<TConcerningObject>(TConcerningObject concerning,
            UserActivityType activityType, string requestUrl = null,
            bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new();

        

        /// <summary>
        /// Bypasses the background queueing mechanism to push activity to front end immediately. Use sparingly for stuff that has to happen right now.
        /// New chat messages are the first use case.
        /// </summary>
        Task<UserActivity> LogUserActivityImmediately<TConcerningObject, TParentDataObject>(
            TConcerningObject concerning,
            TParentDataObject parent,
            UserActivityType activityType, string requestUrl = null, bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new()
            where TParentDataObject : class, IMetisDataObject, new();

        /// <summary>
        /// Bypasses the background queueing mechanism to push activity to front end immediately. Use sparingly for stuff that has to happen right now.
        /// New chat messages are the first use case.
        /// </summary>
        Task<UserActivity> LogUserActivityImmediately<TConcerningObject>(TConcerningObject concerning,
            UserActivityType activityType, string requestUrl = null,
            bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new();
    }
    public class UserActivityLogger : IUserActivityLogger
    {
        private readonly IComponentContext _afContext;
        private readonly IMetisUser _currentUser;
        private readonly IValuationDataContext _db;
        private readonly IExecutionContext _exContext;
        private readonly IEnumerable<IImmediateUserActivityMatcher> _matchers;
        private readonly IPermissionManager _pm;
        private readonly IDistributionHubSender _sender;
        private readonly bool _useHangfireServer;
        public UserActivityLogger(IValuationDataContext db, IPermissionManager pm, IDistributionHubSender sender,
            IMetisUser currentUser, IExecutionContext exContext, IEnumerable<IImmediateUserActivityMatcher> matchers, IComponentContext afContext)
        {
            _db = db;
            _pm = pm;
            _sender = sender;
            _currentUser = currentUser;
            _exContext = exContext;
            _matchers = matchers;
            _afContext = afContext;

            // TODO set to the actual value
            _useHangfireServer = false;
        }

        public Task<UserActivity> LogUserActivity<TConcerningObject>(TConcerningObject concerning,
            UserActivityType activityType,
            string requestUrl = null, bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new()
        {
            return LogUserActivity<TConcerningObject, PermissionTarget>(concerning, null, activityType, requestUrl,
                false, customData);
        }

        public async Task<UserActivity> LogUserActivity<TConcerningObject, TParentDataObject>(
            TConcerningObject concerning, TParentDataObject parent,
            UserActivityType activityType, string requestUrl = null, bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new()
            where TParentDataObject : class, IMetisDataObject, new()
        {
            if (MetisAppSettings.AM_I_READ_ONLY_APP) return await Task.FromResult<UserActivity>(null);
            var toAdd = await CreateUserActivityInternal(concerning, parent, activityType, isActionable, customData);

            //if (concerning is FilesystemNode && activityType == UserActivityType.Create)
            //{
            //    // ensure we run this on hangfire server if available
            //    if (_useHangfireServer)
            //    {
            //    }
            //    // this will ensure only top level primitives will be serialized
            //    var cleanFileData = (concerning as FilesystemNode).GetPOCOCopy(_db);
            //    BackgroundJob.Enqueue<ImageJobs>(job => job.ConvertFileToImages(cleanFileData));
            //}

            BackgroundJob.Enqueue<TriggeredEventJob>(job => job.Process(toAdd));

            return toAdd;
        }

        public async Task<UserActivity> LogUserActivityImmediately<TConcerningObject, TParentDataObject>(
            TConcerningObject concerning, TParentDataObject parent,
            UserActivityType activityType, string requestUrl = null, bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new()
            where TParentDataObject : class, IMetisDataObject, new()
        {
            var toAdd = await CreateUserActivityInternal(concerning, parent, activityType, isActionable, customData);

            _sender.DistributeActivity(toAdd);
            return toAdd;
        }

        public Task<UserActivity> LogUserActivityImmediately<TConcerningObject>(TConcerningObject concerning,
            UserActivityType activityType,
            string requestUrl = null, bool isActionable = false, object customData = null)
            where TConcerningObject : class, IMetisDataObject, new()
        {
            return LogUserActivityImmediately<TConcerningObject, PermissionTarget>(concerning, null, activityType,
                requestUrl,
                false, customData);
        }

        private async Task<UserActivity> CreateUserActivityInternal<TConcerningObject, TParentDataObject>(
            TConcerningObject concerning,
            TParentDataObject parent, UserActivityType activityType, bool isActionable, object customData)
            where TConcerningObject : class, IMetisDataObject, new()
            where TParentDataObject : class, IMetisDataObject, new()
        {
            var concerningPt = concerning as IPermissionTarget;
            var objectId = concerningPt != null ? concerningPt.TargetObjectId : concerning.ObjectId;
            var primaryKey = concerningPt != null ? concerningPt.TargetPrimaryKeyId : concerning.PrimaryKey;
            var targetType = concerningPt != null ? concerningPt.TargetType : concerning.DataObjectTargetType;


            var parentPt = parent as IPermissionTarget;
            Guid? parentObjectId = null;
            int? parentPrimaryKey = null;
            string parentTargetType = null;

            if (parent != null)
            {
                parentObjectId = parentPt != null ? parentPt.TargetObjectId : parent.ObjectId;
                parentPrimaryKey = parentPt != null ? parentPt.TargetPrimaryKeyId : parent.PrimaryKey;
                parentTargetType = parentPt != null ? parentPt.TargetType : parent.DataObjectTargetType;
            }


            var toAdd = new UserActivity
            {
                ConcerningObjectId = objectId,
                ConcerningPrimaryKeyId = primaryKey,
                ConcerningTargetType = targetType,
                UserId = _currentUser.MetisUserId == 0 ? 1 : _currentUser.MetisUserId,
                UserContactId = _currentUser.ContactId == 0 ? 1 : _currentUser.ContactId,
                UserActivityType = activityType,
                ActivityDate = DateTimeOffset.UtcNow,
                ParentObjectId = parentObjectId,
                ParentPrimaryKeyId = parentPrimaryKey,
                ParentTargetType = parentTargetType,
                IsActionable = isActionable,
                ClientToken = _exContext.RequestToken,
                IPAddress = _exContext.IpAddress
            };

            if (customData != null)
            {
                var jsonCustomData = JsonConvert.SerializeObject(customData, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                toAdd.CustomData = jsonCustomData;
            }

            _db.AddToContext(toAdd);
            await _db.SaveChangesAsync();

            foreach(var matcher in _matchers)
            {
                if(await matcher.DoesMatch(concerning, parent, activityType, null, isActionable, customData))
                {
                    using (var scope = _afContext.Resolve<ILifetimeScope>().BeginLifetimeScope())
                    {
                        var handler = scope.Resolve(matcher.Handler) as IImmediateUserActivityHandler;
                        await handler.OnLogUserActivity(concerning, parent, activityType, null, isActionable, customData);
                    }
                }
            }

            return toAdd;
        }
    }
}