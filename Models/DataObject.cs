/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class DataObject : DataObjectBase, IDataObject, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 DataObjectId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return DataObjectId; } set { DataObjectId = value; } }
		public virtual ICollection<DataColumn> Columns { get; set; }
		#region IDataObject.Columns
		IEnumerable<IDataColumn> IDataObject.Columns { get { return this.Columns; } set { this.Columns = new Collection<DataColumn>(value.Cast<DataColumn>().ToList()); } }
		void IDataObject.AddColumn(IDataColumn toAdd) 
		{
			if(this.Columns == null)
			{
				this.Columns = new List<DataColumn>(); 
			}
			this.Columns.Add((DataColumn)toAdd); 
		}
		IDataColumn IDataObject.CreateColumn() { return new DataColumn(); }
		#endregion
		public Int32? ConfigurationId { get; set; }
		public Boolean IsPermissionBound { get; set; }
		public String Label { get; set; }
		public String Name { get; set; }
		public String ODataPath { get; set; }
	}
}
		