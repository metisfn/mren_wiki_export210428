﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Valuation.Logic.DTOs;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public interface ICustomFieldsOwner : ICustomMetaOwner
    {
        string CustomFields { get; set; }

    }

    public interface ICustomMetaOwner : IMetisDataObject
    {
        string CustomMeta { get; set; }
        [NotMapped, JsonIgnore]
        IList<CustomMeta> CustomMetaList { get; set; }
    }
}
