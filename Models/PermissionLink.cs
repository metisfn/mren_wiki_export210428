/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class PermissionLink : DataObjectBase, IPermissionLink, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionLinkId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionLinkId; }
            set { PermissionLinkId = value; }
        }

        public int SourcePermissionTargetId { get; set; }
        public int TargetPermissionTargetId { get; set; }
    }
}