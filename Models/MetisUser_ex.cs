﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    public enum UserType
    {
        Readonly,
        General,
        SiteAdmin,
        Lawyer
    }


    [ODataPath("MetisUsers")]
    public partial class MetisUser
    {
        public const string PERMISSION_GROUPING_NOTIFICATIONS_LIST = "NotificationsList";
        public const string PERMISSION_GROUPING_CONVERSATIONS_LIST = "ConversationsList";
        public const string PERMISSION_GROUPING_OWNED_ISSUES = "OwnedIssues";
        public const string PERMISSION_GROUPING_INVOLVED_ISSUES = "InvolvedIssues";
        public const string PERMISSION_GROUPING_PROJECT_MANAGER = "ProjectManager";
        public const string PERMISSION_ACCEPT_OFFER = "AcceptOffer";
        public const string PERMISSION_GROUPING_SIGNED_NDA_PROJECTS = "SignedNDAProjects";
        public const string PERMISSION_GROUPING_SIGNED_NDA_WORKROOMS = "SignedNDAWorkRooms";
        public const string PERMISSION_GROUPING_WORKROOMS_INVITED_TO = "WorkRoomsInvitedTo";
        public const string PERMISSION_GROUPING_WORKROOMS_INVITES = "WorkRoomInvites";

        [MetisMetadataDataType("datetime")]
        public DateTimeOffset? LastPasswordResetEmailSent { get; set; }

        [MetisMetadataDataType("datetime")]
        public DateTimeOffset? LastUserNameRetrievalEmailSent { get; set; }

        public DateTimeOffset? LastProactiveSupportEmailSent { get; set; }

        [MetisMetadataDataType("datetime")]
        public DateTimeOffset? LastLogin { get; set; }

        [MetisMetadataDataType("datetime")]
        public DateTimeOffset? FirstLogin { get; set; }

        [MetisMetadataDataType("datetime")]
        public DateTimeOffset? InviteAcceptedOn { get; set; }

        [MetisLookup("UserType")]
        public UserType UserType { get; set; }

        public bool IsDisabled { get; set; }
        public bool DisableSync { get; set; }

        public int? MetisIdentityId { get; set; }

        [ForeignKey("MetisIdentityId")]
        public virtual MetisIdentity MetisIdentity { get; set; }

        public string UserName { get; set; }

        public string IsAnon { get; set; }
        public const string ANON_TYPE_ANON = "Anon";
    }
}