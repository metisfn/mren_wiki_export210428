﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Metis.Core.FileRepository.Interfaces;

namespace Valuation.Logic.Models
{
    public interface IValuationDataContext : IBaseDataContext
    {
        IQueryable<UserInvite> UserInvites { get; }
        IQueryable<UserEmailConfirmation> UserEmailConfirmations { get; }
        IQueryable<PasswordReset> PasswordResets { get; }
        IQueryable<UserActivity> UserActivities { get; }
        IQueryable<PropertyChangeNotificationEvent> PropertyChangeNotificationEvents { get; }
        IQueryable<Organization> Organizations { get; }
        IQueryable<Asset> Assets { get; }
        IQueryable<Project> Projects { get; }
        IQueryable<Deal> Deals { get; }
        IQueryable<OpportunityInvite> OpportunityInvites { get; }
        IQueryable<NdaSubmission> NdaSubmissions { get; }
        IQueryable<Gallery> Galleries { get; }
        IQueryable<Nda> Ndas { get; }
        MetisUser AdminUser { get; }
        IQueryable<UnderConsideration> UnderConsiderations { get; }
        IQueryable<OrganizationInvite> OrganizationInvites { get; }
        IQueryable<ScheduledNotification> ScheduledNotifications { get; }
        IQueryable<UserProfilePublic> UserProfilePublics { get; }
        IQueryable<UserProfilePrivate> UserProfilePrivates { get; }
        IQueryable<Tag> Tags { get; }
        IQueryable<TagColor> TagColors { get; }
        IQueryable<OrganizationProfile> OrganizationProfiles { get; }
        IQueryable<OpportunityAnalytic> OpportunityAnalytics { get; }
        IQueryable<WorkRoom> WorkRooms { get; }
        IQueryable<WorkRoomInvite> WorkRoomInvites { get; }
        IQueryable<WorkRoomParticipantInvite> WorkRoomParticipantInvites { get; }
        IQueryable<OwnedWorkRoomFilesystemNodeGrant> OwnedWorkRoomFilesystemNodeGrants { get; }
        IQueryable<GrantedWorkRoomFilesystemNodeGrant> GrantedWorkRoomFilesystemNodeGrants { get; }
        IQueryable<Relationship> Relationships { get; }
        IQueryable<RelationshipLink> RelationshipLinks { get; }
        IQueryable<NotificationsEmailLog> NotificationsEmailLogs { get; }
        IQueryable<NotificationJobLog> NotificationJobLogs { get; }
        IQueryable<WorkRoomFileActivity> WorkRoomFileActivities { get; }
        IQueryable<ImageNode> ImageNodes { get; }
        IQueryable<MetisOneTimeDataMigration> MetisOneTimeDataMigrations { get; }
        IQueryable<MetisEmailLog> MetisEmailLogs { get; }
        IQueryable<GrayLabel> GrayLabels { get; }
        IQueryable<GrayLabelOrganization> GrayLabelOrganizations { get; }
        IQueryable<GrayLabelNdaAcceptance> GrayLabelNdaAcceptances { get; }
        IQueryable<TeaserInvite> TeaserInvites { get; }
        IQueryable<InviteJobsLog> InviteJobsLogs { get; }
        IQueryable<WorkRoomEdge> WorkRoomEdges { get; }
        IQueryable<ProjectGrant> ProjectGrants { get; }
        IQueryable<OrgSuggestion> OrgSuggestions { get; }
        IQueryable<MetisDevOpsNotification> MetisDevOpsNotifications { get; }
        IQueryable<UsernameConfirmation> UsernameConfirmations { get; }
        IQueryable<UserSubscription> UserSubscriptions { get; }
        IQueryable<MobileNotificationLog> MobileNotificationLogs { get; }
        IQueryable<DesktopSyncActivity> DesktopSyncActivities { get; }
        IQueryable<SyncClientConfig> SyncClientConfigs { get; }
        IQueryable<RelationshipLinkInvestment> RelationshipLinkInvestments { get; }
        Database Database { get; }
        IQueryable<OrgSubscription> OrgSubscriptions { get; }
        IQueryable<OrgTransaction> OrgTransactions { get; }
        IQueryable<OrgProfilePrivate> OrgProfilePrivates { get; }
        IQueryable<FilesystemNodeRead> FilesystemNodeReads { get; }
        IQueryable<UserFavorite> UserFavorites { get; }
        //IQueryable<ProjectActivationBypassToken> ProjectActivationBypassTokens { get; }
        IQueryable<PushNotificationRegistration> PushNotificationRegistrations { get; }
        IQueryable<FilesystemNodeUserActivity> FilesystemNodeUserActivities { get; }
        IQueryable<MetisIdentity> MetisIdentities { get; }
        IQueryable<UserNameRetrieval> UserNameRetrievals { get; }
        IQueryable<OrganizationPreference> OrganizationPreferences { get; }
        IQueryable<MetisKeyValuePair> MetisKeyValuePairs { get; }
        IQueryable<MetisUserSkipWorkRoom> MetisUserSkipWorkrooms { get; }
        IQueryable<WorkRoomFileActivityBatch> WorkRoomFileActivityBatches { get; }
        Task<List<IFilesystemNode>> GetFilesystemNodesFromRootIncludingDeleted(int filesystemNodeRootId);

        IQueryable<ConversationReadStatus> ConversationReadStatus { get; }
        IQueryable<RemoteHangfireQueueFallback> RemoteHangfireQueueFallbacks { get; }
        IQueryable<ActivityAlert> ActivityAlerts { get; }
        IQueryable<SendGridEvent> SendGridEvents { get; }
        IQueryable<DigestSubscription> DigestSubscriptions { get; }
        IQueryable<UserCredentialLog> UserCredentialLogs { get; }
        IQueryable<SmsDelivery> SmsDeliveries { get; }
        IQueryable<UserCachedChanges> UserCachedChanges { get; }
        IQueryable<Webhook> Webhooks { get; }
        IQueryable<Mention> Mentions { get; }

        IQueryable<PinnedItem> PinnedItems { get; }
        IQueryable<ProjectAssetPair> ProjectAssetPairs { get; }
        IQueryable<DistroMessage> DistroMessages { get; }
        IQueryable<FilesystemNodeGrant> FilesystemNodeGrants { get; }
        IQueryable<DistroMessageMetisUser> DistroMessageMetisUsers { get; }
        IQueryable<DistroMessageLog> DistroMessageLogs { get; }
    }
}