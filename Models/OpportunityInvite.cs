﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class OpportunityInvite : DataObjectBaseWithHistory<OpportunityInvite>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OpportunityInviteId { get; set; }

        public override int PrimaryKey
        {
            get { return OpportunityInviteId; }
            set { OpportunityInviteId = value; }
        }

        public int ProjectId { get; set; }
        //[MetaIgnore]
        //public virtual Project Project { get; set; }
        public int? MetisUserId { get; set; }
        public int? FromMetisUserId { get; set; }
        //public virtual MetisUser MetisUser { get; set; }
        public int? OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
        public string Status { get; set; }
        public int? RequestedByUserId { get; set; }
        public string InvitedType { get; set; }
        public int? WorkRoomId { get; set; }
        public bool NewUser { get; set; }
        // denormalized: capture email at the time of entry so we can display without over-exposing user's true/normalized contact info
        public string Email { get; set; }
        public DateTimeOffset? LastInviteSent { get; set; }

        public bool UseMetisUsersNameInEmail { get; set; }
        public int? FromUserId { get; set; }
        //Because we can't depending on project org due to being able to share deal summary
        public int? FromOrganizationId { get; set; }
        public string CustomMessage { get; set; }
        public string CustomSubject { get; set; }
        public string SendAsName { get; set; }
        public string SendAsEmail { get; set; }
        public DateTimeOffset LastTimeViewedFromEmail { get; set; }

        public string BaseUrl { get; set; }

        public DateTimeOffset? DateInviteCompleted { get; set; }
        public int? InviteCompletedByMetisUserId { get; set; }
    }
}