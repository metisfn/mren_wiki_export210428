/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class UserPermissionCache : DataObjectBase, IUserPermissionCache, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 UserPermissionCacheId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return UserPermissionCacheId; } set { UserPermissionCacheId = value; } }
		public Int32 RowPrimaryKeyId { get; set; }
		public Int32 PermissionTargetId { get; set; }
		public Int32 TargetPrimaryKeyId { get; set; }
		public String TargetType { get; set; }
		public Guid TargetObjectId { get; set; }
		public DateTimeOffset RevisionDate { get; set; }
		public Int32 UserId { get; set; }
		[MaxLength(150)]
		public String Permission { get; set; }
		public Int32 PermissionCode { get; set; }
		public Int32 TargetTypeCode { get; set; }
	}
}
		