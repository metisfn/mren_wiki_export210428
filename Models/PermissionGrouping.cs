/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class PermissionGrouping : DataObjectBase, IPermissionGrouping, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionGroupingId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionGroupingId; }
            set { PermissionGroupingId = value; }
        }

        [MaxLength(150)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string OwnerTargetType { get; set; }

        public int OwnerPrimaryKeyId { get; set; }
        public Guid OwnerObjectId { get; set; }
        public int OwnerPermissionTargetId { get; set; }
    }
}