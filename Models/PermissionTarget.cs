/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Metis.Core.Permissions.Interfaces;

namespace Valuation.Logic.Models
{
    public class PermissionTarget : DataObjectBase, IPermissionTarget, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionTargetId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return PermissionTargetId; }
            set { PermissionTargetId = value; }
        }

        public int? ParentPermissionTargetId { get; set; }
        public int TargetPrimaryKeyId { get; set; }
        public Guid TargetObjectId { get; set; }

        [MaxLength(150)]
        public string TargetType { get; set; }
    }
}