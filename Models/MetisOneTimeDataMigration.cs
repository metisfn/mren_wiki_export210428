﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class MetisOneTimeDataMigration : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MetisOneTimeDataMigrationId { get; set; }

        public string Name { get; set; }

        public override int PrimaryKey
        {
            get { return MetisOneTimeDataMigrationId; }
            set { MetisOneTimeDataMigrationId = value; }
        }
    }
}