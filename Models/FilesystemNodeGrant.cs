﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Newtonsoft.Json;

namespace Valuation.Logic.Models
{
    [ODataPath("FilesystemNodeGrants")]
    public class FilesystemNodeGrant : DataObjectBaseWithHistory<FilesystemNodeGrant>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FilesystemNodeGrantId { get; set; }

        [Index("IX_Concerning", 1, IsUnique = false, IsClustered = false)]
        public int FilesystemNodeId { get; set; }

        [MaxLength(50), Index("IX_Concerning", 2, IsUnique = false, IsClustered = false)]
        public string ConcerningTargetType { get; set; }

        [Index("IX_Concerning", 3, IsUnique = false, IsClustered = false)]
        public int ConcerningPrimaryKeyId { get; set; }

        [MetisMetadataDataType("MetisUserId")]
        public string GuestMetisUserIds { get; set; }
        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> GuestMetisUserIdsList => this.GuestMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.GuestMetisUserIds);

        [MetisMetadataDataType("MetisUserId")]
        public string AssignedMetisUserIds { get; set; }
        [NotMapped, MetaIgnore, JsonIgnore]
        public virtual ICollection<int> AssignedMetisUserIdsList => this.AssignedMetisUserIds == null ? new List<int>() : RelationshipLiteExtensions.SplitStringIntoIds(this.AssignedMetisUserIds);

        [NotMapped]
        public bool Host { get; set; }

        public override int PrimaryKey
        {
            get { return FilesystemNodeGrantId; }
            set { FilesystemNodeGrantId = value; }
        }
    }
}