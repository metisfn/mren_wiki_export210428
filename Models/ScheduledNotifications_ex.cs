﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects.Attributes;

namespace Valuation.Logic.Models
{
    [ODataPath("ScheduledNotifications")]
    public partial class ScheduledNotification
    {
        public const string DAILY = "Daily";
        public const string WEEKLY = "Weekly";
        public const string MONTHLY = "Monthly";
        public const string ONCE = "Once";
    }

    public enum NotificationFrequency
    {
        Once,
        Daily,
        Weekly,
        Monthly
    };
}
