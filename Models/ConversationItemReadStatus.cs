/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.Conversations.Interfaces;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;
using Newtonsoft.Json;

namespace Valuation.Logic.Models
{
    public partial class ConversationItemReadStatus : DataObjectBase, IConversationItemReadStatus, IMetisScaffold
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConversationItemReadStatusId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return ConversationItemReadStatusId; }
            set { ConversationItemReadStatusId = value; }
        }

        public int ConversationItemId { get; set; }

        [JsonIgnore]
        public int ContactId { get; set; }

        public string ReadStatus { get; set; }
    }
}