﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Attributes;
using Valuation.Logic.DTOs;

namespace Valuation.Logic.Models
{
    [ODataPath("WorkRoomParticipantInvites")]
    public partial class WorkRoomParticipantInvite : DataObjectBaseWithHistory<WorkRoomParticipantInvite>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRoomParticipantInviteId { get; set; }
        public int WorkRoomId { get; set; }
        public int UserId { get; set; }
        // denormalized: capture email at the time of entry so we can display without over-exposing user's true/normalized contact info
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Status { get; set; }
        public string BaseUrl { get; set; }
        public int InvitingUserId { get; set; }
        public override int PrimaryKey 
        { 
            get { return WorkRoomParticipantInviteId; } 
            set { WorkRoomParticipantInviteId = value; }
        }
    }
}
