﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class UserCredentialLog : DataObjectBase
    {
        public const string WrongUsername = "WrongUsername";
        public const string WrongPassword = "WrongPassword";

        public int UserCredentialLogId { get; set; }
        public string AttemptedUsername { get; set; }
        public string AttemptedPasswordHash { get; set; }
        public string BaseUrl { get; set; }
        public string RejectionReason { get; set; }
        public int MetisUserId { get; set; }
        public bool Successful { get; set; }
        public bool SentSupport { get; set; }

        public override int PrimaryKey
        {
            get { return UserCredentialLogId; }
            set { UserCredentialLogId = value; }
        }
    }
}
