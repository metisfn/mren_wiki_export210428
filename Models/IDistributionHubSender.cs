﻿using Metis.Core.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Valuation.Logic.DTOs;

namespace Valuation.Logic.Models
{
    public interface IDistributionHubSender
    {
        void DistributeToUsers(IEnumerable<int> targetUserIds, string commandName, params object[] args);
        void DistributeActivity(UserActivity activity);
        void DistributeWorkRoomFileActivity(IEnumerable<WorkRoom> workRooms);
        void DistributeProgress(int count, int total, int userId, BasicConcerningObjectDto parentContextDto, string text = null);
        void CallComs(IMetisDataObject target, string shortPermission, string commandName, Guid? commandId, params object[] args);
        void DistributeInviteJobActivity(InviteJobsLog inviteJob);
        void DistributeNotificationFromHangfire(UserActivity userActivity);
        void DistributeZipStructure(string tree, int metisIdentityId, string zipRequestId);
        void CreateRequestPackage();
        void AddOperation(string commandName, params object[] args);
        string GetAndRemoveRequestPackage();
        Task ScheduleForDelivery(SignalRMessagePackage msg);
    }

    public class SignalRMessagePackage
    {
        public List<string> Usernames { get; set; }
        public bool DistributeToAll { get; set; }
        public string MethodName { get; set; }
        public object[] Args { get; set; }

        public bool HasUserDestinations
        {
            get
            {
                return DistributeToAll || (Usernames != null && Usernames.Any());
            }
        }
        public bool HasCompleted { get; set; }


        public SignalRMessagePackage() { }

        private SignalRMessagePackage(string methodName, params object[] args)
        {
            MethodName = methodName;
            Args = args;
        }

        public void SetUsername(string username)
        {
            if (!string.IsNullOrEmpty(username))
            {
                Usernames = new List<string>() { username };
            }
        }

        public SignalRMessagePackage(string username, string methodName, params object[] args) : this(methodName, args)
        {
            SetUsername(username);
        }

        public SignalRMessagePackage(IEnumerable<string> usernames, string methodName, params object[] args) : this(methodName, args)
        {
            Usernames = new List<string>(usernames.Where(u => !string.IsNullOrEmpty(u)));
        }

        public bool IsValid()
        {
            return HasUserDestinations && !string.IsNullOrEmpty(MethodName);
        }
    }
}
