﻿namespace Valuation.Logic.Models
{
    public interface INdaOwner
    {
        Nda Nda { get; set; }
        int? NdaId { get; set; }
    }
}