﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public class MetisIdentity : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MetisIdentityId { get; set; }

        public int UserProfilePublicId { get; set; }
        public int UserProfilePrivateId { get; set; }
        public string UserName { get; set; }

        public override int PrimaryKey
        {
            get { return MetisIdentityId; }
            set { MetisIdentityId = value; }
        }
    }
}