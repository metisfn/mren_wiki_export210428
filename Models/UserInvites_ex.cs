﻿namespace Valuation.Logic.Models
{
    public partial class UserInvite
    {
        public const string INVITE_PENDING_APPROVAL = "InvitePendingApproval";
        public const string INVITE_SENT = "InviteSent";
        public const string INVITE_ACCEPTED = "InviteAccepted";
    }
}