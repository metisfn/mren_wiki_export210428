﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metis.Core.DataObjects;

namespace Valuation.Logic.Models
{
    public partial class NotificationJobLog : DataObjectBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationJobLogId { get; set; }

        public string Status { get; set; }

        public bool Hourly { get; set; }
        public bool Daily { get; set; }
        public bool Weekly { get; set; }
        public bool Every5Min { get; set; }

        public override int PrimaryKey
        {
            get { return NotificationJobLogId; }
            set { NotificationJobLogId = value; }
        }
    }
}

