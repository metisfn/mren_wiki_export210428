/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/
using System;
using System.Linq;
using System.Xml;
using Metis.Core.DataObjects;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
	public partial class MetisUser : DataObjectBase, IMetisUser, IMetisScaffold
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Int32 MetisUserId { get; set; }

		[NotMapped]
		public override Int32 PrimaryKey { get { return MetisUserId; } set { MetisUserId = value; } }
		public String Email { get; set; }
		public Int32 ContactId { get; set; }
		public virtual Contact Contact { get; set; }
		IContact IMetisUser.Contact { get { return this.Contact; } set { this.Contact = (Contact)value; } }
	}
}
		