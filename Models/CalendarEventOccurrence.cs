/****************************************************************************************
THIS FILE IS AUTO-GENERATED.

********************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metis.Core.Calendar;
using Metis.Core.DataObjects;
using Metis.Core.DataObjects.Interfaces;

namespace Valuation.Logic.Models
{
    public class CalendarEventOccurrence : DataObjectBase, ICalendarEventOccurrence, IMetisScaffold
    {
        public virtual CalendarEvent CalendarEvent { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CalendarEventOccurrenceId { get; set; }

        [NotMapped]
        public override int PrimaryKey
        {
            get { return CalendarEventOccurrenceId; }
            set { CalendarEventOccurrenceId = value; }
        }

        public int CalendarEventId { get; set; }

        ICalendarEvent ICalendarEventOccurrence.CalendarEvent
        {
            get { return CalendarEvent; }
            set { CalendarEvent = (CalendarEvent) value; }
        }

        public DateTimeOffset EventStartTime { get; set; }
        public DateTimeOffset EventEndTime { get; set; }
        public int? RootFilesystemNodeId { get; set; }
    }
}